$(function() {
	var fileTemplate= ['<div class="Js_updateImg"><div class="updateImgLoadDiv">',
						'	<div class="uploadloadwrap">',
						'		<a href="javascript:;" class="close Js_upload_delete"></a>',
						'		<div class="updateImgLoad">',
						'			<span class="sc">上传中</span>',
						'			<span class="process-text">0%</span>',
						//'		<span class="scName"></span>',
						'			<div class="processDiv">',
						'				<div class="opacityBorder"></div>',
						'				<div class="process" style="width:0%">',
						'				</div>',
						'			</div>',
						'		</div>',
						'	</div>',
						'</div>',
						'<div class="height_10"></div></div>'].join("");

	$(".uploadWrap-single , .file_uploadbox-single").html5Uploader({
		name: 'userfile',
		postUrl: "post_file.php",//base_url+"/post_file.php",
		onClientAbort: function(){
			alert("上传中断，请重新上传");
		},
		onClientLoadStart: function(e, file, xhr) {
			for(var i=0; i<xhr_arr.length; i++){
				if(xhr===xhr_arr[i]){
					var xhrIndex = 	i;
				}
			}
			var fname = file.name;
			var pos = fname.lastIndexOf(".");
			//截取点之后的字符串
			var extension = fname.substring(pos+1);
			if(this.filetype.indexOf(extension)<0){
			//if(file.type.indexOf('image')<0){
				xhr.abort();
				xhr_arr.splice(xhrIndex,1);
				if(typeof FileReader == 'undefined') fileName.splice(xhrIndex,1);
				alert('上传文件格式不正确');
			}else{
				
				var obj = $(fileTemplate)
				if(upload_single.find('.updateSucImg').length>0){
					upload_single.find('.updateSucImg').eq(0).before(obj);
				}else{
					upload_single.append(obj);
					//upload_single.parents('.rowblock').find('.uploadWrap-single').hide();
				}
				uploadObjArr.push(obj);

/*				if($('.scName').length>0){
					uploadObjArr[xhrIndex].find('.scName').text(file.name+'(' + parseInt(file.size / 1024) + 'KB)');
					//显示图片文件名，和图片大小
				}
*/			
			}
		},
		onClientLoad: function(e, file, xhr) {
			for(var i=0; i<xhr_arr.length; i++){
				if(xhr===xhr_arr[i]){
					var xhrIndex = 	i;
				}	
			}
		},
		onServerLoadStart: function(e, file, xhr) {
			if(typeof FileReader == 'undefined'){
				var obj = $(fileTemplate)
				if(upload_single.find('.updateSucImg').length>0){
					upload_single.find('.updateSucImg').eq(0).before(obj);
				}else{
					upload_single.append(obj);
					upload_single.parents('.rowblock').find('.uploadWrap-single').hide();
				}
				uploadObjArr.push(obj);
			}
		},
		onServerProgress: function(e, file, xhr) {
			for(var i=0; i<xhr_arr.length; i++){
				if(xhr===xhr_arr[i]){
					var xhrIndex = 	i;
				}	
			}
			
			var percentComplete = e.loaded / e.total;
			var precentCompleteNum = parseInt((parseInt(e.loaded) / parseInt(e.total))*100);
			uploadObjArr[xhrIndex].find(".process").css("width",precentCompleteNum.toString()+"%");
			uploadObjArr[xhrIndex].find(".process-text").text(precentCompleteNum.toString()+"%");
			if(typeof FileReader == 'undefined'){
				uploadObjArr[xhrIndex].find('.scName').text(file.name+'(' + parseInt(e.total / 1024) + 'KB)');
			}
		},
		onServerLoad: function(e, file) {
			
		},
		onSuccess:function(e, file, xhr, data){
			var data = eval ('('+ data +')')
			var status = data.status;
			if(status== 0){
				switch (data.ext){
				   	case 'doc':
				     	break
				   	case 'pdf':
				     	break
				   	default:
						for(var i=0; i<xhr_arr.length; i++){
							if(xhr===xhr_arr[i]){
								var xhrIndex = 	i;
							}	
						}
						uploadObjArr[xhrIndex].remove();
						xhr_arr.splice(xhrIndex,1);
						uploadObjArr.splice(xhrIndex,1);
						if(typeof FileReader == 'undefined') fileName.splice(xhrIndex,1);
						var path = data.content;
						var tpic =['<div class="updateSucImg">',
								'	<div class="imgwrap">',
								'		<p><img src="'+base_url+path+'"></p>',	
								'	</div>',
								'	<a href="javascript:;" title="删除" class="close Js_file_delete">删除</	a>',
								'	<input name="cover" class="upload_success_cover" type="hidden" value="'+path+'" />',
								'</div>'].join("");
						upload_single.html(tpic);
				}
			}else{
				if(data.error){
					alert(data.error);//这里出要弹出意外信息，比如不支持的上传文件格式
				}else{
					alert("上传文件格式不正确，请从新上传")
				}
				upload_single.parents('.rowblock').find('.uploadWrap-single').show();
				//upload_single.parents('.rowblock').find('.upload-single').children().remove();
			}
		}
	});
	
	$('.Js_upload_delete').live('click',function(){
		for(var i=0; i<uploadObjArr.length; i++){
			if($(this).parents('.Js_updateImg').get(0)===uploadObjArr[i].get(0)){
				var objIndex = 	i;
			}	
		}
		
		if(xhr_arr[objIndex] != '' && xhr_arr[objIndex] != null ) {
			xhr_arr[objIndex].abort();
			xhr_arr.splice(objIndex,1);
			uploadObjArr.splice(objIndex,1);	
		}

		if(typeof FileReader == 'undefined') fileName.splice(objIndex,1);
		$(this).parent().next().remove();
		$(this).parents('.rowblock').find('.uploadWrap-single').show();
		$(this).parents(".updateImgLoadDiv").remove();
	})
	
	$(".uploadWrap-single .Js_uploadbox").live('click',function(){
		if($(this).parents('.rowblock').hasClass('Js_cover')) {
			upload_single = $(this).parents('.Js_cover').find('.upload-single');
		}
		$(this).parent().find(".file_uploadbox-single").click();
	});

    $(".tuploadbox").click(function(){
      $(".file_uploadbox-single").click();
    });

	
	$(".upload-single .Js_file_delete").die().live("click",function(){
		var that = $(this);
		var file_id = null;//文件id
		var match_id = null//比赛id
		var fdata = {task_id:file_id,match_id:match_id}
		//ajax_(base_url+'task/removeFinalWork',fdata,delete_file_c,that);  //发送Ajax数据
		delete_file_c(that)//删除成功回调
	})
	//删除文件成功回调
	function delete_file_c(obj){
		obj.parents('.rowblock').find('.uploadWrap-single').show();
		$(".uploadWrap-single").parents(".updateSucImg").remove();
		var ie = (navigator.appVersion.indexOf("MSIE")!=-1);//IE
		if(ie){
			//$(".file_uploadbox-single").parent()[0].reset();
		}else{
			$(".file_uploadbox-single").val("");
		}
		obj.parent().remove();
		return false
	}
});