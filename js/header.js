WG.UserInfo = {
	init: function(){
		var T = WG.UserInfo;
		$(".Js_user").on("mouseenter", T.showLoginInfo);
		$(".Js_user").on("mouseleave", T.hideLoginInfo)
	},
	showLoginInfo: function(){
		var $iobj = $("#Js_login_info");
		$iobj.show()
	},
	hideLoginInfo: function(){
		var $iobj = $("#Js_login_info");
		$iobj.hide();
	}
};

WG.Ccz = {
	init: function(){
		var T = WG.Ccz;
		$(".Js_search").on("click", T.showCcz);
		$(".Js_cczprompt,.Js_friendlist,.Js_msglist").on("click", function(e){WG.stopBubble(e)});
		$(".Js_friend_btn,.Js_alerts_btn").on("click", T.showFriends);
		$('.Js_search').on("input propertychange", T.showSearchList);
		//$(".Js_search").on("blur", T.hideCcz)
	},
	showCcz: function(e){
		//WG.stopBubble(e);
		var v = $.trim($(".Js_search").val()),
			T = WG.Ccz;
		if(v == ""){
			if($(".Js_cczprompt").is(":hidden")){
				$(".Js_cczprompt").show();
				$('.Js_cczprompt_li a').on('click', function(e){T.tellCcz(e,$(this))})
				$('.Js_cczonce').on('click', 'a.active', function(e) {T.callCcz(e)});
			} 
		}else{
			if($(".Js_searchResult").is(":hidden")) $(".Js_searchResult").show();
		}

		if($(".Js_friendlist").is(":visible")) $(".Js_friendlist").hide();
		if($(".Js_msglist").is(":visible")) $(".Js_msglist").hide();

		$(document).one("click", T.hideCcz);
		WG.stopBubble(e)
	},
	hideCcz: function(){
		var T = WG.Ccz;
		$('.Js_cczonce').off('click', 'a.active');
		$(".Js_cczprompt_li a").off("click");
		if($(".Js_cczprompt").is(":visible")) $(".Js_cczprompt").hide();
		if($(".Js_searchResult").is(":visible")) $(".Js_searchResult").hide();
		if($(".Js_friendlist").is(":visible")) $(".Js_friendlist").hide();
		if($(".Js_msglist").is(":visible")) $(".Js_msglist").hide();
		$(".Js_friendalert a").removeClass('on');
	},
	showSearchList: function(e){
		var T = WG.Ccz,
			$that = $(this);
		var v = $.trim($that.val());
		if(v !== ""){
			T.hideCcz();
			$(".Js_searchResult").show();
		}else{
			T.showCcz();
			$(".Js_searchResult").hide();
		}
		$(document).one("click", T.hideCcz);
		WG.stopBubble(e)

	},
	tellCcz: function(e,obj){
		var $this = obj;
		var index = $this.index();
		$('.Js_cczprompt_li a').not(':eq(' + index + ')').removeClass('active');
		$this.toggleClass('active');
		if ($('.Js_cczprompt_li a[class*=active]').length > 0) $('.Js_cczonce .cczonce').addClass('active');
		else $('.Js_cczonce .cczonce').removeClass('active');
		WG.stopBubble(e)
	},
	callCcz: function(e){
		alert('可以点击');
		WG.stopBubble(e)
	},
	showFriends: function(e){
		var T = WG.Ccz;
		$(".Js_friendalert a").not($(this)).removeClass("on");
		var index = $(this).index();
		var $obj,$obj2;
		if(index==0){
			$obj = $(".Js_friendlist");
			$obj2 = $(".Js_msglist")
		}else{
			$obj = $(".Js_msglist");
			$obj2 = $(".Js_friendlist")
		};
		
		if($obj.is(":hidden")){
			$(this).addClass('on');
			$obj.show();
			$obj2.hide();
		}else{
			$(this).removeClass('on');
			$obj.hide();
			$obj2.hide();
		}

		if($(".Js_cczprompt").is(":visible") || $(".Js_searchResult").is(":visible")) $(".Js_cczprompt,.Js_searchResult").hide();
		$(document).one("click", T.hideCcz);
		WG.stopBubble(e)
	}
}