/**
 * [dataControl 日历组件]
 * auther:liyonggang
 * @param  {[type]} d1 [日历输入框id]
 * @param  {[type]} d2 [日历输入框id]
 * 设置时间段，需要两个日历一起使用
 */
WG.DatePicker = {
	run: function(d1, d2) {
		var date1 = d1 || null,
			date2 = d2 || null;
		this.Obj = [];
		if (date1) this.Obj.push(date1);
		if (date2) this.Obj.push(date2);
		this.dateControl(date1, date2);
	},
	dateControl: function(d1, d2) {
		var a = $('#' + d1).length;
		var b = $("#date1").length;
		if ($('#' + d1).length > 0) {
			if (d2) {
				$('#' + d1).datepicker({
					picker: "<span class='picker'></span>",
					applyrule: this.rule,
					onloaded: this.onloaderCallBack
				});
				$("#" + d2).datepicker({
					picker: "<span class='picker'></span>",
					applyrule: this.rule,
					onloaded: this.onloaderCallBack
				});
			} else {
				$("#" + d1).datepicker({
					picker: "<span class='picker'></span>",
					applyrule: this.rule,
					onloaded: this.onloaderCallBack
				});
			}
		}
	},
	rule: function(id) {
		//before start time picker drops down
		var today = new Date();
		var aM = today.getMonth(),
			aD = today.getDate(),
			aY = today.getFullYear();
		var customDate = new Date(aY, aM, aD); //可以随便自定义日期
		var T = WG.DatePicker;
		if (id == T.Obj[0]) {
			var v = $("#" + T.Obj[1]).val();
			if (!v || v == "") {
				//return { startdate: customDate };
			} else {
				//To get end time. All dates after end time will be disabled.
				var d = v.match(/^(\d{1,4})(-|\/|.)(\d{1,2})\2(\d{1,2})$/);
				if (d != null) {
					var nd = new Date(parseInt(d[1], 10), parseInt(d[3], 10) - 1, parseInt(d[4], 10));
					//return { enddate: nd, startdate: customDate };
				} else {
					//return { startdate: customDate };
				}
			}
		} else {
			var v = $("#" + T.Obj[0]).val();
			if (!v || v == "") {
				//return { startdate: customDate };
			} else {
				//To get start time. All dates before start time will be disabled.
				var d = v.match(/^(\d{1,4})(-|\/|.)(\d{1,2})\2(\d{1,2})$/);
				if (d != null) {
					var nd = new Date(parseInt(d[1], 10), parseInt(d[3], 10) - 1, parseInt(d[4], 10));
					//return { startdate: nd };
				} else {
					//return { startdate: customDate }
				}
			}
		}
	},
	onloaderCallBack: function() {
		//日历加载后执行回调函数
	}
};

$(function(){
	WG.DatePicker.run('date1');
})
