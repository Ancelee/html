/**
 * [RadioItem 单选框类插件]
 * auther:liyonggang
 */
(function($){
	$.fn.radioItem = function(settings){
		this.each(function(){
			var $this=$(this),$obj=$this.find(".Js_check");
				
			$obj.on("click",function(){
				var _this = $(this);
				var _index = _this.index();
				var _obj = _this.parents(this); 
				if($this.find(".inact").length > 0){
					var _o = $this.find(".inact");
					var _i = _index + 1;
					_o.find(".ibox").hide();
					_o.find(".Js_item"+_i).show();
				}
			});
			if($this.find("input").is(":checked")){
				var _t = $this.find("input:checked").parent().index();
				$(this).find(".Js_check").eq(_t).click();
			}
		}
	)}
})(jQuery);
