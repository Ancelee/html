var cityList = [
    {
        name: "请选择..."
    },
    {
        name: "北京",
        attr: {
            id: "10|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "东城区",
                attr: {
                    id: "10|00|01"
                }
            },
            {
                name: "西城区",
                attr: {
                    id: "10|00|02"
                }
            },
            {
                name: "朝阳区",
                attr: {
                    id: "10|00|05"
                }
            },
            {
                name: "丰台区",
                attr: {
                    id: "10|00|06"
                }
            },
            {
                name: "石景山区",
                attr: {
                    id: "10|00|07"
                }
            },
            {
                name: "海淀区",
                attr: {
                    id: "10|00|08"
                }
            },
            {
                name: "门头沟区",
                attr: {
                    id: "10|00|09"
                }
            },
            {
                name: "房山区",
                attr: {
                    id: "10|00|10"
                }
            },
            {
                name: "通州区",
                attr: {
                    id: "10|00|11"
                }
            },
            {
                name: "顺义区",
                attr: {
                    id: "10|00|12"
                }
            },
            {
                name: "昌平区",
                attr: {
                    id: "10|00|13"
                }
            },
            {
                name: "大兴区",
                attr: {
                    id: "10|00|14"
                }
            },
            {
                name: "平谷区",
                attr: {
                    id: "10|00|15"
                }
            },
            {
                name: "怀柔区",
                attr: {
                    id: "10|00|16"
                }
            },
            {
                name: "密云县",
                attr: {
                    id: "10|00|17"
                }
            },
            {
                name: "延庆县",
                attr: {
                    id: "10|00|18"
                }
            }
        ]
    },
    {
        name: "天津",
        attr: {
            id: "11|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "和平区",
                attr: {
                    id: "11|00|01"
                }
            },
            {
                name: "河东区",
                attr: {
                    id: "11|00|02"
                }
            },
            {
                name: "河西区",
                attr: {
                    id: "11|00|03"
                }
            },
            {
                name: "南开区",
                attr: {
                    id: "11|00|04"
                }
            },
            {
                name: "河北区",
                attr: {
                    id: "11|00|05"
                }
            },
            {
                name: "红桥区",
                attr: {
                    id: "11|00|06"
                }
            },
            {
                name: "滨海新区",
                attr: {
                    id: "11|00|07"
                }
            },
            {
                name: "东丽区",
                attr: {
                    id: "11|00|08"
                }
            },
            {
                name: "西青区",
                attr: {
                    id: "11|00|09"
                }
            },
            {
                name: "津南区",
                attr: {
                    id: "11|00|10"
                }
            },
            {
                name: "北辰区",
                attr: {
                    id: "11|00|11"
                }
            },
            {
                name: "宁河县",
                attr: {
                    id: "11|00|12"
                }
            },
            {
                name: "武清区",
                attr: {
                    id: "11|00|13"
                }
            },
            {
                name: "静海县",
                attr: {
                    id: "11|00|14"
                }
            },
            {
                name: "宝坻区",
                attr: {
                    id: "11|00|15"
                }
            },
            {
                name: "蓟县",
                attr: {
                    id: "11|00|16"
                }
            }
        ]
    },
    {
        name: "河北",
        attr: {
            id: "12|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "石家庄",
                attr: {
                    id: "12|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "长安区",
                        attr: {
                            id: "12|01|01"
                        }
                    },
                    {
                        name: "桥东区",
                        attr: {
                            id: "12|01|02"
                        }
                    },
                    {
                        name: "桥西区",
                        attr: {
                            id: "12|01|03"
                        }
                    },
                    {
                        name: "新华区",
                        attr: {
                            id: "12|01|04"
                        }
                    },
                    {
                        name: "井陉矿区",
                        attr: {
                            id: "12|01|05"
                        }
                    },
                    {
                        name: "裕华区",
                        attr: {
                            id: "12|01|06"
                        }
                    },
                    {
                        name: "井陉县",
                        attr: {
                            id: "12|01|07"
                        }
                    },
                    {
                        name: "正定县",
                        attr: {
                            id: "12|01|08"
                        }
                    },
                    {
                        name: "栾城县",
                        attr: {
                            id: "12|01|09"
                        }
                    },
                    {
                        name: "行唐县",
                        attr: {
                            id: "12|01|10"
                        }
                    },
                    {
                        name: "灵寿县",
                        attr: {
                            id: "12|01|11"
                        }
                    },
                    {
                        name: "高邑县",
                        attr: {
                            id: "12|01|12"
                        }
                    },
                    {
                        name: "深泽县",
                        attr: {
                            id: "12|01|13"
                        }
                    },
                    {
                        name: "赞皇县",
                        attr: {
                            id: "12|01|14"
                        }
                    },
                    {
                        name: "无极县",
                        attr: {
                            id: "12|01|15"
                        }
                    },
                    {
                        name: "平山县",
                        attr: {
                            id: "12|01|16"
                        }
                    },
                    {
                        name: "元氏县",
                        attr: {
                            id: "12|01|17"
                        }
                    },
                    {
                        name: "赵县",
                        attr: {
                            id: "12|01|18"
                        }
                    },
                    {
                        name: "辛集市",
                        attr: {
                            id: "12|01|19"
                        }
                    },
                    {
                        name: "藁城市",
                        attr: {
                            id: "12|01|20"
                        }
                    },
                    {
                        name: "晋州市",
                        attr: {
                            id: "12|01|21"
                        }
                    },
                    {
                        name: "新乐市",
                        attr: {
                            id: "12|01|22"
                        }
                    },
                    {
                        name: "鹿泉市",
                        attr: {
                            id: "12|01|23"
                        }
                    }
                ]
            },
            {
                name: "唐山",
                attr: {
                    id: "12|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "路南区",
                        attr: {
                            id: "12|02|01"
                        }
                    },
                    {
                        name: "路北区",
                        attr: {
                            id: "12|02|02"
                        }
                    },
                    {
                        name: "古冶区",
                        attr: {
                            id: "12|02|03"
                        }
                    },
                    {
                        name: "开平区",
                        attr: {
                            id: "12|02|04"
                        }
                    },
                    {
                        name: "丰南区",
                        attr: {
                            id: "12|02|05"
                        }
                    },
                    {
                        name: "丰润区",
                        attr: {
                            id: "12|02|06"
                        }
                    },
                    {
                        name: "滦县",
                        attr: {
                            id: "12|02|07"
                        }
                    },
                    {
                        name: "滦南县",
                        attr: {
                            id: "12|02|08"
                        }
                    },
                    {
                        name: "乐亭县",
                        attr: {
                            id: "12|02|09"
                        }
                    },
                    {
                        name: "迁西县",
                        attr: {
                            id: "12|02|10"
                        }
                    },
                    {
                        name: "玉田县",
                        attr: {
                            id: "12|02|11"
                        }
                    },
                    {
                        name: "曹妃甸区",
                        attr: {
                            id: "12|02|12"
                        }
                    },
                    {
                        name: "遵化市",
                        attr: {
                            id: "12|02|13"
                        }
                    },
                    {
                        name: "赞皇县",
                        attr: {
                            id: "12|02|14"
                        }
                    },
                    {
                        name: "迁安市",
                        attr: {
                            id: "12|02|15"
                        }
                    }
                ]
            },
            {
                name: "秦皇岛",
                attr: {
                    id: "12|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "海港区",
                        attr: {
                            id: "12|03|01"
                        }
                    },
                    {
                        name: "山海关区",
                        attr: {
                            id: "12|03|02"
                        }
                    },
                    {
                        name: "北戴河区",
                        attr: {
                            id: "12|03|03"
                        }
                    },
                    {
                        name: "青龙满族自治县",
                        attr: {
                            id: "12|03|04"
                        }
                    },
                    {
                        name: "昌黎县",
                        attr: {
                            id: "12|03|05"
                        }
                    },
                    {
                        name: "抚宁县",
                        attr: {
                            id: "12|03|06"
                        }
                    },
                    {
                        name: "卢龙县",
                        attr: {
                            id: "12|03|07"
                        }
                    }
                ]
            },
            {
                name: "邯郸",
                attr: {
                    id: "12|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "邯山区",
                        attr: {
                            id: "12|04|01"
                        }
                    },
                    {
                        name: "丛台区",
                        attr: {
                            id: "12|04|02"
                        }
                    },
                    {
                        name: "复兴区",
                        attr: {
                            id: "12|04|03"
                        }
                    },
                    {
                        name: "峰峰矿区",
                        attr: {
                            id: "12|04|04"
                        }
                    },
                    {
                        name: "邯郸县",
                        attr: {
                            id: "12|04|05"
                        }
                    },
                    {
                        name: "临漳县",
                        attr: {
                            id: "12|04|06"
                        }
                    },
                    {
                        name: "成安县",
                        attr: {
                            id: "12|04|07"
                        }
                    },
                    {
                        name: "大名县",
                        attr: {
                            id: "12|04|08"
                        }
                    },
                    {
                        name: "涉县",
                        attr: {
                            id: "12|04|09"
                        }
                    },
                    {
                        name: "磁县",
                        attr: {
                            id: "12|04|10"
                        }
                    },
                    {
                        name: "肥乡县",
                        attr: {
                            id: "12|04|11"
                        }
                    },
                    {
                        name: "永年县",
                        attr: {
                            id: "12|04|12"
                        }
                    },
                    {
                        name: "邱县",
                        attr: {
                            id: "12|04|13"
                        }
                    },
                    {
                        name: "鸡泽县",
                        attr: {
                            id: "12|04|14"
                        }
                    },
                    {
                        name: "广平县",
                        attr: {
                            id: "12|04|15"
                        }
                    },
                    {
                        name: "馆陶县",
                        attr: {
                            id: "12|04|16"
                        }
                    },
                    {
                        name: "魏县",
                        attr: {
                            id: "12|04|17"
                        }
                    },
                    {
                        name: "曲周县",
                        attr: {
                            id: "12|04|18"
                        }
                    },
                    {
                        name: "武安市",
                        attr: {
                            id: "12|04|19"
                        }
                    }
                ]
            },
            {
                name: "邢台",
                attr: {
                    id: "12|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "桥东区",
                        attr: {
                            id: "12|05|01"
                        }
                    },
                    {
                        name: "桥西区",
                        attr: {
                            id: "12|05|02"
                        }
                    },
                    {
                        name: "邢台县",
                        attr: {
                            id: "12|05|03"
                        }
                    },
                    {
                        name: "临城县",
                        attr: {
                            id: "12|05|04"
                        }
                    },
                    {
                        name: "内丘县",
                        attr: {
                            id: "12|05|05"
                        }
                    },
                    {
                        name: "柏乡县",
                        attr: {
                            id: "12|05|06"
                        }
                    },
                    {
                        name: "隆尧县",
                        attr: {
                            id: "12|05|07"
                        }
                    },
                    {
                        name: "任县",
                        attr: {
                            id: "12|05|08"
                        }
                    },
                    {
                        name: "南和县",
                        attr: {
                            id: "12|05|09"
                        }
                    },
                    {
                        name: "宁晋县",
                        attr: {
                            id: "12|05|10"
                        }
                    },
                    {
                        name: "巨鹿县",
                        attr: {
                            id: "12|05|11"
                        }
                    },
                    {
                        name: "新河县",
                        attr: {
                            id: "12|05|12"
                        }
                    },
                    {
                        name: "广宗县",
                        attr: {
                            id: "12|05|13"
                        }
                    },
                    {
                        name: "平乡县",
                        attr: {
                            id: "12|05|14"
                        }
                    },
                    {
                        name: "威县",
                        attr: {
                            id: "12|05|15"
                        }
                    },
                    {
                        name: "清河县",
                        attr: {
                            id: "12|05|16"
                        }
                    },
                    {
                        name: "临西县",
                        attr: {
                            id: "12|05|17"
                        }
                    },
                    {
                        name: "南宫市",
                        attr: {
                            id: "12|05|18"
                        }
                    },
                    {
                        name: "沙河市",
                        attr: {
                            id: "12|05|19"
                        }
                    }
                ]
            },
            {
                name: "保定",
                attr: {
                    id: "12|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "北市区",
                        attr: {
                            id: "12|06|02"
                        }
                    },
                    {
                        name: "南市区",
                        attr: {
                            id: "12|06|03"
                        }
                    },
                    {
                        name: "满城县",
                        attr: {
                            id: "12|06|04"
                        }
                    },
                    {
                        name: "清苑县",
                        attr: {
                            id: "12|06|05"
                        }
                    },
                    {
                        name: "涞水县",
                        attr: {
                            id: "12|06|06"
                        }
                    },
                    {
                        name: "阜平县",
                        attr: {
                            id: "12|06|07"
                        }
                    },
                    {
                        name: "徐水县",
                        attr: {
                            id: "12|06|08"
                        }
                    },
                    {
                        name: "定兴县",
                        attr: {
                            id: "12|06|09"
                        }
                    },
                    {
                        name: "唐县",
                        attr: {
                            id: "12|06|10"
                        }
                    },
                    {
                        name: "高阳县",
                        attr: {
                            id: "12|06|11"
                        }
                    },
                    {
                        name: "容城县",
                        attr: {
                            id: "12|06|12"
                        }
                    },
                    {
                        name: "涞源县",
                        attr: {
                            id: "12|06|13"
                        }
                    },
                    {
                        name: "望都县",
                        attr: {
                            id: "12|06|14"
                        }
                    },
                    {
                        name: "安新县",
                        attr: {
                            id: "12|06|15"
                        }
                    },
                    {
                        name: "易县",
                        attr: {
                            id: "12|06|16"
                        }
                    },
                    {
                        name: "曲阳县",
                        attr: {
                            id: "12|06|17"
                        }
                    },
                    {
                        name: "蠡县",
                        attr: {
                            id: "12|06|18"
                        }
                    },
                    {
                        name: "顺平县",
                        attr: {
                            id: "12|06|19"
                        }
                    },
                    {
                        name: "博野县",
                        attr: {
                            id: "12|06|20"
                        }
                    },
                    {
                        name: "雄县",
                        attr: {
                            id: "12|06|21"
                        }
                    },
                    {
                        name: "涿州市",
                        attr: {
                            id: "12|06|22"
                        }
                    },
                    {
                        name: "定州市",
                        attr: {
                            id: "12|06|23"
                        }
                    },
                    {
                        name: "安国市",
                        attr: {
                            id: "12|06|24"
                        }
                    },
                    {
                        name: "高碑店市",
                        attr: {
                            id: "12|06|25"
                        }
                    }
                ]
            },
            {
                name: "张家口",
                attr: {
                    id: "12|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "桥东区",
                        attr: {
                            id: "12|07|01"
                        }
                    },
                    {
                        name: "桥西区",
                        attr: {
                            id: "12|07|02"
                        }
                    },
                    {
                        name: "宣化区",
                        attr: {
                            id: "12|03|03"
                        }
                    },
                    {
                        name: "下花园区",
                        attr: {
                            id: "12|07|04"
                        }
                    },
                    {
                        name: "宣化县",
                        attr: {
                            id: "12|07|05"
                        }
                    },
                    {
                        name: "张北县",
                        attr: {
                            id: "12|07|06"
                        }
                    },
                    {
                        name: "康保县",
                        attr: {
                            id: "12|07|07"
                        }
                    },
                    {
                        name: "沽源县",
                        attr: {
                            id: "12|07|08"
                        }
                    },
                    {
                        name: "尚义县",
                        attr: {
                            id: "12|07|09"
                        }
                    },
                    {
                        name: "蔚县",
                        attr: {
                            id: "12|07|10"
                        }
                    },
                    {
                        name: "阳原县",
                        attr: {
                            id: "12|07|11"
                        }
                    },
                    {
                        name: "怀安县",
                        attr: {
                            id: "12|07|12"
                        }
                    },
                    {
                        name: "万全县",
                        attr: {
                            id: "12|07|13"
                        }
                    },
                    {
                        name: "怀来县",
                        attr: {
                            id: "12|07|14"
                        }
                    },
                    {
                        name: "涿鹿县",
                        attr: {
                            id: "12|07|15"
                        }
                    },
                    {
                        name: "赤城县",
                        attr: {
                            id: "12|07|16"
                        }
                    },
                    {
                        name: "崇礼县",
                        attr: {
                            id: "12|07|17"
                        }
                    }
                ]
            },
            {
                name: "承德",
                attr: {
                    id: "12|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "双桥区",
                        attr: {
                            id: "12|08|01"
                        }
                    },
                    {
                        name: "双滦区",
                        attr: {
                            id: "12|08|02"
                        }
                    },
                    {
                        name: "鹰手营子矿区",
                        attr: {
                            id: "12|08|03"
                        }
                    },
                    {
                        name: "承德县",
                        attr: {
                            id: "12|08|04"
                        }
                    },
                    {
                        name: "兴隆县",
                        attr: {
                            id: "12|08|05"
                        }
                    },
                    {
                        name: "平泉县",
                        attr: {
                            id: "12|08|06"
                        }
                    },
                    {
                        name: "滦平县",
                        attr: {
                            id: "12|08|07"
                        }
                    },
                    {
                        name: "隆化县",
                        attr: {
                            id: "12|08|08"
                        }
                    },
                    {
                        name: "丰宁满族自治县",
                        attr: {
                            id: "12|08|09"
                        }
                    },
                    {
                        name: "宽城满族自治县",
                        attr: {
                            id: "12|08|10"
                        }
                    },
                    {
                        name: "围场满族蒙古族自治县",
                        attr: {
                            id: "12|08|11"
                        }
                    }
                ]
            },
            {
                name: "沧州",
                attr: {
                    id: "12|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "新华区",
                        attr: {
                            id: "12|09|01"
                        }
                    },
                    {
                        name: "运河区",
                        attr: {
                            id: "12|09|02"
                        }
                    },
                    {
                        name: "沧县",
                        attr: {
                            id: "12|09|03"
                        }
                    },
                    {
                        name: "青县",
                        attr: {
                            id: "12|09|04"
                        }
                    },
                    {
                        name: "东光县",
                        attr: {
                            id: "12|09|05"
                        }
                    },
                    {
                        name: "海兴县",
                        attr: {
                            id: "12|09|06"
                        }
                    },
                    {
                        name: "盐山县",
                        attr: {
                            id: "12|09|07"
                        }
                    },
                    {
                        name: "肃宁县",
                        attr: {
                            id: "12|09|08"
                        }
                    },
                    {
                        name: "南皮县",
                        attr: {
                            id: "12|09|09"
                        }
                    },
                    {
                        name: "吴桥县",
                        attr: {
                            id: "12|09|10"
                        }
                    },
                    {
                        name: "献县",
                        attr: {
                            id: "12|09|11"
                        }
                    },
                    {
                        name: "孟村回族自治县",
                        attr: {
                            id: "12|09|12"
                        }
                    },
                    {
                        name: "泊头市",
                        attr: {
                            id: "12|09|13"
                        }
                    },
                    {
                        name: "任丘市",
                        attr: {
                            id: "12|09|14"
                        }
                    },
                    {
                        name: "黄骅市",
                        attr: {
                            id: "12|09|15"
                        }
                    },
                    {
                        name: "河间市",
                        attr: {
                            id: "12|09|16"
                        }
                    }
                ]
            },
            {
                name: "廊坊",
                attr: {
                    id: "12|10|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "安次区",
                        attr: {
                            id: "12|10|01"
                        }
                    },
                    {
                        name: "广阳区",
                        attr: {
                            id: "12|10|02"
                        }
                    },
                    {
                        name: "固安县",
                        attr: {
                            id: "12|10|03"
                        }
                    },
                    {
                        name: "永清县",
                        attr: {
                            id: "12|10|04"
                        }
                    },
                    {
                        name: "香河县",
                        attr: {
                            id: "12|10|05"
                        }
                    },
                    {
                        name: "大城县",
                        attr: {
                            id: "12|10|06"
                        }
                    },
                    {
                        name: "文安县",
                        attr: {
                            id: "12|10|07"
                        }
                    },
                    {
                        name: "大厂回族自治县",
                        attr: {
                            id: "12|10|08"
                        }
                    },
                    {
                        name: "霸州市",
                        attr: {
                            id: "12|10|09"
                        }
                    },
                    {
                        name: "三河市",
                        attr: {
                            id: "12|10|10"
                        }
                    }
                ]
            },
            {
                name: "衡水",
                attr: {
                    id: "12|11|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "桃城区",
                        attr: {
                            id: "12|11|01"
                        }
                    },
                    {
                        name: "枣强县",
                        attr: {
                            id: "12|11|02"
                        }
                    },
                    {
                        name: "武邑县",
                        attr: {
                            id: "12|11|03"
                        }
                    },
                    {
                        name: "武强县",
                        attr: {
                            id: "12|11|04"
                        }
                    },
                    {
                        name: "饶阳县",
                        attr: {
                            id: "12|11|05"
                        }
                    },
                    {
                        name: "安平县",
                        attr: {
                            id: "12|11|06"
                        }
                    },
                    {
                        name: "故城县",
                        attr: {
                            id: "12|11|07"
                        }
                    },
                    {
                        name: "景县",
                        attr: {
                            id: "12|11|08"
                        }
                    },
                    {
                        name: "阜城县",
                        attr: {
                            id: "12|11|09"
                        }
                    },
                    {
                        name: "冀州市",
                        attr: {
                            id: "12|11|10"
                        }
                    },
                    {
                        name: "深州市",
                        attr: {
                            id: "12|11|11"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "山西",
        attr: {
            id: "13|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "太原",
                attr: {
                    id: "13|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "小店区",
                        attr: {
                            id: "13|01|01"
                        }
                    },
                    {
                        name: "迎泽区",
                        attr: {
                            id: "13|01|02"
                        }
                    },
                    {
                        name: "杏花岭区",
                        attr: {
                            id: "13|01|03"
                        }
                    },
                    {
                        name: "尖草坪区",
                        attr: {
                            id: "13|01|04"
                        }
                    },
                    {
                        name: "万柏林区",
                        attr: {
                            id: "13|01|05"
                        }
                    },
                    {
                        name: "晋源区",
                        attr: {
                            id: "13|01|06"
                        }
                    },
                    {
                        name: "清徐县",
                        attr: {
                            id: "13|01|07"
                        }
                    },
                    {
                        name: "阳曲县",
                        attr: {
                            id: "13|01|08"
                        }
                    },
                    {
                        name: "娄烦县",
                        attr: {
                            id: "13|01|09"
                        }
                    },
                    {
                        name: "古交市",
                        attr: {
                            id: "13|01|10"
                        }
                    }
                ]
            },
            {
                name: "大同",
                attr: {
                    id: "13|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "城区",
                        attr: {
                            id: "13|02|01"
                        }
                    },
                    {
                        name: "矿区",
                        attr: {
                            id: "13|02|02"
                        }
                    },
                    {
                        name: "南郊区",
                        attr: {
                            id: "13|02|03"
                        }
                    },
                    {
                        name: "新荣区",
                        attr: {
                            id: "13|02|04"
                        }
                    },
                    {
                        name: "阳高县",
                        attr: {
                            id: "13|02|05"
                        }
                    },
                    {
                        name: "天镇县",
                        attr: {
                            id: "13|02|06"
                        }
                    },
                    {
                        name: "广灵县",
                        attr: {
                            id: "13|02|07"
                        }
                    },
                    {
                        name: "灵丘县",
                        attr: {
                            id: "13|02|08"
                        }
                    },
                    {
                        name: "浑源县",
                        attr: {
                            id: "13|02|09"
                        }
                    },
                    {
                        name: "左云县",
                        attr: {
                            id: "13|02|10"
                        }
                    },
                    {
                        name: "大同县",
                        attr: {
                            id: "13|02|11"
                        }
                    }
                ]
            },
            {
                name: "阳泉",
                attr: {
                    id: "13|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "城区",
                        attr: {
                            id: "13|03|01"
                        }
                    },
                    {
                        name: "矿区",
                        attr: {
                            id: "13|03|02"
                        }
                    },
                    {
                        name: "郊区",
                        attr: {
                            id: "13|03|03"
                        }
                    },
                    {
                        name: "平定县",
                        attr: {
                            id: "13|03|04"
                        }
                    },
                    {
                        name: "孟县",
                        attr: {
                            id: "13|03|05"
                        }
                    }
                ]
            },
            {
                name: "长治",
                attr: {
                    id: "13|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "城区",
                        attr: {
                            id: "13|04|01"
                        }
                    },
                    {
                        name: "郊区",
                        attr: {
                            id: "13|04|02"
                        }
                    },
                    {
                        name: "长治县",
                        attr: {
                            id: "13|04|03"
                        }
                    },
                    {
                        name: "襄垣县",
                        attr: {
                            id: "13|04|04"
                        }
                    },
                    {
                        name: "屯留县",
                        attr: {
                            id: "13|04|05"
                        }
                    },
                    {
                        name: "平顺县",
                        attr: {
                            id: "13|04|06"
                        }
                    },
                    {
                        name: "黎城县",
                        attr: {
                            id: "13|04|07"
                        }
                    },
                    {
                        name: "壶关县",
                        attr: {
                            id: "13|04|08"
                        }
                    },
                    {
                        name: "长子县",
                        attr: {
                            id: "13|04|09"
                        }
                    },
                    {
                        name: "武乡县",
                        attr: {
                            id: "13|04|10"
                        }
                    },
                    {
                        name: "沁县",
                        attr: {
                            id: "13|04|11"
                        }
                    },
                    {
                        name: "沁源县",
                        attr: {
                            id: "13|04|12"
                        }
                    },
                    {
                        name: "潞城市",
                        attr: {
                            id: "13|04|13"
                        }
                    }
                ]
            },
            {
                name: "晋城",
                attr: {
                    id: "13|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "城区",
                        attr: {
                            id: "13|05|01"
                        }
                    },
                    {
                        name: "沁水县",
                        attr: {
                            id: "13|05|02"
                        }
                    },
                    {
                        name: "阳城县",
                        attr: {
                            id: "13|05|03"
                        }
                    },
                    {
                        name: "陵川县",
                        attr: {
                            id: "13|05|04"
                        }
                    },
                    {
                        name: "泽州县",
                        attr: {
                            id: "13|05|05"
                        }
                    },
                    {
                        name: "高平市",
                        attr: {
                            id: "13|05|06"
                        }
                    }
                ]
            },
            {
                name: "朔州",
                attr: {
                    id: "12|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "朔城区",
                        attr: {
                            id: "12|06|01"
                        }
                    },
                    {
                        name: "平鲁区",
                        attr: {
                            id: "12|06|02"
                        }
                    },
                    {
                        name: "山阴县",
                        attr: {
                            id: "12|06|03"
                        }
                    },
                    {
                        name: "应县",
                        attr: {
                            id: "12|06|04"
                        }
                    },
                    {
                        name: "右玉县",
                        attr: {
                            id: "12|06|05"
                        }
                    },
                    {
                        name: "怀仁县",
                        attr: {
                            id: "13|06|06"
                        }
                    }
                ]
            },
            {
                name: "晋中",
                attr: {
                    id: "13|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "榆次区",
                        attr: {
                            id: "13|07|01"
                        }
                    },
                    {
                        name: "榆社县",
                        attr: {
                            id: "13|07|02"
                        }
                    },
                    {
                        name: "左权县",
                        attr: {
                            id: "13|03|03"
                        }
                    },
                    {
                        name: "和顺县",
                        attr: {
                            id: "13|07|04"
                        }
                    },
                    {
                        name: "昔阳县",
                        attr: {
                            id: "13|07|05"
                        }
                    },
                    {
                        name: "寿阳县",
                        attr: {
                            id: "13|07|06"
                        }
                    },
                    {
                        name: "太谷县",
                        attr: {
                            id: "13|07|07"
                        }
                    },
                    {
                        name: "祁县",
                        attr: {
                            id: "13|07|08"
                        }
                    },
                    {
                        name: "平遥县",
                        attr: {
                            id: "13|07|09"
                        }
                    },
                    {
                        name: "灵石县",
                        attr: {
                            id: "13|07|10"
                        }
                    },
                    {
                        name: "介休市",
                        attr: {
                            id: "13|07|11"
                        }
                    }
                ]
            },
            {
                name: "运城",
                attr: {
                    id: "13|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "盐湖区",
                        attr: {
                            id: "13|08|01"
                        }
                    },
                    {
                        name: "临猗县",
                        attr: {
                            id: "13|08|02"
                        }
                    },
                    {
                        name: "万荣县",
                        attr: {
                            id: "13|08|03"
                        }
                    },
                    {
                        name: "闻喜县",
                        attr: {
                            id: "13|08|04"
                        }
                    },
                    {
                        name: "稷山县",
                        attr: {
                            id: "13|08|05"
                        }
                    },
                    {
                        name: "新绛县",
                        attr: {
                            id: "13|08|06"
                        }
                    },
                    {
                        name: "绛县",
                        attr: {
                            id: "13|08|07"
                        }
                    },
                    {
                        name: "垣曲县",
                        attr: {
                            id: "13|08|08"
                        }
                    },
                    {
                        name: "夏县",
                        attr: {
                            id: "13|08|09"
                        }
                    },
                    {
                        name: "平陆县",
                        attr: {
                            id: "13|08|10"
                        }
                    },
                    {
                        name: "芮城县",
                        attr: {
                            id: "13|08|11"
                        }
                    },
                    {
                        name: "永济市",
                        attr: {
                            id: "13|08|12"
                        }
                    },
                    {
                        name: "河津市",
                        attr: {
                            id: "13|08|13"
                        }
                    }
                ]
            },
            {
                name: "忻州",
                attr: {
                    id: "13|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "忻府区",
                        attr: {
                            id: "13|09|01"
                        }
                    },
                    {
                        name: "定襄县",
                        attr: {
                            id: "13|09|02"
                        }
                    },
                    {
                        name: "五台县",
                        attr: {
                            id: "13|09|03"
                        }
                    },
                    {
                        name: "代县",
                        attr: {
                            id: "13|09|04"
                        }
                    },
                    {
                        name: "繁峙县",
                        attr: {
                            id: "13|09|05"
                        }
                    },
                    {
                        name: "宁武县",
                        attr: {
                            id: "13|09|06"
                        }
                    },
                    {
                        name: "静乐县",
                        attr: {
                            id: "13|09|07"
                        }
                    },
                    {
                        name: "神池县",
                        attr: {
                            id: "13|09|08"
                        }
                    },
                    {
                        name: "五寨县",
                        attr: {
                            id: "13|09|09"
                        }
                    },
                    {
                        name: "岢岚县",
                        attr: {
                            id: "13|09|10"
                        }
                    },
                    {
                        name: "河曲县",
                        attr: {
                            id: "13|09|11"
                        }
                    },
                    {
                        name: "保德县",
                        attr: {
                            id: "13|09|12"
                        }
                    },
                    {
                        name: "偏关县",
                        attr: {
                            id: "13|09|13"
                        }
                    },
                    {
                        name: "原平市",
                        attr: {
                            id: "13|09|14"
                        }
                    }
                ]
            },
            {
                name: "临汾",
                attr: {
                    id: "13|10|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "尧都区",
                        attr: {
                            id: "13|10|01"
                        }
                    },
                    {
                        name: "曲沃县",
                        attr: {
                            id: "13|10|02"
                        }
                    },
                    {
                        name: "翼城县",
                        attr: {
                            id: "13|10|03"
                        }
                    },
                    {
                        name: "襄汾县",
                        attr: {
                            id: "13|10|04"
                        }
                    },
                    {
                        name: "洪洞县",
                        attr: {
                            id: "13|10|05"
                        }
                    },
                    {
                        name: "古县",
                        attr: {
                            id: "13|10|06"
                        }
                    },
                    {
                        name: "安泽县",
                        attr: {
                            id: "13|10|07"
                        }
                    },
                    {
                        name: "浮山县",
                        attr: {
                            id: "13|10|08"
                        }
                    },
                    {
                        name: "吉县",
                        attr: {
                            id: "13|10|09"
                        }
                    },
                    {
                        name: "乡宁县",
                        attr: {
                            id: "13|10|10"
                        }
                    },
                    {
                        name: "大宁县",
                        attr: {
                            id: "13|10|11"
                        }
                    },
                    {
                        name: "隰县",
                        attr: {
                            id: "13|10|12"
                        }
                    },
                    {
                        name: "永和县",
                        attr: {
                            id: "13|10|13"
                        }
                    },
                    {
                        name: "蒲县",
                        attr: {
                            id: "13|10|14"
                        }
                    },
                    {
                        name: "汾西县",
                        attr: {
                            id: "13|10|15"
                        }
                    },
                    {
                        name: "侯马市",
                        attr: {
                            id: "13|10|16"
                        }
                    },
                    {
                        name: "霍州市",
                        attr: {
                            id: "13|10|17"
                        }
                    }
                ]
            },
            {
                name: "吕梁",
                attr: {
                    id: "13|11|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "离石区",
                        attr: {
                            id: "13|11|01"
                        }
                    },
                    {
                        name: "文水县",
                        attr: {
                            id: "13|11|02"
                        }
                    },
                    {
                        name: "交城县",
                        attr: {
                            id: "13|11|03"
                        }
                    },
                    {
                        name: "兴县",
                        attr: {
                            id: "13|11|04"
                        }
                    },
                    {
                        name: "临县",
                        attr: {
                            id: "13|11|05"
                        }
                    },
                    {
                        name: "柳林县",
                        attr: {
                            id: "13|11|06"
                        }
                    },
                    {
                        name: "石楼县",
                        attr: {
                            id: "13|11|07"
                        }
                    },
                    {
                        name: "岚县",
                        attr: {
                            id: "13|11|08"
                        }
                    },
                    {
                        name: "方山县",
                        attr: {
                            id: "13|11|09"
                        }
                    },
                    {
                        name: "中阳县",
                        attr: {
                            id: "13|11|10"
                        }
                    },
                    {
                        name: "交口县",
                        attr: {
                            id: "13|11|11"
                        }
                    },
                    {
                        name: "孝义市",
                        attr: {
                            id: "13|11|12"
                        }
                    },
                    {
                        name: "汾阳市",
                        attr: {
                            id: "13|11|13"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "内蒙古",
        attr: {
            id: "14|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "呼和浩特",
                attr: {
                    id: "14|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "新城区",
                        attr: {
                            id: "14|01|01"
                        }
                    },
                    {
                        name: "回民区",
                        attr: {
                            id: "14|01|02"
                        }
                    },
                    {
                        name: "玉泉区",
                        attr: {
                            id: "14|01|03"
                        }
                    },
                    {
                        name: "赛罕区",
                        attr: {
                            id: "14|01|05"
                        }
                    },
                    {
                        name: "土默特左旗",
                        attr: {
                            id: "14|01|06"
                        }
                    },
                    {
                        name: "托克托县",
                        attr: {
                            id: "14|01|07"
                        }
                    },
                    {
                        name: "和林格尔县",
                        attr: {
                            id: "14|01|08"
                        }
                    },
                    {
                        name: "清水河县",
                        attr: {
                            id: "14|01|09"
                        }
                    },
                    {
                        name: "武川县",
                        attr: {
                            id: "14|01|10"
                        }
                    }
                ]
            },
            {
                name: "包头",
                attr: {
                    id: "14|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "东河区",
                        attr: {
                            id: "14|02|01"
                        }
                    },
                    {
                        name: "昆都仑区",
                        attr: {
                            id: "14|02|02"
                        }
                    },
                    {
                        name: "青山区",
                        attr: {
                            id: "14|02|03"
                        }
                    },
                    {
                        name: "石拐区",
                        attr: {
                            id: "14|02|04"
                        }
                    },
                    {
                        name: "白云鄂博矿区",
                        attr: {
                            id: "14|02|05"
                        }
                    },
                    {
                        name: "九原区",
                        attr: {
                            id: "14|02|06"
                        }
                    },
                    {
                        name: "土默特右旗",
                        attr: {
                            id: "14|02|07"
                        }
                    },
                    {
                        name: "固阳县",
                        attr: {
                            id: "14|02|08"
                        }
                    },
                    {
                        name: "达尔罕茂明安联合旗",
                        attr: {
                            id: "14|02|09"
                        }
                    }
                ]
            },
            {
                name: "乌海",
                attr: {
                    id: "14|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "海勃湾区",
                        attr: {
                            id: "14|03|01"
                        }
                    },
                    {
                        name: "海南区",
                        attr: {
                            id: "14|03|02"
                        }
                    },
                    {
                        name: "乌达区",
                        attr: {
                            id: "14|03|03"
                        }
                    }
                ]
            },
            {
                name: "赤峰",
                attr: {
                    id: "14|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "红山区",
                        attr: {
                            id: "14|04|01"
                        }
                    },
                    {
                        name: "元宝山区",
                        attr: {
                            id: "14|04|02"
                        }
                    },
                    {
                        name: "松山区",
                        attr: {
                            id: "14|04|03"
                        }
                    },
                    {
                        name: "阿鲁科尔沁旗",
                        attr: {
                            id: "14|04|04"
                        }
                    },
                    {
                        name: "巴林左旗",
                        attr: {
                            id: "14|04|05"
                        }
                    },
                    {
                        name: "巴林右旗",
                        attr: {
                            id: "14|04|06"
                        }
                    },
                    {
                        name: "林西县",
                        attr: {
                            id: "14|04|07"
                        }
                    },
                    {
                        name: "克什克腾旗",
                        attr: {
                            id: "14|04|08"
                        }
                    },
                    {
                        name: "翁牛特旗",
                        attr: {
                            id: "14|04|09"
                        }
                    },
                    {
                        name: "喀喇沁旗",
                        attr: {
                            id: "14|04|10"
                        }
                    },
                    {
                        name: "宁城县",
                        attr: {
                            id: "14|04|11"
                        }
                    },
                    {
                        name: "敖汉旗",
                        attr: {
                            id: "14|04|12"
                        }
                    }
                ]
            },
            {
                name: "通辽",
                attr: {
                    id: "14|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "科尔沁区",
                        attr: {
                            id: "14|05|01"
                        }
                    },
                    {
                        name: "科尔沁左翼中旗",
                        attr: {
                            id: "14|05|02"
                        }
                    },
                    {
                        name: "科尔沁左翼后旗",
                        attr: {
                            id: "14|05|03"
                        }
                    },
                    {
                        name: "开鲁县",
                        attr: {
                            id: "14|05|04"
                        }
                    },
                    {
                        name: "库伦旗",
                        attr: {
                            id: "14|05|05"
                        }
                    },
                    {
                        name: "奈曼旗",
                        attr: {
                            id: "14|05|06"
                        }
                    },
                    {
                        name: "扎鲁特旗",
                        attr: {
                            id: "14|05|07"
                        }
                    },
                    {
                        name: "霍林郭勒市",
                        attr: {
                            id: "14|05|08"
                        }
                    }
                ]
            },
            {
                name: "鄂尔多斯",
                attr: {
                    id: "14|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "东胜区",
                        attr: {
                            id: "14|06|01"
                        }
                    },
                    {
                        name: "达拉特旗",
                        attr: {
                            id: "14|06|02"
                        }
                    },
                    {
                        name: "准格尔旗",
                        attr: {
                            id: "14|06|03"
                        }
                    },
                    {
                        name: "鄂托克前旗",
                        attr: {
                            id: "14|06|04"
                        }
                    },
                    {
                        name: "鄂托克旗",
                        attr: {
                            id: "14|06|05"
                        }
                    },
                    {
                        name: "杭锦旗",
                        attr: {
                            id: "14|06|06"
                        }
                    },
                    {
                        name: "乌审旗",
                        attr: {
                            id: "14|06|07"
                        }
                    },
                    {
                        name: "伊金霍洛旗",
                        attr: {
                            id: "14|06|08"
                        }
                    }
                ]
            },
            {
                name: "呼伦贝尔",
                attr: {
                    id: "14|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "海拉尔区",
                        attr: {
                            id: "14|07|01"
                        }
                    },
                    {
                        name: "阿荣旗",
                        attr: {
                            id: "14|07|02"
                        }
                    },
                    {
                        name: "莫力达瓦达斡尔族自治旗",
                        attr: {
                            id: "14|03|03"
                        }
                    },
                    {
                        name: "鄂伦春自治旗",
                        attr: {
                            id: "14|07|04"
                        }
                    },
                    {
                        name: "鄂温克族自治旗",
                        attr: {
                            id: "14|07|05"
                        }
                    },
                    {
                        name: "陈巴尔虎旗",
                        attr: {
                            id: "14|07|06"
                        }
                    },
                    {
                        name: "新巴尔虎左旗",
                        attr: {
                            id: "14|07|07"
                        }
                    },
                    {
                        name: "新巴尔虎右旗",
                        attr: {
                            id: "14|07|08"
                        }
                    },
                    {
                        name: "满洲里市",
                        attr: {
                            id: "14|07|09"
                        }
                    },
                    {
                        name: "牙克石市",
                        attr: {
                            id: "14|07|10"
                        }
                    },
                    {
                        name: "扎兰屯市",
                        attr: {
                            id: "14|07|11"
                        }
                    },
                    {
                        name: "额尔古纳市",
                        attr: {
                            id: "14|07|12"
                        }
                    },
                    {
                        name: "根河市",
                        attr: {
                            id: "14|07|13"
                        }
                    }
                ]
            },
            {
                name: "巴彦淖尔",
                attr: {
                    id: "14|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "临河区",
                        attr: {
                            id: "14|08|01"
                        }
                    },
                    {
                        name: "五原县",
                        attr: {
                            id: "14|08|02"
                        }
                    },
                    {
                        name: "磴口县",
                        attr: {
                            id: "14|08|03"
                        }
                    },
                    {
                        name: "乌拉特前旗",
                        attr: {
                            id: "14|08|04"
                        }
                    },
                    {
                        name: "乌拉特中旗",
                        attr: {
                            id: "14|08|05"
                        }
                    },
                    {
                        name: "乌拉特后旗",
                        attr: {
                            id: "14|08|06"
                        }
                    },
                    {
                        name: "杭锦后旗",
                        attr: {
                            id: "14|08|07"
                        }
                    }
                ]
            },
            {
                name: "乌兰察布",
                attr: {
                    id: "14|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "集宁区",
                        attr: {
                            id: "14|09|01"
                        }
                    },
                    {
                        name: "卓资县",
                        attr: {
                            id: "14|09|02"
                        }
                    },
                    {
                        name: "化德县",
                        attr: {
                            id: "14|09|03"
                        }
                    },
                    {
                        name: "商都县",
                        attr: {
                            id: "14|09|04"
                        }
                    },
                    {
                        name: "兴和县",
                        attr: {
                            id: "14|09|05"
                        }
                    },
                    {
                        name: "凉城县",
                        attr: {
                            id: "14|09|06"
                        }
                    },
                    {
                        name: "察哈尔右翼前旗",
                        attr: {
                            id: "14|09|07"
                        }
                    },
                    {
                        name: "察哈尔右翼中旗",
                        attr: {
                            id: "14|09|08"
                        }
                    },
                    {
                        name: "察哈尔右翼后旗",
                        attr: {
                            id: "14|09|09"
                        }
                    },
                    {
                        name: "四子王旗",
                        attr: {
                            id: "14|09|10"
                        }
                    },
                    {
                        name: "丰镇市",
                        attr: {
                            id: "14|09|11"
                        }
                    }
                ]
            },
            {
                name: "兴安",
                attr: {
                    id: "14|10|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "乌兰浩特市",
                        attr: {
                            id: "14|10|01"
                        }
                    },
                    {
                        name: "阿尔山市",
                        attr: {
                            id: "14|10|02"
                        }
                    },
                    {
                        name: "科尔沁右翼前旗",
                        attr: {
                            id: "14|10|03"
                        }
                    },
                    {
                        name: "科尔沁右翼中旗",
                        attr: {
                            id: "14|10|04"
                        }
                    },
                    {
                        name: "扎赉特旗",
                        attr: {
                            id: "14|10|05"
                        }
                    },
                    {
                        name: "突泉县",
                        attr: {
                            id: "14|10|06"
                        }
                    }
                ]
            },
            {
                name: "锡林郭勒",
                attr: {
                    id: "14|11|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "二连浩特市",
                        attr: {
                            id: "14|11|01"
                        }
                    },
                    {
                        name: "锡林浩特市",
                        attr: {
                            id: "14|11|02"
                        }
                    },
                    {
                        name: "阿巴嘎旗",
                        attr: {
                            id: "14|11|03"
                        }
                    },
                    {
                        name: "苏尼特左旗",
                        attr: {
                            id: "14|11|04"
                        }
                    },
                    {
                        name: "苏尼特右旗",
                        attr: {
                            id: "14|11|05"
                        }
                    },
                    {
                        name: "东乌珠穆沁旗",
                        attr: {
                            id: "14|11|06"
                        }
                    },
                    {
                        name: "西乌珠穆沁旗",
                        attr: {
                            id: "14|11|07"
                        }
                    },
                    {
                        name: "太仆寺旗",
                        attr: {
                            id: "14|11|08"
                        }
                    },
                    {
                        name: "镶黄旗",
                        attr: {
                            id: "14|11|09"
                        }
                    },
                    {
                        name: "正镶白旗",
                        attr: {
                            id: "14|11|10"
                        }
                    },
                    {
                        name: "正蓝旗",
                        attr: {
                            id: "14|11|11"
                        }
                    },
                    {
                        name: "多伦县",
                        attr: {
                            id: "14|11|12"
                        }
                    }
                ]
            },
            {
                name: "阿拉善",
                attr: {
                    id: "14|12|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "阿拉善左旗",
                        attr: {
                            id: "14|12|01"
                        }
                    },
                    {
                        name: "阿拉善右旗",
                        attr: {
                            id: "14|12|02"
                        }
                    },
                    {
                        name: "额济纳旗",
                        attr: {
                            id: "14|12|03"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "辽宁",
        attr: {
            id: "15|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "沈阳",
                attr: {
                    id: "15|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "和平区",
                        attr: {
                            id: "15|01|01"
                        }
                    },
                    {
                        name: "沈河区",
                        attr: {
                            id: "15|01|02"
                        }
                    },
                    {
                        name: "大东区",
                        attr: {
                            id: "15|01|03"
                        }
                    },
                    {
                        name: "皇姑区",
                        attr: {
                            id: "15|01|05"
                        }
                    },
                    {
                        name: "铁西区",
                        attr: {
                            id: "15|01|06"
                        }
                    },
                    {
                        name: "苏家屯区",
                        attr: {
                            id: "15|01|07"
                        }
                    },
                    {
                        name: "东陵区",
                        attr: {
                            id: "15|01|08"
                        }
                    },
                    {
                        name: "新城子区",
                        attr: {
                            id: "15|01|09"
                        }
                    },
                    {
                        name: "于洪区",
                        attr: {
                            id: "15|01|10"
                        }
                    },
                    {
                        name: "康平县",
                        attr: {
                            id: "15|01|11"
                        }
                    },
                    {
                        name: "法库县",
                        attr: {
                            id: "15|01|12"
                        }
                    },
                    {
                        name: "新民市",
                        attr: {
                            id: "15|01|13"
                        }
                    }
                ]
            },
            {
                name: "大连",
                attr: {
                    id: "15|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "中山区",
                        attr: {
                            id: "15|02|01"
                        }
                    },
                    {
                        name: "西岗区",
                        attr: {
                            id: "15|02|02"
                        }
                    },
                    {
                        name: "沙河口区",
                        attr: {
                            id: "15|02|03"
                        }
                    },
                    {
                        name: "甘井子区",
                        attr: {
                            id: "15|02|04"
                        }
                    },
                    {
                        name: "旅顺口区",
                        attr: {
                            id: "15|02|05"
                        }
                    },
                    {
                        name: "金州区",
                        attr: {
                            id: "15|02|06"
                        }
                    },
                    {
                        name: "长海县",
                        attr: {
                            id: "15|02|07"
                        }
                    },
                    {
                        name: "瓦房店市",
                        attr: {
                            id: "15|02|08"
                        }
                    },
                    {
                        name: "普兰店市",
                        attr: {
                            id: "15|02|09"
                        }
                    },
                    {
                        name: "庄河市",
                        attr: {
                            id: "15|02|10"
                        }
                    }
                ]
            },
            {
                name: "鞍山",
                attr: {
                    id: "15|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "铁东区",
                        attr: {
                            id: "15|03|01"
                        }
                    },
                    {
                        name: "铁西区",
                        attr: {
                            id: "15|03|02"
                        }
                    },
                    {
                        name: "立山区",
                        attr: {
                            id: "15|03|03"
                        }
                    },
                    {
                        name: "千山区",
                        attr: {
                            id: "15|03|04"
                        }
                    },
                    {
                        name: "台安县",
                        attr: {
                            id: "15|03|05"
                        }
                    },
                    {
                        name: "岫岩满族自治县",
                        attr: {
                            id: "15|03|06"
                        }
                    },
                    {
                        name: "海城市",
                        attr: {
                            id: "15|03|07"
                        }
                    }
                ]
            },
            {
                name: "抚顺",
                attr: {
                    id: "15|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "新抚区",
                        attr: {
                            id: "15|04|01"
                        }
                    },
                    {
                        name: "东洲区",
                        attr: {
                            id: "15|04|02"
                        }
                    },
                    {
                        name: "望花区",
                        attr: {
                            id: "15|04|03"
                        }
                    },
                    {
                        name: "顺城区",
                        attr: {
                            id: "15|04|04"
                        }
                    },
                    {
                        name: "抚顺县",
                        attr: {
                            id: "15|04|05"
                        }
                    },
                    {
                        name: "新宾满族自治县",
                        attr: {
                            id: "15|04|06"
                        }
                    },
                    {
                        name: "清原满族自治县",
                        attr: {
                            id: "15|04|07"
                        }
                    }
                ]
            },
            {
                name: "本溪",
                attr: {
                    id: "15|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "平山区",
                        attr: {
                            id: "15|05|01"
                        }
                    },
                    {
                        name: "溪湖区",
                        attr: {
                            id: "15|05|02"
                        }
                    },
                    {
                        name: "明山区",
                        attr: {
                            id: "15|05|03"
                        }
                    },
                    {
                        name: "南芬区",
                        attr: {
                            id: "15|05|04"
                        }
                    },
                    {
                        name: "本溪满族自治县",
                        attr: {
                            id: "15|05|05"
                        }
                    },
                    {
                        name: "桓仁满族自治县",
                        attr: {
                            id: "15|05|06"
                        }
                    }
                ]
            },
            {
                name: "丹东",
                attr: {
                    id: "15|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "元宝区",
                        attr: {
                            id: "15|06|01"
                        }
                    },
                    {
                        name: "振兴区",
                        attr: {
                            id: "15|06|02"
                        }
                    },
                    {
                        name: "振安区",
                        attr: {
                            id: "15|06|03"
                        }
                    },
                    {
                        name: "宽甸满族自治县",
                        attr: {
                            id: "15|06|04"
                        }
                    },
                    {
                        name: "东港市",
                        attr: {
                            id: "15|06|05"
                        }
                    },
                    {
                        name: "凤城市",
                        attr: {
                            id: "15|06|06"
                        }
                    }
                ]
            },
            {
                name: "锦州",
                attr: {
                    id: "15|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "古塔区",
                        attr: {
                            id: "15|07|01"
                        }
                    },
                    {
                        name: "凌河区",
                        attr: {
                            id: "15|07|02"
                        }
                    },
                    {
                        name: "太和区",
                        attr: {
                            id: "15|03|03"
                        }
                    },
                    {
                        name: "黑山县",
                        attr: {
                            id: "15|07|04"
                        }
                    },
                    {
                        name: "义县",
                        attr: {
                            id: "15|07|05"
                        }
                    },
                    {
                        name: "凌海市",
                        attr: {
                            id: "15|07|06"
                        }
                    },
                    {
                        name: "北镇市",
                        attr: {
                            id: "15|07|07"
                        }
                    }
                ]
            },
            {
                name: "营口",
                attr: {
                    id: "15|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "站前区",
                        attr: {
                            id: "15|08|01"
                        }
                    },
                    {
                        name: "西市区",
                        attr: {
                            id: "15|08|02"
                        }
                    },
                    {
                        name: "鲅鱼圈区",
                        attr: {
                            id: "15|08|03"
                        }
                    },
                    {
                        name: "老边区",
                        attr: {
                            id: "15|08|04"
                        }
                    },
                    {
                        name: "盖州市",
                        attr: {
                            id: "15|08|05"
                        }
                    },
                    {
                        name: "大石桥市",
                        attr: {
                            id: "15|08|06"
                        }
                    }
                ]
            },
            {
                name: "阜新",
                attr: {
                    id: "15|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "海州区",
                        attr: {
                            id: "15|09|01"
                        }
                    },
                    {
                        name: "新邱区",
                        attr: {
                            id: "15|09|02"
                        }
                    },
                    {
                        name: "太平区",
                        attr: {
                            id: "15|09|03"
                        }
                    },
                    {
                        name: "清河门区",
                        attr: {
                            id: "15|09|04"
                        }
                    },
                    {
                        name: "细河区",
                        attr: {
                            id: "15|09|05"
                        }
                    },
                    {
                        name: "阜新蒙古族自治县",
                        attr: {
                            id: "15|09|06"
                        }
                    },
                    {
                        name: "彰武县",
                        attr: {
                            id: "15|09|07"
                        }
                    }
                ]
            },
            {
                name: "辽阳",
                attr: {
                    id: "15|10|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "白塔区",
                        attr: {
                            id: "15|10|01"
                        }
                    },
                    {
                        name: "文圣区",
                        attr: {
                            id: "15|10|02"
                        }
                    },
                    {
                        name: "宏伟区",
                        attr: {
                            id: "15|10|03"
                        }
                    },
                    {
                        name: "弓长岭区",
                        attr: {
                            id: "15|10|04"
                        }
                    },
                    {
                        name: "太子河区",
                        attr: {
                            id: "15|10|05"
                        }
                    },
                    {
                        name: "辽阳县",
                        attr: {
                            id: "15|10|06"
                        }
                    },
                    {
                        name: "灯塔市",
                        attr: {
                            id: "15|10|07"
                        }
                    }
                ]
            },
            {
                name: "盘锦",
                attr: {
                    id: "15|11|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "双台子区",
                        attr: {
                            id: "15|11|01"
                        }
                    },
                    {
                        name: "兴隆台区",
                        attr: {
                            id: "15|11|02"
                        }
                    },
                    {
                        name: "大洼县",
                        attr: {
                            id: "15|11|03"
                        }
                    },
                    {
                        name: "盘山县",
                        attr: {
                            id: "15|11|04"
                        }
                    }
                ]
            },
            {
                name: "铁岭",
                attr: {
                    id: "15|12|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "银州区",
                        attr: {
                            id: "15|12|01"
                        }
                    },
                    {
                        name: "清河区",
                        attr: {
                            id: "15|12|02"
                        }
                    },
                    {
                        name: "铁岭县",
                        attr: {
                            id: "15|12|03"
                        }
                    },
                    {
                        name: "西丰县",
                        attr: {
                            id: "15|12|04"
                        }
                    },
                    {
                        name: "昌图县",
                        attr: {
                            id: "15|12|05"
                        }
                    },
                    {
                        name: "调兵山市",
                        attr: {
                            id: "15|12|06"
                        }
                    },
                    {
                        name: "开原市",
                        attr: {
                            id: "15|12|07"
                        }
                    }
                ]
            },
            {
                name: "朝阳",
                attr: {
                    id: "15|13|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "双塔区",
                        attr: {
                            id: "15|13|01"
                        }
                    },
                    {
                        name: "龙城区",
                        attr: {
                            id: "15|13|02"
                        }
                    },
                    {
                        name: "朝阳县",
                        attr: {
                            id: "15|13|03"
                        }
                    },
                    {
                        name: "建平县",
                        attr: {
                            id: "15|13|04"
                        }
                    },
                    {
                        name: "喀喇沁左翼蒙古族自治县",
                        attr: {
                            id: "15|13|05"
                        }
                    },
                    {
                        name: "北票市",
                        attr: {
                            id: "15|13|06"
                        }
                    },
                    {
                        name: "凌源市",
                        attr: {
                            id: "15|13|07"
                        }
                    }
                ]
            },
            {
                name: "葫芦岛",
                attr: {
                    id: "15|14|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "连山区",
                        attr: {
                            id: "15|14|01"
                        }
                    },
                    {
                        name: "龙港区",
                        attr: {
                            id: "15|14|02"
                        }
                    },
                    {
                        name: "南票区",
                        attr: {
                            id: "15|14|03"
                        }
                    },
                    {
                        name: "绥中县",
                        attr: {
                            id: "15|14|04"
                        }
                    },
                    {
                        name: "建昌县",
                        attr: {
                            id: "15|14|05"
                        }
                    },
                    {
                        name: "兴城市",
                        attr: {
                            id: "15|14|06"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "吉林",
        attr: {
            id: "16|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "长春",
                attr: {
                    id: "16|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "南关区",
                        attr: {
                            id: "16|01|01"
                        }
                    },
                    {
                        name: "宽城区",
                        attr: {
                            id: "16|01|02"
                        }
                    },
                    {
                        name: "朝阳区",
                        attr: {
                            id: "16|01|03"
                        }
                    },
                    {
                        name: "二道区",
                        attr: {
                            id: "16|01|04"
                        }
                    },
                    {
                        name: "绿园区",
                        attr: {
                            id: "16|01|05"
                        }
                    },
                    {
                        name: "双阳区",
                        attr: {
                            id: "16|01|06"
                        }
                    },
                    {
                        name: "农安县",
                        attr: {
                            id: "16|01|07"
                        }
                    },
                    {
                        name: "九台市",
                        attr: {
                            id: "16|01|08"
                        }
                    },
                    {
                        name: "榆树市",
                        attr: {
                            id: "16|01|09"
                        }
                    },
                    {
                        name: "德惠市",
                        attr: {
                            id: "16|01|10"
                        }
                    }
                ]
            },
            {
                name: "吉林",
                attr: {
                    id: "16|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "昌邑区",
                        attr: {
                            id: "16|02|01"
                        }
                    },
                    {
                        name: "龙潭区",
                        attr: {
                            id: "16|02|02"
                        }
                    },
                    {
                        name: "船营区",
                        attr: {
                            id: "16|02|03"
                        }
                    },
                    {
                        name: "丰满区",
                        attr: {
                            id: "16|02|04"
                        }
                    },
                    {
                        name: "永吉县",
                        attr: {
                            id: "16|02|05"
                        }
                    },
                    {
                        name: "蛟河市",
                        attr: {
                            id: "16|02|06"
                        }
                    },
                    {
                        name: "桦甸市",
                        attr: {
                            id: "16|02|07"
                        }
                    },
                    {
                        name: "舒兰市",
                        attr: {
                            id: "16|02|08"
                        }
                    },
                    {
                        name: "磐石市",
                        attr: {
                            id: "16|02|09"
                        }
                    }
                ]
            },
            {
                name: "四平",
                attr: {
                    id: "16|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "铁西区",
                        attr: {
                            id: "16|03|01"
                        }
                    },
                    {
                        name: "铁东区",
                        attr: {
                            id: "16|03|02"
                        }
                    },
                    {
                        name: "梨树县",
                        attr: {
                            id: "16|03|03"
                        }
                    },
                    {
                        name: "伊通满族自治县",
                        attr: {
                            id: "16|03|04"
                        }
                    },
                    {
                        name: "公主岭市",
                        attr: {
                            id: "16|03|05"
                        }
                    },
                    {
                        name: "双辽市",
                        attr: {
                            id: "16|03|06"
                        }
                    }
                ]
            },
            {
                name: "辽源",
                attr: {
                    id: "16|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "龙山区",
                        attr: {
                            id: "16|04|01"
                        }
                    },
                    {
                        name: "西安区",
                        attr: {
                            id: "16|04|02"
                        }
                    },
                    {
                        name: "东丰县",
                        attr: {
                            id: "16|04|03"
                        }
                    },
                    {
                        name: "东辽县",
                        attr: {
                            id: "16|04|04"
                        }
                    }
                ]
            },
            {
                name: "通化",
                attr: {
                    id: "16|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "东昌区",
                        attr: {
                            id: "16|05|01"
                        }
                    },
                    {
                        name: "二道江区",
                        attr: {
                            id: "16|05|02"
                        }
                    },
                    {
                        name: "通化县",
                        attr: {
                            id: "16|05|03"
                        }
                    },
                    {
                        name: "辉南县",
                        attr: {
                            id: "16|05|04"
                        }
                    },
                    {
                        name: "柳河县",
                        attr: {
                            id: "16|05|05"
                        }
                    },
                    {
                        name: "梅河口市",
                        attr: {
                            id: "16|05|06"
                        }
                    },
                    {
                        name: "集安市",
                        attr: {
                            id: "16|05|06"
                        }
                    }
                ]
            },
            {
                name: "白山",
                attr: {
                    id: "16|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "八道江区",
                        attr: {
                            id: "16|06|01"
                        }
                    },
                    {
                        name: "江源区",
                        attr: {
                            id: "16|06|02"
                        }
                    },
                    {
                        name: "抚松县",
                        attr: {
                            id: "16|06|03"
                        }
                    },
                    {
                        name: "靖宇县",
                        attr: {
                            id: "16|06|04"
                        }
                    },
                    {
                        name: "长白朝鲜族自治县",
                        attr: {
                            id: "16|06|05"
                        }
                    },
                    {
                        name: "临江市",
                        attr: {
                            id: "16|06|06"
                        }
                    }
                ]
            },
            {
                name: "松原",
                attr: {
                    id: "16|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "宁江区",
                        attr: {
                            id: "16|07|01"
                        }
                    },
                    {
                        name: "前郭尔罗斯蒙古族自治县",
                        attr: {
                            id: "16|07|02"
                        }
                    },
                    {
                        name: "长岭县",
                        attr: {
                            id: "16|03|03"
                        }
                    },
                    {
                        name: "乾安县",
                        attr: {
                            id: "16|07|04"
                        }
                    },
                    {
                        name: "扶余县",
                        attr: {
                            id: "16|07|05"
                        }
                    }
                ]
            },
            {
                name: "白城",
                attr: {
                    id: "16|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "洮北区",
                        attr: {
                            id: "16|08|01"
                        }
                    },
                    {
                        name: "镇赉县",
                        attr: {
                            id: "16|08|02"
                        }
                    },
                    {
                        name: "通榆县",
                        attr: {
                            id: "16|08|03"
                        }
                    },
                    {
                        name: "洮南市",
                        attr: {
                            id: "16|08|04"
                        }
                    },
                    {
                        name: "大安市",
                        attr: {
                            id: "16|08|05"
                        }
                    }
                ]
            },
            {
                name: "延边",
                attr: {
                    id: "16|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "延吉市",
                        attr: {
                            id: "16|09|01"
                        }
                    },
                    {
                        name: "图们市",
                        attr: {
                            id: "16|09|02"
                        }
                    },
                    {
                        name: "敦化市",
                        attr: {
                            id: "16|09|03"
                        }
                    },
                    {
                        name: "珲春市",
                        attr: {
                            id: "16|09|04"
                        }
                    },
                    {
                        name: "龙井市",
                        attr: {
                            id: "16|09|05"
                        }
                    },
                    {
                        name: "和龙市",
                        attr: {
                            id: "16|09|06"
                        }
                    },
                    {
                        name: "汪清县",
                        attr: {
                            id: "16|09|07"
                        }
                    },
                    {
                        name: "安图县",
                        attr: {
                            id: "16|09|08"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "黑龙江",
        attr: {
            id: "17|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "哈尔滨",
                attr: {
                    id: "17|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "道里区",
                        attr: {
                            id: "17|01|01"
                        }
                    },
                    {
                        name: "南岗区",
                        attr: {
                            id: "17|01|02"
                        }
                    },
                    {
                        name: "道外区",
                        attr: {
                            id: "17|01|03"
                        }
                    },
                    {
                        name: "平房区",
                        attr: {
                            id: "17|01|04"
                        }
                    },
                    {
                        name: "松北区",
                        attr: {
                            id: "17|01|05"
                        }
                    },
                    {
                        name: "香坊区",
                        attr: {
                            id: "17|01|06"
                        }
                    },
                    {
                        name: "呼兰区",
                        attr: {
                            id: "17|01|07"
                        }
                    },
                    {
                        name: "阿城区",
                        attr: {
                            id: "17|01|08"
                        }
                    },
                    {
                        name: "依兰县",
                        attr: {
                            id: "17|01|09"
                        }
                    },
                    {
                        name: "方正县",
                        attr: {
                            id: "17|01|10"
                        }
                    },
                    {
                        name: "宾县",
                        attr: {
                            id: "17|01|11"
                        }
                    },
                    {
                        name: "巴彦县",
                        attr: {
                            id: "17|01|12"
                        }
                    },
                    {
                        name: "木兰县",
                        attr: {
                            id: "17|01|13"
                        }
                    },
                    {
                        name: "通河县",
                        attr: {
                            id: "17|01|14"
                        }
                    },
                    {
                        name: "延寿县",
                        attr: {
                            id: "17|01|15"
                        }
                    },
                    {
                        name: "双城市",
                        attr: {
                            id: "17|01|16"
                        }
                    },
                    {
                        name: "尚志市",
                        attr: {
                            id: "17|01|17"
                        }
                    },
                    {
                        name: "五常市",
                        attr: {
                            id: "17|01|18"
                        }
                    }
                ]
            },
            {
                name: "齐齐哈尔",
                attr: {
                    id: "17|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "龙沙区",
                        attr: {
                            id: "17|02|01"
                        }
                    },
                    {
                        name: "建华区",
                        attr: {
                            id: "17|02|02"
                        }
                    },
                    {
                        name: "铁锋区",
                        attr: {
                            id: "17|02|03"
                        }
                    },
                    {
                        name: "昂昂溪区",
                        attr: {
                            id: "17|02|04"
                        }
                    },
                    {
                        name: "富拉尔基区",
                        attr: {
                            id: "17|02|05"
                        }
                    },
                    {
                        name: "碾子山区",
                        attr: {
                            id: "17|02|06"
                        }
                    },
                    {
                        name: "梅里斯达斡尔族区",
                        attr: {
                            id: "17|02|07"
                        }
                    },
                    {
                        name: "龙江县",
                        attr: {
                            id: "17|02|08"
                        }
                    },
                    {
                        name: "依安县",
                        attr: {
                            id: "17|02|09"
                        }
                    },
                    {
                        name: "泰来县",
                        attr: {
                            id: "17|02|10"
                        }
                    },
                    {
                        name: "甘南县",
                        attr: {
                            id: "17|02|11"
                        }
                    },
                    {
                        name: "富裕县",
                        attr: {
                            id: "17|02|12"
                        }
                    },
                    {
                        name: "克山县",
                        attr: {
                            id: "17|02|13"
                        }
                    },
                    {
                        name: "克东县",
                        attr: {
                            id: "17|02|14"
                        }
                    },
                    {
                        name: "拜泉县",
                        attr: {
                            id: "17|02|15"
                        }
                    },
                    {
                        name: "讷河市",
                        attr: {
                            id: "17|02|16"
                        }
                    }
                ]
            },
            {
                name: "鸡西",
                attr: {
                    id: "17|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "鸡冠区",
                        attr: {
                            id: "17|03|01"
                        }
                    },
                    {
                        name: "恒山区",
                        attr: {
                            id: "17|03|02"
                        }
                    },
                    {
                        name: "滴道区",
                        attr: {
                            id: "17|03|03"
                        }
                    },
                    {
                        name: "梨树区",
                        attr: {
                            id: "17|03|04"
                        }
                    },
                    {
                        name: "城子河区",
                        attr: {
                            id: "17|03|05"
                        }
                    },
                    {
                        name: "麻山区",
                        attr: {
                            id: "17|03|06"
                        }
                    },
                    {
                        name: "鸡东县",
                        attr: {
                            id: "17|03|07"
                        }
                    },
                    {
                        name: "虎林市",
                        attr: {
                            id: "17|03|08"
                        }
                    },
                    {
                        name: "密山市",
                        attr: {
                            id: "17|03|09"
                        }
                    }
                ]
            },
            {
                name: "鹤岗",
                attr: {
                    id: "17|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "向阳区",
                        attr: {
                            id: "17|04|01"
                        }
                    },
                    {
                        name: "工农区",
                        attr: {
                            id: "17|04|02"
                        }
                    },
                    {
                        name: "南山区",
                        attr: {
                            id: "17|04|03"
                        }
                    },
                    {
                        name: "兴安区",
                        attr: {
                            id: "17|04|04"
                        }
                    },
                    {
                        name: "东山区",
                        attr: {
                            id: "17|04|05"
                        }
                    },
                    {
                        name: "兴山区",
                        attr: {
                            id: "17|04|06"
                        }
                    },
                    {
                        name: "萝北县",
                        attr: {
                            id: "17|04|07"
                        }
                    },
                    {
                        name: "绥滨县",
                        attr: {
                            id: "17|04|08"
                        }
                    }
                ]
            },
            {
                name: "双鸭山",
                attr: {
                    id: "17|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "尖山区",
                        attr: {
                            id: "17|05|01"
                        }
                    },
                    {
                        name: "岭东区",
                        attr: {
                            id: "17|05|02"
                        }
                    },
                    {
                        name: "四方台区",
                        attr: {
                            id: "17|05|03"
                        }
                    },
                    {
                        name: "宝山区",
                        attr: {
                            id: "17|05|04"
                        }
                    },
                    {
                        name: "集贤县",
                        attr: {
                            id: "17|05|05"
                        }
                    },
                    {
                        name: "友谊县",
                        attr: {
                            id: "17|05|06"
                        }
                    },
                    {
                        name: "宝清县",
                        attr: {
                            id: "17|05|07"
                        }
                    },
                    {
                        name: "饶河县",
                        attr: {
                            id: "17|05|08"
                        }
                    }
                ]
            },
            {
                name: "大庆",
                attr: {
                    id: "17|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "萨尔图区",
                        attr: {
                            id: "17|06|01"
                        }
                    },
                    {
                        name: "龙凤区",
                        attr: {
                            id: "17|06|02"
                        }
                    },
                    {
                        name: "让胡路区",
                        attr: {
                            id: "17|06|03"
                        }
                    },
                    {
                        name: "红岗区",
                        attr: {
                            id: "17|06|04"
                        }
                    },
                    {
                        name: "大同区",
                        attr: {
                            id: "17|06|05"
                        }
                    },
                    {
                        name: "肇州县",
                        attr: {
                            id: "17|06|06"
                        }
                    },
                    {
                        name: "肇源县",
                        attr: {
                            id: "17|06|07"
                        }
                    },
                    {
                        name: "林甸县",
                        attr: {
                            id: "17|06|08"
                        }
                    },
                    {
                        name: "杜尔伯特蒙古族自治县",
                        attr: {
                            id: "17|06|09"
                        }
                    }
                ]
            },
            {
                name: "伊春",
                attr: {
                    id: "17|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "伊春区",
                        attr: {
                            id: "17|07|01"
                        }
                    },
                    {
                        name: "南岔区",
                        attr: {
                            id: "17|07|02"
                        }
                    },
                    {
                        name: "友好区",
                        attr: {
                            id: "17|03|03"
                        }
                    },
                    {
                        name: "西林区",
                        attr: {
                            id: "17|07|04"
                        }
                    },
                    {
                        name: "翠峦区",
                        attr: {
                            id: "17|07|05"
                        }
                    },
                    {
                        name: "新青区",
                        attr: {
                            id: "17|07|06"
                        }
                    },
                    {
                        name: "美溪区",
                        attr: {
                            id: "17|07|07"
                        }
                    },
                    {
                        name: "金山屯区",
                        attr: {
                            id: "17|07|08"
                        }
                    },
                    {
                        name: "五营区",
                        attr: {
                            id: "17|07|09"
                        }
                    },
                    {
                        name: "乌马河区",
                        attr: {
                            id: "17|07|10"
                        }
                    },
                    {
                        name: "汤旺河区",
                        attr: {
                            id: "17|07|11"
                        }
                    },
                    {
                        name: "带岭区",
                        attr: {
                            id: "17|07|12"
                        }
                    },
                    {
                        name: "乌伊岭区",
                        attr: {
                            id: "17|07|13"
                        }
                    },
                    {
                        name: "红星区",
                        attr: {
                            id: "17|07|14"
                        }
                    },
                    {
                        name: "上甘岭区",
                        attr: {
                            id: "17|07|15"
                        }
                    },
                    {
                        name: "嘉荫县",
                        attr: {
                            id: "17|07|16"
                        }
                    },
                    {
                        name: "铁力市",
                        attr: {
                            id: "17|07|17"
                        }
                    }
                ]
            },
            {
                name: "佳木斯",
                attr: {
                    id: "17|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "向阳区",
                        attr: {
                            id: "17|08|01"
                        }
                    },
                    {
                        name: "前进区",
                        attr: {
                            id: "17|08|02"
                        }
                    },
                    {
                        name: "东风区",
                        attr: {
                            id: "17|08|03"
                        }
                    },
                    {
                        name: "郊区",
                        attr: {
                            id: "17|08|04"
                        }
                    },
                    {
                        name: "桦南县",
                        attr: {
                            id: "17|08|05"
                        }
                    },
                    {
                        name: "桦川县",
                        attr: {
                            id: "17|08|06"
                        }
                    },
                    {
                        name: "汤原县",
                        attr: {
                            id: "17|08|07"
                        }
                    },
                    {
                        name: "抚远县",
                        attr: {
                            id: "17|08|08"
                        }
                    },
                    {
                        name: "同江市",
                        attr: {
                            id: "17|08|09"
                        }
                    },
                    {
                        name: "富锦市",
                        attr: {
                            id: "17|08|10"
                        }
                    }
                ]
            },
            {
                name: "七台河",
                attr: {
                    id: "17|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "新兴区",
                        attr: {
                            id: "17|09|01"
                        }
                    },
                    {
                        name: "桃山区",
                        attr: {
                            id: "17|09|02"
                        }
                    },
                    {
                        name: "茄子河区",
                        attr: {
                            id: "17|09|03"
                        }
                    },
                    {
                        name: "勃利县",
                        attr: {
                            id: "17|09|04"
                        }
                    }
                ]
            },
            {
                name: "牡丹江",
                attr: {
                    id: "17|10|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "东安区",
                        attr: {
                            id: "17|10|01"
                        }
                    },
                    {
                        name: "阳明区",
                        attr: {
                            id: "17|10|02"
                        }
                    },
                    {
                        name: "爱民区",
                        attr: {
                            id: "17|10|03"
                        }
                    },
                    {
                        name: "西安区",
                        attr: {
                            id: "17|10|04"
                        }
                    },
                    {
                        name: "东宁县",
                        attr: {
                            id: "17|10|05"
                        }
                    },
                    {
                        name: "林口县",
                        attr: {
                            id: "17|10|06"
                        }
                    },
                    {
                        name: "绥芬河市",
                        attr: {
                            id: "17|10|07"
                        }
                    },
                    {
                        name: "海林市",
                        attr: {
                            id: "17|10|08"
                        }
                    },
                    {
                        name: "宁安市",
                        attr: {
                            id: "17|10|09"
                        }
                    },
                    {
                        name: "穆棱市",
                        attr: {
                            id: "17|10|10"
                        }
                    }
                ]
            },
            {
                name: "黑河",
                attr: {
                    id: "17|11|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "爱辉区",
                        attr: {
                            id: "17|11|01"
                        }
                    },
                    {
                        name: "嫩江县",
                        attr: {
                            id: "17|11|02"
                        }
                    },
                    {
                        name: "逊克县",
                        attr: {
                            id: "17|11|03"
                        }
                    },
                    {
                        name: "孙吴县",
                        attr: {
                            id: "17|11|04"
                        }
                    },
                    {
                        name: "北安市",
                        attr: {
                            id: "17|11|05"
                        }
                    },
                    {
                        name: "五大连池市",
                        attr: {
                            id: "17|11|06"
                        }
                    }
                ]
            },
            {
                name: "绥化",
                attr: {
                    id: "17|12|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "北林区",
                        attr: {
                            id: "17|12|01"
                        }
                    },
                    {
                        name: "望奎县",
                        attr: {
                            id: "17|12|02"
                        }
                    },
                    {
                        name: "兰西县",
                        attr: {
                            id: "17|12|03"
                        }
                    },
                    {
                        name: "青冈县",
                        attr: {
                            id: "17|12|04"
                        }
                    },
                    {
                        name: "庆安县",
                        attr: {
                            id: "17|12|05"
                        }
                    },
                    {
                        name: "明水县",
                        attr: {
                            id: "17|12|06"
                        }
                    },
                    {
                        name: "绥棱县",
                        attr: {
                            id: "17|12|07"
                        }
                    },
                    {
                        name: "安达市",
                        attr: {
                            id: "17|12|08"
                        }
                    },
                    {
                        name: "肇东市",
                        attr: {
                            id: "17|12|09"
                        }
                    },
                    {
                        name: "海伦市",
                        attr: {
                            id: "17|12|10"
                        }
                    }
                ]
            },
            {
                name: "大兴安岭",
                attr: {
                    id: "17|13|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "加格达奇区",
                        attr: {
                            id: "17|13|01"
                        }
                    },
                    {
                        name: "松岭区",
                        attr: {
                            id: "17|13|02"
                        }
                    },
                    {
                        name: "新林区",
                        attr: {
                            id: "17|13|03"
                        }
                    },
                    {
                        name: "呼中区",
                        attr: {
                            id: "17|13|04"
                        }
                    },
                    {
                        name: "呼玛县",
                        attr: {
                            id: "17|13|05"
                        }
                    },
                    {
                        name: "塔河县",
                        attr: {
                            id: "17|13|06"
                        }
                    },
                    {
                        name: "漠河县",
                        attr: {
                            id: "17|13|07"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "上海",
        attr: {
            id: "18|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "黄浦区",
                attr: {
                    id: "18|00|01"
                }
            },
            {
                name: "徐汇区",
                attr: {
                    id: "18|00|03"
                }
            },
            {
                name: "长宁区",
                attr: {
                    id: "18|00|04"
                }
            },
            {
                name: "静安区",
                attr: {
                    id: "18|00|05"
                }
            },
            {
                name: "普陀区",
                attr: {
                    id: "18|00|06"
                }
            },
            {
                name: "闸北区",
                attr: {
                    id: "18|00|07"
                }
            },
            {
                name: "虹口区",
                attr: {
                    id: "18|00|08"
                }
            },
            {
                name: "杨浦区",
                attr: {
                    id: "18|00|09"
                }
            },
            {
                name: "闵行区",
                attr: {
                    id: "18|00|10"
                }
            },
            {
                name: "宝山区",
                attr: {
                    id: "18|00|11"
                }
            },
            {
                name: "嘉定区",
                attr: {
                    id: "18|00|12"
                }
            },
            {
                name: "浦东新区",
                attr: {
                    id: "18|00|13"
                }
            },
            {
                name: "金山区",
                attr: {
                    id: "18|00|14"
                }
            },
            {
                name: "松江区",
                attr: {
                    id: "18|00|15"
                }
            },
            {
                name: "奉贤区",
                attr: {
                    id: "18|00|16"
                }
            },
            {
                name: "青浦区",
                attr: {
                    id: "18|00|17"
                }
            },
            {
                name: "崇明县",
                attr: {
                    id: "18|00|18"
                }
            }
        ]
    },
    {
        name: "江苏",
        attr: {
            id: "19|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "南京",
                attr: {
                    id: "19|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "玄武区",
                        attr: {
                            id: "19|01|01"
                        }
                    },
                    {
                        name: "秦淮区",
                        attr: {
                            id: "19|01|03"
                        }
                    },
                    {
                        name: "建邺区",
                        attr: {
                            id: "19|01|04"
                        }
                    },
                    {
                        name: "鼓楼区",
                        attr: {
                            id: "19|01|05"
                        }
                    },
                    {
                        name: "浦口区",
                        attr: {
                            id: "19|01|07"
                        }
                    },
                    {
                        name: "栖霞区",
                        attr: {
                            id: "19|01|08"
                        }
                    },
                    {
                        name: "雨花台区",
                        attr: {
                            id: "19|01|09"
                        }
                    },
                    {
                        name: "江宁区",
                        attr: {
                            id: "19|01|10"
                        }
                    },
                    {
                        name: "六合区",
                        attr: {
                            id: "19|01|11"
                        }
                    },
                    {
                        name: "溧水区",
                        attr: {
                            id: "19|01|12"
                        }
                    },
                    {
                        name: "高淳区",
                        attr: {
                            id: "19|01|13"
                        }
                    }
                ]
            },
            {
                name: "无锡",
                attr: {
                    id: "19|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "崇安区",
                        attr: {
                            id: "19|02|01"
                        }
                    },
                    {
                        name: "南长区",
                        attr: {
                            id: "19|02|02"
                        }
                    },
                    {
                        name: "北塘区",
                        attr: {
                            id: "19|02|03"
                        }
                    },
                    {
                        name: "锡山区",
                        attr: {
                            id: "19|02|04"
                        }
                    },
                    {
                        name: "惠山区",
                        attr: {
                            id: "19|02|05"
                        }
                    },
                    {
                        name: "滨湖区",
                        attr: {
                            id: "19|02|06"
                        }
                    },
                    {
                        name: "江阴市",
                        attr: {
                            id: "19|02|07"
                        }
                    },
                    {
                        name: "宜兴市",
                        attr: {
                            id: "19|02|08"
                        }
                    }
                ]
            },
            {
                name: "徐州",
                attr: {
                    id: "19|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "鼓楼区",
                        attr: {
                            id: "19|03|01"
                        }
                    },
                    {
                        name: "云龙区",
                        attr: {
                            id: "19|03|02"
                        }
                    },
                    {
                        name: "九里区",
                        attr: {
                            id: "19|03|03"
                        }
                    },
                    {
                        name: "贾汪区",
                        attr: {
                            id: "19|03|04"
                        }
                    },
                    {
                        name: "泉山区",
                        attr: {
                            id: "19|03|05"
                        }
                    },
                    {
                        name: "丰县",
                        attr: {
                            id: "19|03|06"
                        }
                    },
                    {
                        name: "沛县",
                        attr: {
                            id: "19|03|07"
                        }
                    },
                    {
                        name: "铜山县",
                        attr: {
                            id: "19|03|08"
                        }
                    },
                    {
                        name: "睢宁县",
                        attr: {
                            id: "19|03|09"
                        }
                    },
                    {
                        name: "新沂市",
                        attr: {
                            id: "19|03|10"
                        }
                    },
                    {
                        name: "邳州市",
                        attr: {
                            id: "19|03|11"
                        }
                    }
                ]
            },
            {
                name: "常州",
                attr: {
                    id: "19|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "天宁区",
                        attr: {
                            id: "19|04|01"
                        }
                    },
                    {
                        name: "钟楼区",
                        attr: {
                            id: "19|04|02"
                        }
                    },
                    {
                        name: "戚墅堰区",
                        attr: {
                            id: "19|04|03"
                        }
                    },
                    {
                        name: "新北区",
                        attr: {
                            id: "19|04|04"
                        }
                    },
                    {
                        name: "武进区",
                        attr: {
                            id: "19|04|05"
                        }
                    },
                    {
                        name: "溧阳市",
                        attr: {
                            id: "19|04|06"
                        }
                    },
                    {
                        name: "金坛市",
                        attr: {
                            id: "19|04|07"
                        }
                    }
                ]
            },
            {
                name: "苏州",
                attr: {
                    id: "19|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "姑苏区",
                        attr: {
                            id: "19|05|01"
                        }
                    },
                    {
                        name: "虎丘区",
                        attr: {
                            id: "19|05|04"
                        }
                    },
                    {
                        name: "吴中区",
                        attr: {
                            id: "19|05|05"
                        }
                    },
                    {
                        name: "相城区",
                        attr: {
                            id: "19|05|06"
                        }
                    },
                    {
                        name: "常熟市",
                        attr: {
                            id: "19|05|07"
                        }
                    },
                    {
                        name: "张家港市",
                        attr: {
                            id: "19|05|08"
                        }
                    },
                    {
                        name: "昆山市",
                        attr: {
                            id: "19|05|09"
                        }
                    },
                    {
                        name: "吴江市",
                        attr: {
                            id: "19|05|10"
                        }
                    },
                    {
                        name: "太仓市",
                        attr: {
                            id: "19|05|11"
                        }
                    }
                ]
            },
            {
                name: "南通",
                attr: {
                    id: "19|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "崇川区",
                        attr: {
                            id: "19|06|01"
                        }
                    },
                    {
                        name: "港闸区",
                        attr: {
                            id: "19|06|02"
                        }
                    },
                    {
                        name: "海安县",
                        attr: {
                            id: "19|06|03"
                        }
                    },
                    {
                        name: "如东县",
                        attr: {
                            id: "19|06|04"
                        }
                    },
                    {
                        name: "启东市",
                        attr: {
                            id: "19|06|05"
                        }
                    },
                    {
                        name: "如皋市",
                        attr: {
                            id: "19|06|06"
                        }
                    },
                    {
                        name: "通州市",
                        attr: {
                            id: "19|06|07"
                        }
                    },
                    {
                        name: "海门市",
                        attr: {
                            id: "19|06|08"
                        }
                    }
                ]
            },
            {
                name: "连云港",
                attr: {
                    id: "19|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "连云区",
                        attr: {
                            id: "19|07|01"
                        }
                    },
                    {
                        name: "新浦区",
                        attr: {
                            id: "19|07|02"
                        }
                    },
                    {
                        name: "海州区",
                        attr: {
                            id: "19|03|03"
                        }
                    },
                    {
                        name: "赣榆县",
                        attr: {
                            id: "19|07|04"
                        }
                    },
                    {
                        name: "东海县",
                        attr: {
                            id: "19|07|05"
                        }
                    },
                    {
                        name: "灌云县",
                        attr: {
                            id: "19|07|06"
                        }
                    },
                    {
                        name: "灌南县",
                        attr: {
                            id: "19|07|07"
                        }
                    }
                ]
            },
            {
                name: "淮安",
                attr: {
                    id: "19|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "清河区",
                        attr: {
                            id: "19|08|01"
                        }
                    },
                    {
                        name: "楚州区",
                        attr: {
                            id: "19|08|02"
                        }
                    },
                    {
                        name: "淮阴区",
                        attr: {
                            id: "19|08|03"
                        }
                    },
                    {
                        name: "清浦区",
                        attr: {
                            id: "19|08|04"
                        }
                    },
                    {
                        name: "涟水县",
                        attr: {
                            id: "19|08|05"
                        }
                    },
                    {
                        name: "洪泽县",
                        attr: {
                            id: "19|08|06"
                        }
                    },
                    {
                        name: "盱眙县",
                        attr: {
                            id: "19|08|07"
                        }
                    },
                    {
                        name: "金湖县",
                        attr: {
                            id: "19|08|08"
                        }
                    }
                ]
            },
            {
                name: "盐城",
                attr: {
                    id: "19|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "亭湖区",
                        attr: {
                            id: "19|09|01"
                        }
                    },
                    {
                        name: "盐都区",
                        attr: {
                            id: "19|09|02"
                        }
                    },
                    {
                        name: "响水县",
                        attr: {
                            id: "19|09|03"
                        }
                    },
                    {
                        name: "滨海县",
                        attr: {
                            id: "19|09|04"
                        }
                    },
                    {
                        name: "阜宁县",
                        attr: {
                            id: "19|09|05"
                        }
                    },
                    {
                        name: "射阳县",
                        attr: {
                            id: "19|09|06"
                        }
                    },
                    {
                        name: "建湖县",
                        attr: {
                            id: "19|09|07"
                        }
                    },
                    {
                        name: "东台市",
                        attr: {
                            id: "19|09|08"
                        }
                    },
                    {
                        name: "大丰市",
                        attr: {
                            id: "19|09|09"
                        }
                    }
                ]
            },
            {
                name: "扬州",
                attr: {
                    id: "19|10|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "广陵区",
                        attr: {
                            id: "19|10|01"
                        }
                    },
                    {
                        name: "邗江区",
                        attr: {
                            id: "19|10|02"
                        }
                    },
                    {
                        name: "宝应县",
                        attr: {
                            id: "19|10|04"
                        }
                    },
                    {
                        name: "仪征市",
                        attr: {
                            id: "19|10|05"
                        }
                    },
                    {
                        name: "高邮市",
                        attr: {
                            id: "19|10|06"
                        }
                    },
                    {
                        name: "江都区",
                        attr: {
                            id: "19|10|07"
                        }
                    }
                ]
            },
            {
                name: "镇江",
                attr: {
                    id: "19|11|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "京口区",
                        attr: {
                            id: "19|11|01"
                        }
                    },
                    {
                        name: "润州区",
                        attr: {
                            id: "19|11|02"
                        }
                    },
                    {
                        name: "丹徒区",
                        attr: {
                            id: "19|11|03"
                        }
                    },
                    {
                        name: "丹阳市",
                        attr: {
                            id: "19|11|04"
                        }
                    },
                    {
                        name: "扬中市",
                        attr: {
                            id: "19|11|05"
                        }
                    },
                    {
                        name: "句容市",
                        attr: {
                            id: "19|11|06"
                        }
                    }
                ]
            },
            {
                name: "泰州",
                attr: {
                    id: "19|12|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "海陵区",
                        attr: {
                            id: "19|12|01"
                        }
                    },
                    {
                        name: "高港区",
                        attr: {
                            id: "19|12|02"
                        }
                    },
                    {
                        name: "兴化市",
                        attr: {
                            id: "19|12|03"
                        }
                    },
                    {
                        name: "靖江市",
                        attr: {
                            id: "19|12|04"
                        }
                    },
                    {
                        name: "泰兴市",
                        attr: {
                            id: "19|12|05"
                        }
                    },
                    {
                        name: "姜堰区",
                        attr: {
                            id: "19|12|06"
                        }
                    }
                ]
            },
            {
                name: "宿迁",
                attr: {
                    id: "19|13|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "宿城区",
                        attr: {
                            id: "19|13|01"
                        }
                    },
                    {
                        name: "宿豫区",
                        attr: {
                            id: "19|13|02"
                        }
                    },
                    {
                        name: "沭阳县",
                        attr: {
                            id: "19|13|03"
                        }
                    },
                    {
                        name: "泗阳县",
                        attr: {
                            id: "19|13|04"
                        }
                    },
                    {
                        name: "泗洪县",
                        attr: {
                            id: "19|13|05"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "浙江",
        attr: {
            id: "20|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "杭州",
                attr: {
                    id: "20|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "上城区",
                        attr: {
                            id: "20|01|01"
                        }
                    },
                    {
                        name: "下城区",
                        attr: {
                            id: "20|01|02"
                        }
                    },
                    {
                        name: "江干区",
                        attr: {
                            id: "20|01|03"
                        }
                    },
                    {
                        name: "拱墅区",
                        attr: {
                            id: "20|01|04"
                        }
                    },
                    {
                        name: "西湖区",
                        attr: {
                            id: "20|01|05"
                        }
                    },
                    {
                        name: "滨江区",
                        attr: {
                            id: "20|01|06"
                        }
                    },
                    {
                        name: "萧山区",
                        attr: {
                            id: "20|01|07"
                        }
                    },
                    {
                        name: "余杭区",
                        attr: {
                            id: "20|01|08"
                        }
                    },
                    {
                        name: "桐庐县",
                        attr: {
                            id: "20|01|09"
                        }
                    },
                    {
                        name: "淳安县",
                        attr: {
                            id: "20|01|10"
                        }
                    },
                    {
                        name: "建德市",
                        attr: {
                            id: "20|01|11"
                        }
                    },
                    {
                        name: "富阳市",
                        attr: {
                            id: "20|01|12"
                        }
                    },
                    {
                        name: "临安市",
                        attr: {
                            id: "20|01|13"
                        }
                    }
                ]
            },
            {
                name: "宁波",
                attr: {
                    id: "20|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "海曙区",
                        attr: {
                            id: "20|02|01"
                        }
                    },
                    {
                        name: "江东区",
                        attr: {
                            id: "20|02|02"
                        }
                    },
                    {
                        name: "江北区",
                        attr: {
                            id: "20|02|03"
                        }
                    },
                    {
                        name: "北仑区",
                        attr: {
                            id: "20|02|04"
                        }
                    },
                    {
                        name: "镇海区",
                        attr: {
                            id: "20|02|05"
                        }
                    },
                    {
                        name: "鄞州区",
                        attr: {
                            id: "20|02|06"
                        }
                    },
                    {
                        name: "象山县",
                        attr: {
                            id: "20|02|07"
                        }
                    },
                    {
                        name: "宁海县",
                        attr: {
                            id: "20|02|08"
                        }
                    },
                    {
                        name: "余姚市",
                        attr: {
                            id: "20|02|09"
                        }
                    },
                    {
                        name: "慈溪市",
                        attr: {
                            id: "20|02|10"
                        }
                    },
                    {
                        name: "奉化市",
                        attr: {
                            id: "20|02|11"
                        }
                    }
                ]
            },
            {
                name: "温州",
                attr: {
                    id: "20|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "鹿城区",
                        attr: {
                            id: "20|03|01"
                        }
                    },
                    {
                        name: "龙湾区",
                        attr: {
                            id: "20|03|02"
                        }
                    },
                    {
                        name: "瓯海区",
                        attr: {
                            id: "20|03|03"
                        }
                    },
                    {
                        name: "洞头县",
                        attr: {
                            id: "20|03|04"
                        }
                    },
                    {
                        name: "永嘉县",
                        attr: {
                            id: "20|03|05"
                        }
                    },
                    {
                        name: "平阳县",
                        attr: {
                            id: "20|03|06"
                        }
                    },
                    {
                        name: "苍南县",
                        attr: {
                            id: "20|03|07"
                        }
                    },
                    {
                        name: "文成县",
                        attr: {
                            id: "20|03|08"
                        }
                    },
                    {
                        name: "泰顺县",
                        attr: {
                            id: "20|03|09"
                        }
                    },
                    {
                        name: "瑞安市",
                        attr: {
                            id: "20|03|10"
                        }
                    },
                    {
                        name: "乐清市",
                        attr: {
                            id: "20|03|11"
                        }
                    }
                ]
            },
            {
                name: "嘉兴",
                attr: {
                    id: "20|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "南湖区",
                        attr: {
                            id: "20|04|01"
                        }
                    },
                    {
                        name: "秀洲区",
                        attr: {
                            id: "20|04|02"
                        }
                    },
                    {
                        name: "嘉善县",
                        attr: {
                            id: "20|04|03"
                        }
                    },
                    {
                        name: "海盐县",
                        attr: {
                            id: "20|04|04"
                        }
                    },
                    {
                        name: "海宁市",
                        attr: {
                            id: "20|04|05"
                        }
                    },
                    {
                        name: "平湖市",
                        attr: {
                            id: "20|04|06"
                        }
                    },
                    {
                        name: "桐乡市",
                        attr: {
                            id: "20|04|07"
                        }
                    }
                ]
            },
            {
                name: "湖州",
                attr: {
                    id: "20|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "吴兴区",
                        attr: {
                            id: "20|05|01"
                        }
                    },
                    {
                        name: "南浔区",
                        attr: {
                            id: "20|05|02"
                        }
                    },
                    {
                        name: "德清县",
                        attr: {
                            id: "20|05|03"
                        }
                    },
                    {
                        name: "长兴县",
                        attr: {
                            id: "20|05|04"
                        }
                    },
                    {
                        name: "安吉县",
                        attr: {
                            id: "20|05|05"
                        }
                    }
                ]
            },
            {
                name: "绍兴",
                attr: {
                    id: "20|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "越城区",
                        attr: {
                            id: "20|06|01"
                        }
                    },
                    {
                        name: "柯桥区",
                        attr: {
                            id: "20|06|02"
                        }
                    },
                    {
                        name: "新昌县",
                        attr: {
                            id: "20|06|03"
                        }
                    },
                    {
                        name: "诸暨市",
                        attr: {
                            id: "20|06|04"
                        }
                    },
                    {
                        name: "上虞区",
                        attr: {
                            id: "20|06|05"
                        }
                    },
                    {
                        name: "嵊州市",
                        attr: {
                            id: "20|06|06"
                        }
                    }
                ]
            },
            {
                name: "金华",
                attr: {
                    id: "20|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "婺城区",
                        attr: {
                            id: "20|07|01"
                        }
                    },
                    {
                        name: "金东区",
                        attr: {
                            id: "20|07|02"
                        }
                    },
                    {
                        name: "武义县",
                        attr: {
                            id: "20|03|03"
                        }
                    },
                    {
                        name: "浦江县",
                        attr: {
                            id: "20|07|04"
                        }
                    },
                    {
                        name: "磐安县",
                        attr: {
                            id: "20|07|05"
                        }
                    },
                    {
                        name: "兰溪市",
                        attr: {
                            id: "20|07|06"
                        }
                    },
                    {
                        name: "义乌市",
                        attr: {
                            id: "20|07|07"
                        }
                    },
                    {
                        name: "东阳市",
                        attr: {
                            id: "20|07|08"
                        }
                    },
                    {
                        name: "永康市",
                        attr: {
                            id: "20|07|09"
                        }
                    }
                ]
            },
            {
                name: "衢州",
                attr: {
                    id: "20|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "柯城区",
                        attr: {
                            id: "20|08|01"
                        }
                    },
                    {
                        name: "衢江区",
                        attr: {
                            id: "20|08|02"
                        }
                    },
                    {
                        name: "常山县",
                        attr: {
                            id: "20|08|03"
                        }
                    },
                    {
                        name: "开化县",
                        attr: {
                            id: "20|08|04"
                        }
                    },
                    {
                        name: "龙游县",
                        attr: {
                            id: "20|08|05"
                        }
                    },
                    {
                        name: "江山市",
                        attr: {
                            id: "20|08|06"
                        }
                    }
                ]
            },
            {
                name: "舟山",
                attr: {
                    id: "20|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "定海区",
                        attr: {
                            id: "20|09|01"
                        }
                    },
                    {
                        name: "普陀区",
                        attr: {
                            id: "20|09|02"
                        }
                    },
                    {
                        name: "岱山县",
                        attr: {
                            id: "20|09|03"
                        }
                    },
                    {
                        name: "嵊泗县",
                        attr: {
                            id: "20|09|04"
                        }
                    }
                ]
            },
            {
                name: "台州",
                attr: {
                    id: "20|10|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "椒江区",
                        attr: {
                            id: "20|10|01"
                        }
                    },
                    {
                        name: "黄岩区",
                        attr: {
                            id: "20|10|02"
                        }
                    },
                    {
                        name: "路桥区",
                        attr: {
                            id: "20|10|03"
                        }
                    },
                    {
                        name: "玉环县",
                        attr: {
                            id: "20|10|04"
                        }
                    },
                    {
                        name: "三门县",
                        attr: {
                            id: "20|10|05"
                        }
                    },
                    {
                        name: "天台县",
                        attr: {
                            id: "20|10|06"
                        }
                    },
                    {
                        name: "仙居县",
                        attr: {
                            id: "20|10|07"
                        }
                    },
                    {
                        name: "温岭市",
                        attr: {
                            id: "20|10|08"
                        }
                    },
                    {
                        name: "临海市",
                        attr: {
                            id: "20|10|09"
                        }
                    }
                ]
            },
            {
                name: "丽水",
                attr: {
                    id: "20|11|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "莲都区",
                        attr: {
                            id: "20|11|01"
                        }
                    },
                    {
                        name: "青田县",
                        attr: {
                            id: "20|11|02"
                        }
                    },
                    {
                        name: "缙云县",
                        attr: {
                            id: "20|11|03"
                        }
                    },
                    {
                        name: "遂昌县",
                        attr: {
                            id: "20|11|04"
                        }
                    },
                    {
                        name: "松阳县",
                        attr: {
                            id: "20|11|05"
                        }
                    },
                    {
                        name: "云和县",
                        attr: {
                            id: "20|11|06"
                        }
                    },
                    {
                        name: "庆元县",
                        attr: {
                            id: "20|11|07"
                        }
                    },
                    {
                        name: "景宁畲族自治县",
                        attr: {
                            id: "20|11|08"
                        }
                    },
                    {
                        name: "龙泉市",
                        attr: {
                            id: "20|11|09"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "安徽",
        attr: {
            id: "21|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "合肥",
                attr: {
                    id: "21|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "瑶海区",
                        attr: {
                            id: "21|01|01"
                        }
                    },
                    {
                        name: "庐阳区",
                        attr: {
                            id: "21|01|02"
                        }
                    },
                    {
                        name: "蜀山区",
                        attr: {
                            id: "21|01|03"
                        }
                    },
                    {
                        name: "包河区",
                        attr: {
                            id: "21|01|04"
                        }
                    },
                    {
                        name: "长丰县",
                        attr: {
                            id: "21|01|05"
                        }
                    },
                    {
                        name: "肥东县",
                        attr: {
                            id: "21|01|06"
                        }
                    },
                    {
                        name: "肥西县",
                        attr: {
                            id: "21|01|07"
                        }
                    },
                    {
                        name: "庐江县",
                        attr: {
                            id: "21|01|08"
                        }
                    },
                    {
                        name: "巢湖市",
                        attr: {
                            id: "21|01|09"
                        }
                    }
                ]
            },
            {
                name: "芜湖",
                attr: {
                    id: "21|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "镜湖区",
                        attr: {
                            id: "21|02|01"
                        }
                    },
                    {
                        name: "弋江区",
                        attr: {
                            id: "21|02|02"
                        }
                    },
                    {
                        name: "鸠江区",
                        attr: {
                            id: "21|02|03"
                        }
                    },
                    {
                        name: "三山区",
                        attr: {
                            id: "21|02|04"
                        }
                    },
                    {
                        name: "芜湖县",
                        attr: {
                            id: "21|02|05"
                        }
                    },
                    {
                        name: "繁昌县",
                        attr: {
                            id: "21|02|06"
                        }
                    },
                    {
                        name: "南陵县",
                        attr: {
                            id: "21|02|07"
                        }
                    },
                    {
                        name: "无为县",
                        attr: {
                            id: "21|02|08"
                        }
                    }
                ]
            },
            {
                name: "蚌埠",
                attr: {
                    id: "21|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "龙子湖区",
                        attr: {
                            id: "21|03|01"
                        }
                    },
                    {
                        name: "蚌山区",
                        attr: {
                            id: "21|03|02"
                        }
                    },
                    {
                        name: "禹会区",
                        attr: {
                            id: "21|03|03"
                        }
                    },
                    {
                        name: "淮上区",
                        attr: {
                            id: "21|03|04"
                        }
                    },
                    {
                        name: "怀远县",
                        attr: {
                            id: "21|03|05"
                        }
                    },
                    {
                        name: "五河县",
                        attr: {
                            id: "21|03|06"
                        }
                    },
                    {
                        name: "固镇县",
                        attr: {
                            id: "21|03|07"
                        }
                    }
                ]
            },
            {
                name: "淮南",
                attr: {
                    id: "21|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "大通区",
                        attr: {
                            id: "21|04|01"
                        }
                    },
                    {
                        name: "田家庵区",
                        attr: {
                            id: "21|04|02"
                        }
                    },
                    {
                        name: "谢家集区",
                        attr: {
                            id: "21|04|03"
                        }
                    },
                    {
                        name: "八公山区",
                        attr: {
                            id: "21|04|04"
                        }
                    },
                    {
                        name: "潘集区",
                        attr: {
                            id: "21|04|05"
                        }
                    },
                    {
                        name: "凤台县",
                        attr: {
                            id: "21|04|06"
                        }
                    }
                ]
            },
            {
                name: "马鞍山",
                attr: {
                    id: "21|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "博望区",
                        attr: {
                            id: "21|05|01"
                        }
                    },
                    {
                        name: "花山区",
                        attr: {
                            id: "21|05|02"
                        }
                    },
                    {
                        name: "雨山区",
                        attr: {
                            id: "21|05|03"
                        }
                    },
                    {
                        name: "当涂县",
                        attr: {
                            id: "21|05|04"
                        }
                    },
                    {
                        name: "含山县",
                        attr: {
                            id: "21|05|05"
                        }
                    },
                    {
                        name: "和县",
                        attr: {
                            id: "21|05|06"
                        }
                    }
                ]
            },
            {
                name: "淮北",
                attr: {
                    id: "21|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "杜集区",
                        attr: {
                            id: "21|06|01"
                        }
                    },
                    {
                        name: "相山区",
                        attr: {
                            id: "21|06|02"
                        }
                    },
                    {
                        name: "烈山区",
                        attr: {
                            id: "21|06|03"
                        }
                    },
                    {
                        name: "濉溪县",
                        attr: {
                            id: "21|06|04"
                        }
                    }
                ]
            },
            {
                name: "铜陵",
                attr: {
                    id: "21|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "铜官山区",
                        attr: {
                            id: "21|07|01"
                        }
                    },
                    {
                        name: "狮子山区",
                        attr: {
                            id: "21|07|02"
                        }
                    },
                    {
                        name: "郊区",
                        attr: {
                            id: "21|03|03"
                        }
                    },
                    {
                        name: "铜陵县",
                        attr: {
                            id: "21|07|04"
                        }
                    }
                ]
            },
            {
                name: "安庆",
                attr: {
                    id: "21|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "迎江区",
                        attr: {
                            id: "21|08|01"
                        }
                    },
                    {
                        name: "大观区",
                        attr: {
                            id: "21|08|02"
                        }
                    },
                    {
                        name: "宜秀区",
                        attr: {
                            id: "21|08|03"
                        }
                    },
                    {
                        name: "怀宁县",
                        attr: {
                            id: "21|08|04"
                        }
                    },
                    {
                        name: "枞阳县",
                        attr: {
                            id: "21|08|05"
                        }
                    },
                    {
                        name: "潜山县",
                        attr: {
                            id: "21|08|06"
                        }
                    },
                    {
                        name: "太湖县",
                        attr: {
                            id: "21|08|07"
                        }
                    },
                    {
                        name: "宿松县",
                        attr: {
                            id: "21|08|08"
                        }
                    },
                    {
                        name: "望江县",
                        attr: {
                            id: "21|08|09"
                        }
                    },
                    {
                        name: "岳西县",
                        attr: {
                            id: "21|08|10"
                        }
                    },
                    {
                        name: "桐城市",
                        attr: {
                            id: "21|08|11"
                        }
                    }
                ]
            },
            {
                name: "黄山",
                attr: {
                    id: "21|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "屯溪区",
                        attr: {
                            id: "21|09|01"
                        }
                    },
                    {
                        name: "黄山区",
                        attr: {
                            id: "21|09|02"
                        }
                    },
                    {
                        name: "徽州区",
                        attr: {
                            id: "21|09|03"
                        }
                    },
                    {
                        name: "歙县",
                        attr: {
                            id: "21|09|04"
                        }
                    },
                    {
                        name: "休宁县",
                        attr: {
                            id: "21|09|04"
                        }
                    },
                    {
                        name: "黟县",
                        attr: {
                            id: "21|09|04"
                        }
                    },
                    {
                        name: "祁门县",
                        attr: {
                            id: "21|09|04"
                        }
                    }
                ]
            },
            {
                name: "滁州",
                attr: {
                    id: "21|10|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "琅琊区",
                        attr: {
                            id: "21|10|01"
                        }
                    },
                    {
                        name: "南谯区",
                        attr: {
                            id: "21|10|02"
                        }
                    },
                    {
                        name: "来安县",
                        attr: {
                            id: "21|10|03"
                        }
                    },
                    {
                        name: "全椒县",
                        attr: {
                            id: "21|10|04"
                        }
                    },
                    {
                        name: "定远县",
                        attr: {
                            id: "21|10|05"
                        }
                    },
                    {
                        name: "凤阳县",
                        attr: {
                            id: "21|10|06"
                        }
                    },
                    {
                        name: "天长市",
                        attr: {
                            id: "21|10|07"
                        }
                    },
                    {
                        name: "明光市",
                        attr: {
                            id: "21|10|08"
                        }
                    }
                ]
            },
            {
                name: "阜阳",
                attr: {
                    id: "21|11|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "颍州区",
                        attr: {
                            id: "21|11|01"
                        }
                    },
                    {
                        name: "颍东区",
                        attr: {
                            id: "21|11|02"
                        }
                    },
                    {
                        name: "颍泉区",
                        attr: {
                            id: "21|11|03"
                        }
                    },
                    {
                        name: "临泉县",
                        attr: {
                            id: "21|11|04"
                        }
                    },
                    {
                        name: "太和县",
                        attr: {
                            id: "21|11|05"
                        }
                    },
                    {
                        name: "阜南县",
                        attr: {
                            id: "21|11|06"
                        }
                    },
                    {
                        name: "颍上县",
                        attr: {
                            id: "21|11|07"
                        }
                    },
                    {
                        name: "界首市",
                        attr: {
                            id: "21|11|08"
                        }
                    }
                ]
            },
            {
                name: "宿州",
                attr: {
                    id: "21|12|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "埇桥区",
                        attr: {
                            id: "21|12|01"
                        }
                    },
                    {
                        name: "砀山县",
                        attr: {
                            id: "21|12|02"
                        }
                    },
                    {
                        name: "萧县",
                        attr: {
                            id: "21|12|03"
                        }
                    },
                    {
                        name: "灵璧县",
                        attr: {
                            id: "21|12|04"
                        }
                    },
                    {
                        name: "泗县",
                        attr: {
                            id: "21|12|05"
                        }
                    }
                ]
            },
            {
                name: "六安",
                attr: {
                    id: "21|14|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "金安区",
                        attr: {
                            id: "21|14|01"
                        }
                    },
                    {
                        name: "裕安区",
                        attr: {
                            id: "21|14|02"
                        }
                    },
                    {
                        name: "寿县",
                        attr: {
                            id: "21|14|03"
                        }
                    },
                    {
                        name: "霍邱县",
                        attr: {
                            id: "21|14|04"
                        }
                    },
                    {
                        name: "舒城县",
                        attr: {
                            id: "21|14|05"
                        }
                    },
                    {
                        name: "金寨县",
                        attr: {
                            id: "21|14|06"
                        }
                    },
                    {
                        name: "霍山县",
                        attr: {
                            id: "21|14|07"
                        }
                    }
                ]
            },
            {
                name: "亳州",
                attr: {
                    id: "21|15|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "谯城区",
                        attr: {
                            id: "21|15|01"
                        }
                    },
                    {
                        name: "涡阳县",
                        attr: {
                            id: "21|15|02"
                        }
                    },
                    {
                        name: "蒙城县",
                        attr: {
                            id: "21|15|03"
                        }
                    },
                    {
                        name: "利辛县",
                        attr: {
                            id: "21|15|04"
                        }
                    }
                ]
            },
            {
                name: "池州",
                attr: {
                    id: "21|16|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "贵池区",
                        attr: {
                            id: "21|16|01"
                        }
                    },
                    {
                        name: "东至县",
                        attr: {
                            id: "21|16|02"
                        }
                    },
                    {
                        name: "石台县",
                        attr: {
                            id: "21|16|03"
                        }
                    },
                    {
                        name: "青阳县",
                        attr: {
                            id: "21|16|04"
                        }
                    }
                ]
            },
            {
                name: "宣城",
                attr: {
                    id: "21|17|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "宣州区",
                        attr: {
                            id: "21|17|01"
                        }
                    },
                    {
                        name: "郎溪县",
                        attr: {
                            id: "21|17|02"
                        }
                    },
                    {
                        name: "广德县",
                        attr: {
                            id: "21|17|03"
                        }
                    },
                    {
                        name: "泾县",
                        attr: {
                            id: "21|17|04"
                        }
                    },
                    {
                        name: "绩溪县",
                        attr: {
                            id: "21|17|05"
                        }
                    },
                    {
                        name: "旌德县",
                        attr: {
                            id: "21|17|06"
                        }
                    },
                    {
                        name: "宁国市",
                        attr: {
                            id: "21|17|07"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "福建",
        attr: {
            id: "22|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "福州",
                attr: {
                    id: "22|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "鼓楼区",
                        attr: {
                            id: "22|01|01"
                        }
                    },
                    {
                        name: "台江区",
                        attr: {
                            id: "22|01|02"
                        }
                    },
                    {
                        name: "仓山区",
                        attr: {
                            id: "22|01|03"
                        }
                    },
                    {
                        name: "马尾区",
                        attr: {
                            id: "22|01|04"
                        }
                    },
                    {
                        name: "晋安区",
                        attr: {
                            id: "22|01|05"
                        }
                    },
                    {
                        name: "闽侯县",
                        attr: {
                            id: "22|01|06"
                        }
                    },
                    {
                        name: "连江县",
                        attr: {
                            id: "22|01|07"
                        }
                    },
                    {
                        name: "罗源县",
                        attr: {
                            id: "22|01|08"
                        }
                    },
                    {
                        name: "闽清县",
                        attr: {
                            id: "22|01|09"
                        }
                    },
                    {
                        name: "永泰县",
                        attr: {
                            id: "22|01|10"
                        }
                    },
                    {
                        name: "平潭县",
                        attr: {
                            id: "22|01|11"
                        }
                    },
                    {
                        name: "福清市",
                        attr: {
                            id: "22|01|12"
                        }
                    },
                    {
                        name: "长乐市",
                        attr: {
                            id: "22|01|13"
                        }
                    }
                ]
            },
            {
                name: "厦门",
                attr: {
                    id: "22|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "思明区",
                        attr: {
                            id: "22|02|01"
                        }
                    },
                    {
                        name: "海沧区",
                        attr: {
                            id: "22|02|02"
                        }
                    },
                    {
                        name: "湖里区",
                        attr: {
                            id: "22|02|03"
                        }
                    },
                    {
                        name: "集美区",
                        attr: {
                            id: "22|02|04"
                        }
                    },
                    {
                        name: "同安区",
                        attr: {
                            id: "22|02|05"
                        }
                    },
                    {
                        name: "翔安区",
                        attr: {
                            id: "22|02|06"
                        }
                    }
                ]
            },
            {
                name: "莆田",
                attr: {
                    id: "22|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "城厢区",
                        attr: {
                            id: "22|03|01"
                        }
                    },
                    {
                        name: "涵江区",
                        attr: {
                            id: "22|03|02"
                        }
                    },
                    {
                        name: "荔城区",
                        attr: {
                            id: "22|03|03"
                        }
                    },
                    {
                        name: "秀屿区",
                        attr: {
                            id: "22|03|04"
                        }
                    },
                    {
                        name: "仙游县",
                        attr: {
                            id: "22|03|05"
                        }
                    }
                ]
            },
            {
                name: "三明",
                attr: {
                    id: "22|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "梅列区",
                        attr: {
                            id: "22|04|01"
                        }
                    },
                    {
                        name: "三元区",
                        attr: {
                            id: "22|04|02"
                        }
                    },
                    {
                        name: "明溪县",
                        attr: {
                            id: "22|04|03"
                        }
                    },
                    {
                        name: "清流县",
                        attr: {
                            id: "22|04|04"
                        }
                    },
                    {
                        name: "宁化县",
                        attr: {
                            id: "22|04|05"
                        }
                    },
                    {
                        name: "大田县",
                        attr: {
                            id: "22|04|06"
                        }
                    },
                    {
                        name: "尤溪县",
                        attr: {
                            id: "22|04|07"
                        }
                    },
                    {
                        name: "沙县",
                        attr: {
                            id: "22|04|08"
                        }
                    },
                    {
                        name: "将乐县",
                        attr: {
                            id: "22|04|09"
                        }
                    },
                    {
                        name: "泰宁县",
                        attr: {
                            id: "22|04|10"
                        }
                    },
                    {
                        name: "建宁县",
                        attr: {
                            id: "22|04|11"
                        }
                    },
                    {
                        name: "永安市",
                        attr: {
                            id: "22|04|12"
                        }
                    }
                ]
            },
            {
                name: "泉州",
                attr: {
                    id: "22|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "鲤城区",
                        attr: {
                            id: "22|05|01"
                        }
                    },
                    {
                        name: "丰泽区",
                        attr: {
                            id: "22|05|02"
                        }
                    },
                    {
                        name: "洛江区",
                        attr: {
                            id: "22|05|03"
                        }
                    },
                    {
                        name: "泉港区",
                        attr: {
                            id: "22|05|04"
                        }
                    },
                    {
                        name: "惠安县",
                        attr: {
                            id: "22|05|05"
                        }
                    },
                    {
                        name: "安溪县",
                        attr: {
                            id: "22|05|06"
                        }
                    },
                    {
                        name: "永春县",
                        attr: {
                            id: "22|05|07"
                        }
                    },
                    {
                        name: "德化县",
                        attr: {
                            id: "22|05|08"
                        }
                    },
                    {
                        name: "金门县",
                        attr: {
                            id: "22|05|09"
                        }
                    },
                    {
                        name: "石狮市",
                        attr: {
                            id: "22|05|10"
                        }
                    },
                    {
                        name: "晋江市",
                        attr: {
                            id: "22|05|11"
                        }
                    },
                    {
                        name: "南安市",
                        attr: {
                            id: "22|05|12"
                        }
                    }
                ]
            },
            {
                name: "漳州",
                attr: {
                    id: "22|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "芗城区",
                        attr: {
                            id: "22|06|01"
                        }
                    },
                    {
                        name: "龙文区",
                        attr: {
                            id: "22|06|02"
                        }
                    },
                    {
                        name: "云霄县",
                        attr: {
                            id: "22|06|03"
                        }
                    },
                    {
                        name: "漳浦县",
                        attr: {
                            id: "22|06|04"
                        }
                    },
                    {
                        name: "诏安县",
                        attr: {
                            id: "22|06|05"
                        }
                    },
                    {
                        name: "长泰县",
                        attr: {
                            id: "22|06|06"
                        }
                    },
                    {
                        name: "东山县",
                        attr: {
                            id: "22|06|07"
                        }
                    },
                    {
                        name: "南靖县",
                        attr: {
                            id: "22|06|08"
                        }
                    },
                    {
                        name: "平和县",
                        attr: {
                            id: "22|06|09"
                        }
                    },
                    {
                        name: "华安县",
                        attr: {
                            id: "22|06|10"
                        }
                    },
                    {
                        name: "龙海市",
                        attr: {
                            id: "22|06|11"
                        }
                    }
                ]
            },
            {
                name: "南平",
                attr: {
                    id: "22|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "延平区",
                        attr: {
                            id: "22|07|01"
                        }
                    },
                    {
                        name: "顺昌县",
                        attr: {
                            id: "22|07|02"
                        }
                    },
                    {
                        name: "浦城县",
                        attr: {
                            id: "22|03|03"
                        }
                    },
                    {
                        name: "光泽县",
                        attr: {
                            id: "22|07|04"
                        }
                    },
                    {
                        name: "松溪县",
                        attr: {
                            id: "22|07|05"
                        }
                    },
                    {
                        name: "政和县",
                        attr: {
                            id: "22|07|06"
                        }
                    },
                    {
                        name: "邵武市",
                        attr: {
                            id: "22|07|07"
                        }
                    },
                    {
                        name: "武夷山市",
                        attr: {
                            id: "22|07|08"
                        }
                    },
                    {
                        name: "建瓯市",
                        attr: {
                            id: "22|07|09"
                        }
                    },
                    {
                        name: "建阳市",
                        attr: {
                            id: "22|07|10"
                        }
                    }
                ]
            },
            {
                name: "龙岩",
                attr: {
                    id: "22|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "新罗区",
                        attr: {
                            id: "22|08|01"
                        }
                    },
                    {
                        name: "长汀县",
                        attr: {
                            id: "22|08|02"
                        }
                    },
                    {
                        name: "永定县",
                        attr: {
                            id: "22|08|03"
                        }
                    },
                    {
                        name: "上杭县",
                        attr: {
                            id: "22|08|04"
                        }
                    },
                    {
                        name: "武平县",
                        attr: {
                            id: "22|08|05"
                        }
                    },
                    {
                        name: "连城县",
                        attr: {
                            id: "22|08|06"
                        }
                    },
                    {
                        name: "漳平市",
                        attr: {
                            id: "22|08|07"
                        }
                    }
                ]
            },
            {
                name: "宁德",
                attr: {
                    id: "22|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "蕉城区",
                        attr: {
                            id: "22|09|01"
                        }
                    },
                    {
                        name: "霞浦县",
                        attr: {
                            id: "22|09|02"
                        }
                    },
                    {
                        name: "古田县",
                        attr: {
                            id: "22|09|03"
                        }
                    },
                    {
                        name: "屏南县",
                        attr: {
                            id: "22|09|04"
                        }
                    },
                    {
                        name: "寿宁县",
                        attr: {
                            id: "22|09|05"
                        }
                    },
                    {
                        name: "周宁县",
                        attr: {
                            id: "22|09|06"
                        }
                    },
                    {
                        name: "柘荣县",
                        attr: {
                            id: "22|09|07"
                        }
                    },
                    {
                        name: "福安市",
                        attr: {
                            id: "22|09|08"
                        }
                    },
                    {
                        name: "福鼎市",
                        attr: {
                            id: "22|09|09"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "江西",
        attr: {
            id: "23|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "南昌",
                attr: {
                    id: "23|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "东湖区",
                        attr: {
                            id: "23|01|01"
                        }
                    },
                    {
                        name: "西湖区",
                        attr: {
                            id: "23|01|02"
                        }
                    },
                    {
                        name: "青云谱区",
                        attr: {
                            id: "23|01|03"
                        }
                    },
                    {
                        name: "湾里区",
                        attr: {
                            id: "23|01|04"
                        }
                    },
                    {
                        name: "青山湖区",
                        attr: {
                            id: "23|01|05"
                        }
                    },
                    {
                        name: "南昌县",
                        attr: {
                            id: "23|01|06"
                        }
                    },
                    {
                        name: "新建县",
                        attr: {
                            id: "23|01|07"
                        }
                    },
                    {
                        name: "安义县",
                        attr: {
                            id: "23|01|08"
                        }
                    },
                    {
                        name: "进贤县",
                        attr: {
                            id: "23|01|09"
                        }
                    }
                ]
            },
            {
                name: "景德镇",
                attr: {
                    id: "23|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "昌江区",
                        attr: {
                            id: "23|02|01"
                        }
                    },
                    {
                        name: "珠山区",
                        attr: {
                            id: "23|02|02"
                        }
                    },
                    {
                        name: "浮梁县",
                        attr: {
                            id: "23|02|03"
                        }
                    },
                    {
                        name: "乐平市",
                        attr: {
                            id: "23|02|04"
                        }
                    }
                ]
            },
            {
                name: "萍乡",
                attr: {
                    id: "23|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "安源区",
                        attr: {
                            id: "23|03|01"
                        }
                    },
                    {
                        name: "湘东区",
                        attr: {
                            id: "23|03|02"
                        }
                    },
                    {
                        name: "莲花县",
                        attr: {
                            id: "23|03|03"
                        }
                    },
                    {
                        name: "上栗县",
                        attr: {
                            id: "23|03|04"
                        }
                    },
                    {
                        name: "芦溪县",
                        attr: {
                            id: "23|03|05"
                        }
                    }
                ]
            },
            {
                name: "九江",
                attr: {
                    id: "23|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "庐山区",
                        attr: {
                            id: "23|04|01"
                        }
                    },
                    {
                        name: "浔阳区",
                        attr: {
                            id: "23|04|02"
                        }
                    },
                    {
                        name: "九江县",
                        attr: {
                            id: "23|04|03"
                        }
                    },
                    {
                        name: "武宁县",
                        attr: {
                            id: "23|04|04"
                        }
                    },
                    {
                        name: "修水县",
                        attr: {
                            id: "23|04|05"
                        }
                    },
                    {
                        name: "永修县",
                        attr: {
                            id: "23|04|06"
                        }
                    },
                    {
                        name: "德安县",
                        attr: {
                            id: "23|04|07"
                        }
                    },
                    {
                        name: "星子县",
                        attr: {
                            id: "23|04|08"
                        }
                    },
                    {
                        name: "都昌县",
                        attr: {
                            id: "23|04|09"
                        }
                    },
                    {
                        name: "湖口县",
                        attr: {
                            id: "23|04|10"
                        }
                    },
                    {
                        name: "彭泽县",
                        attr: {
                            id: "23|04|11"
                        }
                    },
                    {
                        name: "瑞昌市",
                        attr: {
                            id: "23|04|12"
                        }
                    },
                    {
                        name: "共青城市",
                        attr: {
                            id: "23|04|13"
                        }
                    }
                ]
            },
            {
                name: "新余",
                attr: {
                    id: "23|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "渝水区",
                        attr: {
                            id: "23|05|01"
                        }
                    },
                    {
                        name: "分宜县",
                        attr: {
                            id: "23|05|02"
                        }
                    }
                ]
            },
            {
                name: "鹰潭",
                attr: {
                    id: "23|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "月湖区",
                        attr: {
                            id: "23|06|01"
                        }
                    },
                    {
                        name: "余江县",
                        attr: {
                            id: "23|06|02"
                        }
                    },
                    {
                        name: "贵溪市",
                        attr: {
                            id: "23|06|03"
                        }
                    }
                ]
            },
            {
                name: "赣州",
                attr: {
                    id: "23|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "章贡区",
                        attr: {
                            id: "23|07|01"
                        }
                    },
                    {
                        name: "赣县",
                        attr: {
                            id: "23|07|02"
                        }
                    },
                    {
                        name: "信丰县",
                        attr: {
                            id: "23|03|03"
                        }
                    },
                    {
                        name: "大余县",
                        attr: {
                            id: "23|07|04"
                        }
                    },
                    {
                        name: "上犹县",
                        attr: {
                            id: "23|07|05"
                        }
                    },
                    {
                        name: "崇义县",
                        attr: {
                            id: "23|07|06"
                        }
                    },
                    {
                        name: "安远县",
                        attr: {
                            id: "23|07|07"
                        }
                    },
                    {
                        name: "龙南县",
                        attr: {
                            id: "23|07|08"
                        }
                    },
                    {
                        name: "定南县",
                        attr: {
                            id: "23|07|09"
                        }
                    },
                    {
                        name: "全南县",
                        attr: {
                            id: "23|07|10"
                        }
                    },
                    {
                        name: "宁都县",
                        attr: {
                            id: "23|07|11"
                        }
                    },
                    {
                        name: "于都县",
                        attr: {
                            id: "23|07|12"
                        }
                    },
                    {
                        name: "兴国县",
                        attr: {
                            id: "23|07|13"
                        }
                    },
                    {
                        name: "会昌县",
                        attr: {
                            id: "23|07|14"
                        }
                    },
                    {
                        name: "寻乌县",
                        attr: {
                            id: "23|07|15"
                        }
                    },
                    {
                        name: "石城县",
                        attr: {
                            id: "23|07|16"
                        }
                    },
                    {
                        name: "瑞金市",
                        attr: {
                            id: "23|07|17"
                        }
                    },
                    {
                        name: "南康区",
                        attr: {
                            id: "23|07|18"
                        }
                    }
                ]
            },
            {
                name: "吉安",
                attr: {
                    id: "23|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "吉州区",
                        attr: {
                            id: "23|08|01"
                        }
                    },
                    {
                        name: "青原区",
                        attr: {
                            id: "23|08|02"
                        }
                    },
                    {
                        name: "吉安县",
                        attr: {
                            id: "23|08|03"
                        }
                    },
                    {
                        name: "吉水县",
                        attr: {
                            id: "23|08|04"
                        }
                    },
                    {
                        name: "峡江县",
                        attr: {
                            id: "23|08|05"
                        }
                    },
                    {
                        name: "新干县",
                        attr: {
                            id: "23|08|06"
                        }
                    },
                    {
                        name: "永丰县",
                        attr: {
                            id: "23|08|07"
                        }
                    },
                    {
                        name: "泰和县",
                        attr: {
                            id: "23|08|08"
                        }
                    },
                    {
                        name: "遂川县",
                        attr: {
                            id: "23|08|09"
                        }
                    },
                    {
                        name: "万安县",
                        attr: {
                            id: "23|08|10"
                        }
                    },
                    {
                        name: "安福县",
                        attr: {
                            id: "23|08|11"
                        }
                    },
                    {
                        name: "永新县",
                        attr: {
                            id: "23|08|12"
                        }
                    },
                    {
                        name: "井冈山市",
                        attr: {
                            id: "23|08|13"
                        }
                    }
                ]
            },
            {
                name: "宜春",
                attr: {
                    id: "23|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "袁州区",
                        attr: {
                            id: "23|09|01"
                        }
                    },
                    {
                        name: "奉新县",
                        attr: {
                            id: "23|09|02"
                        }
                    },
                    {
                        name: "万载县",
                        attr: {
                            id: "23|09|03"
                        }
                    },
                    {
                        name: "上高县",
                        attr: {
                            id: "23|09|04"
                        }
                    },
                    {
                        name: "宜丰县",
                        attr: {
                            id: "23|09|05"
                        }
                    },
                    {
                        name: "靖安县",
                        attr: {
                            id: "23|09|06"
                        }
                    },
                    {
                        name: "铜鼓县",
                        attr: {
                            id: "23|09|07"
                        }
                    },
                    {
                        name: "丰城市",
                        attr: {
                            id: "23|09|08"
                        }
                    },
                    {
                        name: "樟树市",
                        attr: {
                            id: "23|09|09"
                        }
                    },
                    {
                        name: "高安市",
                        attr: {
                            id: "23|09|10"
                        }
                    }
                ]
            },
            {
                name: "抚州",
                attr: {
                    id: "23|10|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "临川区",
                        attr: {
                            id: "23|10|01"
                        }
                    },
                    {
                        name: "南城县",
                        attr: {
                            id: "23|10|02"
                        }
                    },
                    {
                        name: "黎川县",
                        attr: {
                            id: "23|10|03"
                        }
                    },
                    {
                        name: "南丰县",
                        attr: {
                            id: "23|10|04"
                        }
                    },
                    {
                        name: "崇仁县",
                        attr: {
                            id: "23|10|05"
                        }
                    },
                    {
                        name: "乐安县",
                        attr: {
                            id: "23|10|06"
                        }
                    },
                    {
                        name: "宜黄县",
                        attr: {
                            id: "23|10|07"
                        }
                    },
                    {
                        name: "金溪县",
                        attr: {
                            id: "23|10|08"
                        }
                    },
                    {
                        name: "资溪县",
                        attr: {
                            id: "23|10|09"
                        }
                    },
                    {
                        name: "东乡县",
                        attr: {
                            id: "23|10|10"
                        }
                    },
                    {
                        name: "广昌县",
                        attr: {
                            id: "23|10|11"
                        }
                    }
                ]
            },
            {
                name: "宁德",
                attr: {
                    id: "23|11|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "信州区",
                        attr: {
                            id: "23|11|01"
                        }
                    },
                    {
                        name: "上饶县",
                        attr: {
                            id: "23|11|02"
                        }
                    },
                    {
                        name: "广丰县",
                        attr: {
                            id: "23|11|03"
                        }
                    },
                    {
                        name: "玉山县",
                        attr: {
                            id: "23|11|04"
                        }
                    },
                    {
                        name: "铅山县",
                        attr: {
                            id: "23|11|05"
                        }
                    },
                    {
                        name: "横峰县",
                        attr: {
                            id: "23|11|06"
                        }
                    },
                    {
                        name: "弋阳县",
                        attr: {
                            id: "23|11|07"
                        }
                    },
                    {
                        name: "余干县",
                        attr: {
                            id: "23|11|08"
                        }
                    },
                    {
                        name: "鄱阳县",
                        attr: {
                            id: "23|11|09"
                        }
                    },
                    {
                        name: "万年县",
                        attr: {
                            id: "23|11|10"
                        }
                    },
                    {
                        name: "婺源县",
                        attr: {
                            id: "23|11|11"
                        }
                    },
                    {
                        name: "德兴市",
                        attr: {
                            id: "23|11|12"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "山东",
        attr: {
            id: "24|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "济南",
                attr: {
                    id: "24|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "历下区",
                        attr: {
                            id: "24|01|01"
                        }
                    },
                    {
                        name: "市中区",
                        attr: {
                            id: "24|01|02"
                        }
                    },
                    {
                        name: "槐荫区",
                        attr: {
                            id: "24|01|03"
                        }
                    },
                    {
                        name: "天桥区",
                        attr: {
                            id: "24|01|04"
                        }
                    },
                    {
                        name: "历城区",
                        attr: {
                            id: "24|01|05"
                        }
                    },
                    {
                        name: "长清区",
                        attr: {
                            id: "24|01|06"
                        }
                    },
                    {
                        name: "平阴县",
                        attr: {
                            id: "24|01|07"
                        }
                    },
                    {
                        name: "济阳县",
                        attr: {
                            id: "24|01|08"
                        }
                    },
                    {
                        name: "商河县",
                        attr: {
                            id: "24|01|09"
                        }
                    },
                    {
                        name: "章丘市",
                        attr: {
                            id: "24|01|10"
                        }
                    }
                ]
            },
            {
                name: "青岛",
                attr: {
                    id: "24|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "市南区",
                        attr: {
                            id: "24|02|01"
                        }
                    },
                    {
                        name: "市北区",
                        attr: {
                            id: "24|02|02"
                        }
                    },
                    {
                        name: "黄岛区",
                        attr: {
                            id: "24|02|04"
                        }
                    },
                    {
                        name: "崂山区",
                        attr: {
                            id: "24|02|05"
                        }
                    },
                    {
                        name: "李沧区",
                        attr: {
                            id: "24|02|06"
                        }
                    },
                    {
                        name: "城阳区",
                        attr: {
                            id: "24|02|07"
                        }
                    },
                    {
                        name: "胶州市",
                        attr: {
                            id: "24|02|08"
                        }
                    },
                    {
                        name: "即墨市",
                        attr: {
                            id: "24|02|09"
                        }
                    },
                    {
                        name: "平度市",
                        attr: {
                            id: "24|02|10"
                        }
                    },
                    {
                        name: "莱西市",
                        attr: {
                            id: "24|02|12"
                        }
                    }
                ]
            },
            {
                name: "淄博",
                attr: {
                    id: "24|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "淄川区",
                        attr: {
                            id: "24|03|01"
                        }
                    },
                    {
                        name: "张店区",
                        attr: {
                            id: "24|03|02"
                        }
                    },
                    {
                        name: "博山区",
                        attr: {
                            id: "24|03|03"
                        }
                    },
                    {
                        name: "临淄区",
                        attr: {
                            id: "24|03|04"
                        }
                    },
                    {
                        name: "周村区",
                        attr: {
                            id: "24|03|05"
                        }
                    },
                    {
                        name: "桓台县",
                        attr: {
                            id: "24|03|06"
                        }
                    },
                    {
                        name: "高青县",
                        attr: {
                            id: "24|03|07"
                        }
                    },
                    {
                        name: "沂源县",
                        attr: {
                            id: "24|03|08"
                        }
                    }
                ]
            },
            {
                name: "枣庄",
                attr: {
                    id: "24|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "市中区",
                        attr: {
                            id: "24|04|01"
                        }
                    },
                    {
                        name: "薛城区",
                        attr: {
                            id: "24|04|02"
                        }
                    },
                    {
                        name: "峄城区",
                        attr: {
                            id: "24|04|03"
                        }
                    },
                    {
                        name: "台儿庄区",
                        attr: {
                            id: "24|04|04"
                        }
                    },
                    {
                        name: "山亭区",
                        attr: {
                            id: "24|04|05"
                        }
                    },
                    {
                        name: "滕州市",
                        attr: {
                            id: "24|04|06"
                        }
                    }
                ]
            },
            {
                name: "东营",
                attr: {
                    id: "24|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "东营区",
                        attr: {
                            id: "24|05|01"
                        }
                    },
                    {
                        name: "河口区",
                        attr: {
                            id: "24|05|02"
                        }
                    },
                    {
                        name: "垦利县",
                        attr: {
                            id: "24|05|03"
                        }
                    },
                    {
                        name: "利津县",
                        attr: {
                            id: "24|05|04"
                        }
                    },
                    {
                        name: "广饶县",
                        attr: {
                            id: "24|05|05"
                        }
                    }
                ]
            },
            {
                name: "烟台",
                attr: {
                    id: "24|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "芝罘区",
                        attr: {
                            id: "24|06|01"
                        }
                    },
                    {
                        name: "福山区",
                        attr: {
                            id: "24|06|02"
                        }
                    },
                    {
                        name: "牟平区",
                        attr: {
                            id: "24|06|03"
                        }
                    },
                    {
                        name: "莱山区",
                        attr: {
                            id: "24|06|04"
                        }
                    },
                    {
                        name: "长岛县",
                        attr: {
                            id: "24|06|05"
                        }
                    },
                    {
                        name: "龙口市",
                        attr: {
                            id: "24|06|06"
                        }
                    },
                    {
                        name: "莱阳市",
                        attr: {
                            id: "24|06|07"
                        }
                    },
                    {
                        name: "莱州市",
                        attr: {
                            id: "24|06|08"
                        }
                    },
                    {
                        name: "蓬莱市",
                        attr: {
                            id: "24|06|09"
                        }
                    },
                    {
                        name: "招远市",
                        attr: {
                            id: "24|06|10"
                        }
                    },
                    {
                        name: "栖霞市",
                        attr: {
                            id: "24|06|11"
                        }
                    },
                    {
                        name: "海阳市",
                        attr: {
                            id: "24|06|12"
                        }
                    }
                ]
            },
            {
                name: "潍坊",
                attr: {
                    id: "24|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "潍城区",
                        attr: {
                            id: "24|07|01"
                        }
                    },
                    {
                        name: "寒亭区",
                        attr: {
                            id: "24|07|02"
                        }
                    },
                    {
                        name: "坊子区",
                        attr: {
                            id: "24|03|03"
                        }
                    },
                    {
                        name: "奎文区",
                        attr: {
                            id: "24|07|04"
                        }
                    },
                    {
                        name: "临朐县",
                        attr: {
                            id: "24|07|05"
                        }
                    },
                    {
                        name: "昌乐县",
                        attr: {
                            id: "24|07|06"
                        }
                    },
                    {
                        name: "青州市",
                        attr: {
                            id: "24|07|07"
                        }
                    },
                    {
                        name: "诸城市",
                        attr: {
                            id: "24|07|08"
                        }
                    },
                    {
                        name: "寿光市",
                        attr: {
                            id: "24|07|09"
                        }
                    },
                    {
                        name: "安丘市",
                        attr: {
                            id: "24|07|10"
                        }
                    },
                    {
                        name: "高密市",
                        attr: {
                            id: "24|07|11"
                        }
                    },
                    {
                        name: "昌邑市",
                        attr: {
                            id: "24|07|12"
                        }
                    }
                ]
            },
            {
                name: "济宁",
                attr: {
                    id: "24|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "市中区",
                        attr: {
                            id: "24|08|01"
                        }
                    },
                    {
                        name: "任城区",
                        attr: {
                            id: "24|08|02"
                        }
                    },
                    {
                        name: "微山县",
                        attr: {
                            id: "24|08|03"
                        }
                    },
                    {
                        name: "鱼台县",
                        attr: {
                            id: "24|08|04"
                        }
                    },
                    {
                        name: "金乡县",
                        attr: {
                            id: "24|08|05"
                        }
                    },
                    {
                        name: "嘉祥县",
                        attr: {
                            id: "24|08|06"
                        }
                    },
                    {
                        name: "汶上县",
                        attr: {
                            id: "24|08|07"
                        }
                    },
                    {
                        name: "泗水县",
                        attr: {
                            id: "24|08|08"
                        }
                    },
                    {
                        name: "梁山县",
                        attr: {
                            id: "24|08|09"
                        }
                    },
                    {
                        name: "曲阜市",
                        attr: {
                            id: "24|08|10"
                        }
                    },
                    {
                        name: "兖州市",
                        attr: {
                            id: "24|08|11"
                        }
                    },
                    {
                        name: "邹城市",
                        attr: {
                            id: "24|08|12"
                        }
                    }
                ]
            },
            {
                name: "泰安",
                attr: {
                    id: "24|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "泰山区",
                        attr: {
                            id: "24|09|01"
                        }
                    },
                    {
                        name: "岱岳区",
                        attr: {
                            id: "24|09|02"
                        }
                    },
                    {
                        name: "宁阳县",
                        attr: {
                            id: "24|09|03"
                        }
                    },
                    {
                        name: "东平县",
                        attr: {
                            id: "24|09|04"
                        }
                    },
                    {
                        name: "新泰市",
                        attr: {
                            id: "24|09|05"
                        }
                    },
                    {
                        name: "肥城市",
                        attr: {
                            id: "24|09|06"
                        }
                    }
                ]
            },
            {
                name: "威海",
                attr: {
                    id: "24|10|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "环翠区",
                        attr: {
                            id: "24|10|01"
                        }
                    },
                    {
                        name: "文登市",
                        attr: {
                            id: "24|10|02"
                        }
                    },
                    {
                        name: "荣成市",
                        attr: {
                            id: "24|10|03"
                        }
                    },
                    {
                        name: "乳山市",
                        attr: {
                            id: "24|10|04"
                        }
                    }
                ]
            },
            {
                name: "日照",
                attr: {
                    id: "24|11|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "东港区",
                        attr: {
                            id: "24|11|01"
                        }
                    },
                    {
                        name: "岚山区",
                        attr: {
                            id: "24|11|02"
                        }
                    },
                    {
                        name: "五莲县",
                        attr: {
                            id: "24|11|03"
                        }
                    },
                    {
                        name: "莒县",
                        attr: {
                            id: "24|11|04"
                        }
                    }
                ]
            },
            {
                name: "莱芜",
                attr: {
                    id: "24|12|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "莱城区",
                        attr: {
                            id: "24|12|01"
                        }
                    },
                    {
                        name: "钢城区",
                        attr: {
                            id: "24|12|02"
                        }
                    }
                ]
            },
            {
                name: "临沂",
                attr: {
                    id: "24|13|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "兰山区",
                        attr: {
                            id: "24|13|01"
                        }
                    },
                    {
                        name: "罗庄区",
                        attr: {
                            id: "24|13|02"
                        }
                    },
                    {
                        name: "河东区",
                        attr: {
                            id: "24|13|03"
                        }
                    },
                    {
                        name: "沂南县",
                        attr: {
                            id: "24|13|04"
                        }
                    },
                    {
                        name: "郯城县",
                        attr: {
                            id: "24|13|05"
                        }
                    },
                    {
                        name: "沂水县",
                        attr: {
                            id: "24|13|06"
                        }
                    },
                    {
                        name: "苍山县",
                        attr: {
                            id: "24|13|07"
                        }
                    },
                    {
                        name: "费县",
                        attr: {
                            id: "24|13|08"
                        }
                    },
                    {
                        name: "平邑县",
                        attr: {
                            id: "24|13|09"
                        }
                    },
                    {
                        name: "莒南县",
                        attr: {
                            id: "24|13|10"
                        }
                    },
                    {
                        name: "蒙阴县",
                        attr: {
                            id: "24|13|11"
                        }
                    },
                    {
                        name: "临沭县",
                        attr: {
                            id: "24|13|12"
                        }
                    }
                ]
            },
            {
                name: "德州",
                attr: {
                    id: "24|14|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "德城区",
                        attr: {
                            id: "24|14|01"
                        }
                    },
                    {
                        name: "陵县",
                        attr: {
                            id: "24|14|02"
                        }
                    },
                    {
                        name: "宁津县",
                        attr: {
                            id: "24|14|03"
                        }
                    },
                    {
                        name: "庆云县",
                        attr: {
                            id: "24|14|04"
                        }
                    },
                    {
                        name: "临邑县",
                        attr: {
                            id: "24|14|05"
                        }
                    },
                    {
                        name: "齐河县",
                        attr: {
                            id: "24|14|06"
                        }
                    },
                    {
                        name: "平原县",
                        attr: {
                            id: "24|14|07"
                        }
                    },
                    {
                        name: "夏津县",
                        attr: {
                            id: "24|14|08"
                        }
                    },
                    {
                        name: "武城县",
                        attr: {
                            id: "24|14|09"
                        }
                    },
                    {
                        name: "乐陵市",
                        attr: {
                            id: "24|14|10"
                        }
                    },
                    {
                        name: "禹城市",
                        attr: {
                            id: "24|14|11"
                        }
                    }
                ]
            },
            {
                name: "聊城",
                attr: {
                    id: "24|15|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "东昌府区",
                        attr: {
                            id: "24|15|01"
                        }
                    },
                    {
                        name: "阳谷县",
                        attr: {
                            id: "24|15|02"
                        }
                    },
                    {
                        name: "莘县",
                        attr: {
                            id: "24|15|03"
                        }
                    },
                    {
                        name: "茌平县",
                        attr: {
                            id: "24|15|04"
                        }
                    },
                    {
                        name: "东阿县",
                        attr: {
                            id: "24|15|05"
                        }
                    },
                    {
                        name: "冠县",
                        attr: {
                            id: "24|15|06"
                        }
                    },
                    {
                        name: "高唐县",
                        attr: {
                            id: "24|15|07"
                        }
                    },
                    {
                        name: "临清市",
                        attr: {
                            id: "24|15|08"
                        }
                    }
                ]
            },
            {
                name: "滨州",
                attr: {
                    id: "24|16|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "滨城区",
                        attr: {
                            id: "24|16|01"
                        }
                    },
                    {
                        name: "惠民县",
                        attr: {
                            id: "24|16|02"
                        }
                    },
                    {
                        name: "阳信县",
                        attr: {
                            id: "24|16|03"
                        }
                    },
                    {
                        name: "无棣县",
                        attr: {
                            id: "24|16|04"
                        }
                    },
                    {
                        name: "沾化县",
                        attr: {
                            id: "24|16|05"
                        }
                    },
                    {
                        name: "博兴县",
                        attr: {
                            id: "24|16|06"
                        }
                    },
                    {
                        name: "邹平县",
                        attr: {
                            id: "24|16|07"
                        }
                    }
                ]
            },
            {
                name: "菏泽",
                attr: {
                    id: "24|17|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "牡丹区",
                        attr: {
                            id: "24|17|01"
                        }
                    },
                    {
                        name: "曹县",
                        attr: {
                            id: "24|17|02"
                        }
                    },
                    {
                        name: "单县",
                        attr: {
                            id: "24|17|03"
                        }
                    },
                    {
                        name: "成武县",
                        attr: {
                            id: "24|17|04"
                        }
                    },
                    {
                        name: "巨野县",
                        attr: {
                            id: "24|17|05"
                        }
                    },
                    {
                        name: "郓城县",
                        attr: {
                            id: "24|17|06"
                        }
                    },
                    {
                        name: "鄄城县",
                        attr: {
                            id: "24|17|07"
                        }
                    },
                    {
                        name: "定陶县",
                        attr: {
                            id: "24|17|08"
                        }
                    },
                    {
                        name: "东明县",
                        attr: {
                            id: "24|17|09"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "河南",
        attr: {
            id: "25|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "郑州",
                attr: {
                    id: "25|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "中原区",
                        attr: {
                            id: "25|01|01"
                        }
                    },
                    {
                        name: "二七区",
                        attr: {
                            id: "25|01|02"
                        }
                    },
                    {
                        name: "管城回族区",
                        attr: {
                            id: "25|01|03"
                        }
                    },
                    {
                        name: "金水区",
                        attr: {
                            id: "25|01|04"
                        }
                    },
                    {
                        name: "上街区",
                        attr: {
                            id: "25|01|05"
                        }
                    },
                    {
                        name: "惠济区",
                        attr: {
                            id: "25|01|06"
                        }
                    },
                    {
                        name: "中牟县",
                        attr: {
                            id: "25|01|07"
                        }
                    },
                    {
                        name: "巩义市",
                        attr: {
                            id: "25|01|08"
                        }
                    },
                    {
                        name: "荥阳市",
                        attr: {
                            id: "25|01|09"
                        }
                    },
                    {
                        name: "新密市",
                        attr: {
                            id: "25|01|10"
                        }
                    },
                    {
                        name: "新郑市",
                        attr: {
                            id: "25|01|11"
                        }
                    },
                    {
                        name: "登封市",
                        attr: {
                            id: "25|01|12"
                        }
                    }
                ]
            },
            {
                name: "开封",
                attr: {
                    id: "25|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "龙亭区",
                        attr: {
                            id: "25|02|01"
                        }
                    },
                    {
                        name: "顺河回族区",
                        attr: {
                            id: "25|02|02"
                        }
                    },
                    {
                        name: "鼓楼区",
                        attr: {
                            id: "25|02|03"
                        }
                    },
                    {
                        name: "禹王台区",
                        attr: {
                            id: "25|02|04"
                        }
                    },
                    {
                        name: "金明区",
                        attr: {
                            id: "25|02|05"
                        }
                    },
                    {
                        name: "杞县",
                        attr: {
                            id: "25|02|06"
                        }
                    },
                    {
                        name: "通许县",
                        attr: {
                            id: "25|02|07"
                        }
                    },
                    {
                        name: "尉氏县",
                        attr: {
                            id: "25|02|08"
                        }
                    },
                    {
                        name: "开封县",
                        attr: {
                            id: "25|02|09"
                        }
                    },
                    {
                        name: "兰考县",
                        attr: {
                            id: "25|02|10"
                        }
                    }
                ]
            },
            {
                name: "洛阳",
                attr: {
                    id: "25|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "老城区",
                        attr: {
                            id: "25|03|01"
                        }
                    },
                    {
                        name: "西工区",
                        attr: {
                            id: "25|03|02"
                        }
                    },
                    {
                        name: "廛河回族区",
                        attr: {
                            id: "25|03|03"
                        }
                    },
                    {
                        name: "涧西区",
                        attr: {
                            id: "25|03|04"
                        }
                    },
                    {
                        name: "吉利区",
                        attr: {
                            id: "25|03|05"
                        }
                    },
                    {
                        name: "洛龙区",
                        attr: {
                            id: "25|03|06"
                        }
                    },
                    {
                        name: "孟津县",
                        attr: {
                            id: "25|03|07"
                        }
                    },
                    {
                        name: "新安县",
                        attr: {
                            id: "25|03|08"
                        }
                    },
                    {
                        name: "栾川县",
                        attr: {
                            id: "25|03|09"
                        }
                    },
                    {
                        name: "嵩县",
                        attr: {
                            id: "25|03|10"
                        }
                    },
                    {
                        name: "汝阳县",
                        attr: {
                            id: "25|03|11"
                        }
                    },
                    {
                        name: "宜阳县",
                        attr: {
                            id: "25|03|12"
                        }
                    },
                    {
                        name: "洛宁县",
                        attr: {
                            id: "25|03|13"
                        }
                    },
                    {
                        name: "伊川县",
                        attr: {
                            id: "25|03|14"
                        }
                    },
                    {
                        name: "偃师市",
                        attr: {
                            id: "25|03|15"
                        }
                    }
                ]
            },
            {
                name: "平顶山",
                attr: {
                    id: "25|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "新华区",
                        attr: {
                            id: "25|04|01"
                        }
                    },
                    {
                        name: "卫东区",
                        attr: {
                            id: "25|04|02"
                        }
                    },
                    {
                        name: "石龙区",
                        attr: {
                            id: "25|04|03"
                        }
                    },
                    {
                        name: "湛河区",
                        attr: {
                            id: "25|04|04"
                        }
                    },
                    {
                        name: "宝丰县",
                        attr: {
                            id: "25|04|05"
                        }
                    },
                    {
                        name: "叶县",
                        attr: {
                            id: "25|04|06"
                        }
                    },
                    {
                        name: "鲁山县",
                        attr: {
                            id: "25|04|07"
                        }
                    },
                    {
                        name: "郏县",
                        attr: {
                            id: "25|04|08"
                        }
                    },
                    {
                        name: "舞钢市",
                        attr: {
                            id: "25|04|09"
                        }
                    },
                    {
                        name: "汝州市",
                        attr: {
                            id: "25|04|10"
                        }
                    }
                ]
            },
            {
                name: "安阳",
                attr: {
                    id: "25|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "文峰区",
                        attr: {
                            id: "25|05|01"
                        }
                    },
                    {
                        name: "北关区",
                        attr: {
                            id: "25|05|02"
                        }
                    },
                    {
                        name: "殷都区",
                        attr: {
                            id: "25|05|03"
                        }
                    },
                    {
                        name: "龙安区",
                        attr: {
                            id: "25|05|04"
                        }
                    },
                    {
                        name: "安阳县",
                        attr: {
                            id: "25|05|05"
                        }
                    },
                    {
                        name: "汤阴县",
                        attr: {
                            id: "25|05|06"
                        }
                    },
                    {
                        name: "滑县",
                        attr: {
                            id: "25|05|07"
                        }
                    },
                    {
                        name: "内黄县",
                        attr: {
                            id: "25|05|08"
                        }
                    },
                    {
                        name: "林州市",
                        attr: {
                            id: "25|05|09"
                        }
                    }
                ]
            },
            {
                name: "鹤壁",
                attr: {
                    id: "25|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "鹤山区",
                        attr: {
                            id: "25|06|01"
                        }
                    },
                    {
                        name: "山城区",
                        attr: {
                            id: "25|06|02"
                        }
                    },
                    {
                        name: "淇滨区",
                        attr: {
                            id: "25|06|03"
                        }
                    },
                    {
                        name: "浚县",
                        attr: {
                            id: "25|06|04"
                        }
                    },
                    {
                        name: "淇县",
                        attr: {
                            id: "25|06|05"
                        }
                    }
                ]
            },
            {
                name: "新乡",
                attr: {
                    id: "25|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "红旗区",
                        attr: {
                            id: "25|07|01"
                        }
                    },
                    {
                        name: "卫滨区",
                        attr: {
                            id: "25|07|02"
                        }
                    },
                    {
                        name: "凤泉区",
                        attr: {
                            id: "25|03|03"
                        }
                    },
                    {
                        name: "牧野区",
                        attr: {
                            id: "25|07|04"
                        }
                    },
                    {
                        name: "新乡县",
                        attr: {
                            id: "25|07|05"
                        }
                    },
                    {
                        name: "获嘉县",
                        attr: {
                            id: "25|07|06"
                        }
                    },
                    {
                        name: "原阳县",
                        attr: {
                            id: "25|07|07"
                        }
                    },
                    {
                        name: "延津县",
                        attr: {
                            id: "25|07|08"
                        }
                    },
                    {
                        name: "封丘县",
                        attr: {
                            id: "25|07|09"
                        }
                    },
                    {
                        name: "长垣县",
                        attr: {
                            id: "25|07|10"
                        }
                    },
                    {
                        name: "卫辉市",
                        attr: {
                            id: "25|07|11"
                        }
                    },
                    {
                        name: "辉县市",
                        attr: {
                            id: "25|07|12"
                        }
                    }
                ]
            },
            {
                name: "焦作",
                attr: {
                    id: "25|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "解放区",
                        attr: {
                            id: "25|08|01"
                        }
                    },
                    {
                        name: "中站区",
                        attr: {
                            id: "25|08|02"
                        }
                    },
                    {
                        name: "马村区",
                        attr: {
                            id: "25|08|03"
                        }
                    },
                    {
                        name: "山阳区",
                        attr: {
                            id: "25|08|04"
                        }
                    },
                    {
                        name: "修武县",
                        attr: {
                            id: "25|08|05"
                        }
                    },
                    {
                        name: "博爱县",
                        attr: {
                            id: "25|08|06"
                        }
                    },
                    {
                        name: "武陟县",
                        attr: {
                            id: "25|08|07"
                        }
                    },
                    {
                        name: "温县",
                        attr: {
                            id: "25|08|08"
                        }
                    },
                    {
                        name: "沁阳市",
                        attr: {
                            id: "25|08|09"
                        }
                    },
                    {
                        name: "孟州市",
                        attr: {
                            id: "25|08|10"
                        }
                    }
                ]
            },
            {
                name: "濮阳",
                attr: {
                    id: "25|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "华龙区",
                        attr: {
                            id: "25|09|01"
                        }
                    },
                    {
                        name: "清丰县",
                        attr: {
                            id: "25|09|02"
                        }
                    },
                    {
                        name: "南乐县",
                        attr: {
                            id: "25|09|03"
                        }
                    },
                    {
                        name: "范县",
                        attr: {
                            id: "25|09|04"
                        }
                    },
                    {
                        name: "台前县",
                        attr: {
                            id: "25|09|05"
                        }
                    },
                    {
                        name: "濮阳县",
                        attr: {
                            id: "25|09|06"
                        }
                    }
                ]
            },
            {
                name: "许昌",
                attr: {
                    id: "25|10|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "魏都区",
                        attr: {
                            id: "25|10|01"
                        }
                    },
                    {
                        name: "许昌县",
                        attr: {
                            id: "25|10|02"
                        }
                    },
                    {
                        name: "鄢陵县",
                        attr: {
                            id: "25|10|03"
                        }
                    },
                    {
                        name: "襄城县",
                        attr: {
                            id: "25|10|04"
                        }
                    },
                    {
                        name: "禹州市",
                        attr: {
                            id: "25|10|05"
                        }
                    },
                    {
                        name: "长葛市",
                        attr: {
                            id: "25|10|06"
                        }
                    }
                ]
            },
            {
                name: "漯河",
                attr: {
                    id: "25|11|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "源汇区",
                        attr: {
                            id: "25|11|01"
                        }
                    },
                    {
                        name: "郾城区",
                        attr: {
                            id: "25|11|02"
                        }
                    },
                    {
                        name: "召陵区",
                        attr: {
                            id: "25|11|03"
                        }
                    },
                    {
                        name: "舞阳县",
                        attr: {
                            id: "25|11|04"
                        }
                    },
                    {
                        name: "临颍县",
                        attr: {
                            id: "25|11|05"
                        }
                    }
                ]
            },
            {
                name: "三门峡",
                attr: {
                    id: "25|12|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "湖滨区",
                        attr: {
                            id: "25|12|01"
                        }
                    },
                    {
                        name: "渑池县",
                        attr: {
                            id: "25|12|02"
                        }
                    },
                    {
                        name: "陕县",
                        attr: {
                            id: "25|12|03"
                        }
                    },
                    {
                        name: "卢氏县",
                        attr: {
                            id: "25|12|04"
                        }
                    },
                    {
                        name: "义马市",
                        attr: {
                            id: "25|12|05"
                        }
                    },
                    {
                        name: "灵宝市",
                        attr: {
                            id: "25|12|06"
                        }
                    }
                ]
            },
            {
                name: "南阳",
                attr: {
                    id: "25|13|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "宛城区",
                        attr: {
                            id: "25|13|01"
                        }
                    },
                    {
                        name: "卧龙区",
                        attr: {
                            id: "25|13|02"
                        }
                    },
                    {
                        name: "南召县",
                        attr: {
                            id: "25|13|03"
                        }
                    },
                    {
                        name: "方城县",
                        attr: {
                            id: "25|13|04"
                        }
                    },
                    {
                        name: "西峡县",
                        attr: {
                            id: "25|13|05"
                        }
                    },
                    {
                        name: "镇平县",
                        attr: {
                            id: "25|13|06"
                        }
                    },
                    {
                        name: "内乡县",
                        attr: {
                            id: "25|13|07"
                        }
                    },
                    {
                        name: "淅川县",
                        attr: {
                            id: "25|13|08"
                        }
                    },
                    {
                        name: "社旗县",
                        attr: {
                            id: "25|13|09"
                        }
                    },
                    {
                        name: "唐河县",
                        attr: {
                            id: "25|13|10"
                        }
                    },
                    {
                        name: "新野县",
                        attr: {
                            id: "25|13|11"
                        }
                    },
                    {
                        name: "桐柏县",
                        attr: {
                            id: "25|13|12"
                        }
                    },
                    {
                        name: "邓州市",
                        attr: {
                            id: "25|13|13"
                        }
                    }
                ]
            },
            {
                name: "商丘",
                attr: {
                    id: "25|14|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "梁园区",
                        attr: {
                            id: "25|14|01"
                        }
                    },
                    {
                        name: "睢阳区",
                        attr: {
                            id: "25|14|02"
                        }
                    },
                    {
                        name: "民权县",
                        attr: {
                            id: "25|14|03"
                        }
                    },
                    {
                        name: "睢县",
                        attr: {
                            id: "25|14|04"
                        }
                    },
                    {
                        name: "宁陵县",
                        attr: {
                            id: "25|14|05"
                        }
                    },
                    {
                        name: "柘城县",
                        attr: {
                            id: "25|14|06"
                        }
                    },
                    {
                        name: "虞城县",
                        attr: {
                            id: "25|14|07"
                        }
                    },
                    {
                        name: "夏邑县",
                        attr: {
                            id: "25|14|08"
                        }
                    },
                    {
                        name: "永城市",
                        attr: {
                            id: "25|14|09"
                        }
                    }
                ]
            },
            {
                name: "信阳",
                attr: {
                    id: "25|15|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "浉河区",
                        attr: {
                            id: "25|15|01"
                        }
                    },
                    {
                        name: "平桥区",
                        attr: {
                            id: "25|15|02"
                        }
                    },
                    {
                        name: "罗山县",
                        attr: {
                            id: "25|15|03"
                        }
                    },
                    {
                        name: "光山县",
                        attr: {
                            id: "25|15|04"
                        }
                    },
                    {
                        name: "新县",
                        attr: {
                            id: "25|15|05"
                        }
                    },
                    {
                        name: "商城县",
                        attr: {
                            id: "25|15|06"
                        }
                    },
                    {
                        name: "固始县",
                        attr: {
                            id: "25|15|07"
                        }
                    },
                    {
                        name: "潢川县",
                        attr: {
                            id: "25|15|08"
                        }
                    },
                    {
                        name: "淮滨县",
                        attr: {
                            id: "25|15|09"
                        }
                    },
                    {
                        name: "息县",
                        attr: {
                            id: "25|15|10"
                        }
                    }
                ]
            },
            {
                name: "周口",
                attr: {
                    id: "25|16|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "川汇区",
                        attr: {
                            id: "25|16|01"
                        }
                    },
                    {
                        name: "扶沟县",
                        attr: {
                            id: "25|16|02"
                        }
                    },
                    {
                        name: "西华县",
                        attr: {
                            id: "25|16|03"
                        }
                    },
                    {
                        name: "商水县",
                        attr: {
                            id: "25|16|04"
                        }
                    },
                    {
                        name: "沈丘县",
                        attr: {
                            id: "25|16|05"
                        }
                    },
                    {
                        name: "郸城县",
                        attr: {
                            id: "25|16|06"
                        }
                    },
                    {
                        name: "淮阳县",
                        attr: {
                            id: "25|16|07"
                        }
                    },
                    {
                        name: "太康县",
                        attr: {
                            id: "25|16|08"
                        }
                    },
                    {
                        name: "鹿邑县",
                        attr: {
                            id: "25|16|09"
                        }
                    },
                    {
                        name: "项城市",
                        attr: {
                            id: "25|16|10"
                        }
                    }
                ]
            },
            {
                name: "驻马店",
                attr: {
                    id: "25|17|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "驿城区",
                        attr: {
                            id: "25|17|01"
                        }
                    },
                    {
                        name: "西平县",
                        attr: {
                            id: "25|17|02"
                        }
                    },
                    {
                        name: "上蔡县",
                        attr: {
                            id: "25|17|03"
                        }
                    },
                    {
                        name: "平舆县",
                        attr: {
                            id: "25|17|04"
                        }
                    },
                    {
                        name: "正阳县",
                        attr: {
                            id: "25|17|05"
                        }
                    },
                    {
                        name: "确山县",
                        attr: {
                            id: "25|17|06"
                        }
                    },
                    {
                        name: "泌阳县",
                        attr: {
                            id: "25|17|07"
                        }
                    },
                    {
                        name: "汝南县",
                        attr: {
                            id: "25|17|08"
                        }
                    },
                    {
                        name: "遂平县",
                        attr: {
                            id: "25|17|09"
                        }
                    },
                    {
                        name: "新蔡县",
                        attr: {
                            id: "25|17|10"
                        }
                    }
                ]
            },
            {
                name: "济源",
                attr: {
                    id: "25|18|00"
                }
            }
        ]
    },
    {
        name: "湖北",
        attr: {
            id: "26|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "郑州",
                attr: {
                    id: "26|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "江岸区",
                        attr: {
                            id: "26|01|01"
                        }
                    },
                    {
                        name: "江汉区",
                        attr: {
                            id: "26|01|02"
                        }
                    },
                    {
                        name: "硚口区",
                        attr: {
                            id: "26|01|03"
                        }
                    },
                    {
                        name: "汉阳区",
                        attr: {
                            id: "26|01|04"
                        }
                    },
                    {
                        name: "武昌区",
                        attr: {
                            id: "26|01|05"
                        }
                    },
                    {
                        name: "青山区",
                        attr: {
                            id: "26|01|06"
                        }
                    },
                    {
                        name: "洪山区",
                        attr: {
                            id: "26|01|07"
                        }
                    },
                    {
                        name: "东西湖区",
                        attr: {
                            id: "26|01|08"
                        }
                    },
                    {
                        name: "荥阳市",
                        attr: {
                            id: "26|01|09"
                        }
                    },
                    {
                        name: "蔡甸区",
                        attr: {
                            id: "26|01|10"
                        }
                    },
                    {
                        name: "江夏区",
                        attr: {
                            id: "26|01|11"
                        }
                    },
                    {
                        name: "黄陂区",
                        attr: {
                            id: "26|01|12"
                        }
                    },
                    {
                        name: "新洲区",
                        attr: {
                            id: "26|01|13"
                        }
                    }
                ]
            },
            {
                name: "黄石",
                attr: {
                    id: "26|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "黄石港区",
                        attr: {
                            id: "26|02|01"
                        }
                    },
                    {
                        name: "西塞山区",
                        attr: {
                            id: "26|02|02"
                        }
                    },
                    {
                        name: "下陆区",
                        attr: {
                            id: "26|02|03"
                        }
                    },
                    {
                        name: "铁山区",
                        attr: {
                            id: "26|02|04"
                        }
                    },
                    {
                        name: "阳新县",
                        attr: {
                            id: "26|02|05"
                        }
                    },
                    {
                        name: "大冶市",
                        attr: {
                            id: "26|02|06"
                        }
                    }
                ]
            },
            {
                name: "十堰",
                attr: {
                    id: "26|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "茅箭区",
                        attr: {
                            id: "26|03|01"
                        }
                    },
                    {
                        name: "张湾区",
                        attr: {
                            id: "26|03|02"
                        }
                    },
                    {
                        name: "郧县",
                        attr: {
                            id: "26|03|03"
                        }
                    },
                    {
                        name: "郧西县",
                        attr: {
                            id: "26|03|04"
                        }
                    },
                    {
                        name: "竹山县",
                        attr: {
                            id: "26|03|05"
                        }
                    },
                    {
                        name: "竹溪县",
                        attr: {
                            id: "26|03|06"
                        }
                    },
                    {
                        name: "房县",
                        attr: {
                            id: "26|03|07"
                        }
                    },
                    {
                        name: "丹江口市",
                        attr: {
                            id: "26|03|08"
                        }
                    }
                ]
            },
            {
                name: "宜昌",
                attr: {
                    id: "26|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "西陵区",
                        attr: {
                            id: "26|04|01"
                        }
                    },
                    {
                        name: "伍家岗区",
                        attr: {
                            id: "26|04|02"
                        }
                    },
                    {
                        name: "点军区",
                        attr: {
                            id: "26|04|03"
                        }
                    },
                    {
                        name: "猇亭区",
                        attr: {
                            id: "26|04|04"
                        }
                    },
                    {
                        name: "夷陵区",
                        attr: {
                            id: "26|04|05"
                        }
                    },
                    {
                        name: "远安县",
                        attr: {
                            id: "26|04|06"
                        }
                    },
                    {
                        name: "兴山县",
                        attr: {
                            id: "26|04|07"
                        }
                    },
                    {
                        name: "秭归县",
                        attr: {
                            id: "26|04|08"
                        }
                    },
                    {
                        name: "长阳土家族自治县",
                        attr: {
                            id: "26|04|09"
                        }
                    },
                    {
                        name: "五峰土家族自治县",
                        attr: {
                            id: "26|04|10"
                        }
                    },
                    {
                        name: "宜都市",
                        attr: {
                            id: "26|04|11"
                        }
                    },
                    {
                        name: "当阳市",
                        attr: {
                            id: "26|04|12"
                        }
                    },
                    {
                        name: "枝江市",
                        attr: {
                            id: "26|04|13"
                        }
                    }
                ]
            },
            {
                name: "襄阳",
                attr: {
                    id: "26|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "襄城区",
                        attr: {
                            id: "26|05|01"
                        }
                    },
                    {
                        name: "樊城区",
                        attr: {
                            id: "26|05|02"
                        }
                    },
                    {
                        name: "襄州区",
                        attr: {
                            id: "26|05|03"
                        }
                    },
                    {
                        name: "南漳县",
                        attr: {
                            id: "26|05|04"
                        }
                    },
                    {
                        name: "谷城县",
                        attr: {
                            id: "26|05|05"
                        }
                    },
                    {
                        name: "保康县",
                        attr: {
                            id: "26|05|06"
                        }
                    },
                    {
                        name: "老河口市",
                        attr: {
                            id: "26|05|07"
                        }
                    },
                    {
                        name: "枣阳市",
                        attr: {
                            id: "26|05|08"
                        }
                    },
                    {
                        name: "宜城市",
                        attr: {
                            id: "26|05|09"
                        }
                    }
                ]
            },
            {
                name: "鄂州",
                attr: {
                    id: "26|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "梁子湖区",
                        attr: {
                            id: "26|06|01"
                        }
                    },
                    {
                        name: "华容区",
                        attr: {
                            id: "26|06|02"
                        }
                    },
                    {
                        name: "鄂城区",
                        attr: {
                            id: "26|06|03"
                        }
                    }
                ]
            },
            {
                name: "荆门",
                attr: {
                    id: "26|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "东宝区",
                        attr: {
                            id: "26|07|01"
                        }
                    },
                    {
                        name: "掇刀区",
                        attr: {
                            id: "26|07|02"
                        }
                    },
                    {
                        name: "京山县",
                        attr: {
                            id: "26|03|03"
                        }
                    },
                    {
                        name: "沙洋县",
                        attr: {
                            id: "26|07|04"
                        }
                    },
                    {
                        name: "钟祥市",
                        attr: {
                            id: "26|07|05"
                        }
                    }
                ]
            },
            {
                name: "孝感",
                attr: {
                    id: "26|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "孝南区",
                        attr: {
                            id: "26|08|01"
                        }
                    },
                    {
                        name: "孝昌县",
                        attr: {
                            id: "26|08|02"
                        }
                    },
                    {
                        name: "大悟县",
                        attr: {
                            id: "26|08|03"
                        }
                    },
                    {
                        name: "云梦县",
                        attr: {
                            id: "26|08|04"
                        }
                    },
                    {
                        name: "应城市",
                        attr: {
                            id: "26|08|05"
                        }
                    },
                    {
                        name: "安陆市",
                        attr: {
                            id: "26|08|06"
                        }
                    },
                    {
                        name: "汉川市",
                        attr: {
                            id: "26|08|07"
                        }
                    }
                ]
            },
            {
                name: "荆州",
                attr: {
                    id: "26|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "沙市区",
                        attr: {
                            id: "26|09|01"
                        }
                    },
                    {
                        name: "荆州区",
                        attr: {
                            id: "26|09|02"
                        }
                    },
                    {
                        name: "公安县",
                        attr: {
                            id: "26|09|03"
                        }
                    },
                    {
                        name: "监利县",
                        attr: {
                            id: "26|09|04"
                        }
                    },
                    {
                        name: "江陵县",
                        attr: {
                            id: "26|09|05"
                        }
                    },
                    {
                        name: "石首市",
                        attr: {
                            id: "26|09|06"
                        }
                    },
                    {
                        name: "洪湖市",
                        attr: {
                            id: "26|09|07"
                        }
                    },
                    {
                        name: "松滋市",
                        attr: {
                            id: "26|09|08"
                        }
                    }
                ]
            },
            {
                name: "黄冈",
                attr: {
                    id: "26|10|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "黄州区",
                        attr: {
                            id: "26|10|01"
                        }
                    },
                    {
                        name: "团风县",
                        attr: {
                            id: "26|10|02"
                        }
                    },
                    {
                        name: "红安县",
                        attr: {
                            id: "26|10|03"
                        }
                    },
                    {
                        name: "罗田县",
                        attr: {
                            id: "26|10|04"
                        }
                    },
                    {
                        name: "英山县",
                        attr: {
                            id: "26|10|05"
                        }
                    },
                    {
                        name: "浠水县",
                        attr: {
                            id: "26|10|06"
                        }
                    },
                    {
                        name: "蕲春县",
                        attr: {
                            id: "26|10|07"
                        }
                    },
                    {
                        name: "黄梅县",
                        attr: {
                            id: "26|10|08"
                        }
                    },
                    {
                        name: "麻城市",
                        attr: {
                            id: "26|10|09"
                        }
                    },
                    {
                        name: "武穴市",
                        attr: {
                            id: "26|10|10"
                        }
                    }
                ]
            },
            {
                name: "咸宁",
                attr: {
                    id: "26|11|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "咸安区",
                        attr: {
                            id: "26|11|01"
                        }
                    },
                    {
                        name: "嘉鱼县",
                        attr: {
                            id: "26|11|02"
                        }
                    },
                    {
                        name: "通城县",
                        attr: {
                            id: "26|11|03"
                        }
                    },
                    {
                        name: "崇阳县",
                        attr: {
                            id: "26|11|04"
                        }
                    },
                    {
                        name: "通山县",
                        attr: {
                            id: "26|11|05"
                        }
                    },
                    {
                        name: "赤壁市",
                        attr: {
                            id: "26|11|06"
                        }
                    }
                ]
            },
            {
                name: "随州",
                attr: {
                    id: "26|12|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "曾都区",
                        attr: {
                            id: "26|12|01"
                        }
                    },
                    {
                        name: "随县",
                        attr: {
                            id: "26|12|02"
                        }
                    },
                    {
                        name: "广水市",
                        attr: {
                            id: "26|12|03"
                        }
                    }
                ]
            },
            {
                name: "恩施",
                attr: {
                    id: "26|13|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "恩施市",
                        attr: {
                            id: "26|13|01"
                        }
                    },
                    {
                        name: "利川市",
                        attr: {
                            id: "26|13|02"
                        }
                    },
                    {
                        name: "建始县",
                        attr: {
                            id: "26|13|03"
                        }
                    },
                    {
                        name: "巴东县",
                        attr: {
                            id: "26|13|04"
                        }
                    },
                    {
                        name: "宣恩县",
                        attr: {
                            id: "26|13|05"
                        }
                    },
                    {
                        name: "咸丰县",
                        attr: {
                            id: "26|13|06"
                        }
                    },
                    {
                        name: "来凤县",
                        attr: {
                            id: "26|13|07"
                        }
                    },
                    {
                        name: "鹤峰县",
                        attr: {
                            id: "26|13|08"
                        }
                    }
                ]
            },
            {
                name: "仙桃",
                attr: {
                    id: "26|14|00"
                }
            },
            {
                name: "潜江",
                attr: {
                    id: "26|15|00"
                }
            },
            {
                name: "天门",
                attr: {
                    id: "26|16|00"
                }
            },
            {
                name: "神农架",
                attr: {
                    id: "26|17|00"
                }
            }
        ]
    },
    {
        name: "湖南",
        attr: {
            id: "27|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "长沙",
                attr: {
                    id: "27|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "芙蓉区",
                        attr: {
                            id: "27|01|01"
                        }
                    },
                    {
                        name: "天心区",
                        attr: {
                            id: "27|01|02"
                        }
                    },
                    {
                        name: "岳麓区",
                        attr: {
                            id: "27|01|03"
                        }
                    },
                    {
                        name: "开福区",
                        attr: {
                            id: "27|01|04"
                        }
                    },
                    {
                        name: "雨花区",
                        attr: {
                            id: "27|01|05"
                        }
                    },
                    {
                        name: "长沙县",
                        attr: {
                            id: "27|01|06"
                        }
                    },
                    {
                        name: "望城县",
                        attr: {
                            id: "27|01|07"
                        }
                    },
                    {
                        name: "宁乡县",
                        attr: {
                            id: "27|01|08"
                        }
                    },
                    {
                        name: "浏阳市",
                        attr: {
                            id: "27|01|09"
                        }
                    }
                ]
            },
            {
                name: "株洲",
                attr: {
                    id: "27|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "荷塘区",
                        attr: {
                            id: "27|02|01"
                        }
                    },
                    {
                        name: "芦淞区",
                        attr: {
                            id: "27|02|02"
                        }
                    },
                    {
                        name: "石峰区",
                        attr: {
                            id: "27|02|03"
                        }
                    },
                    {
                        name: "天元区",
                        attr: {
                            id: "27|02|04"
                        }
                    },
                    {
                        name: "株洲县",
                        attr: {
                            id: "27|02|05"
                        }
                    },
                    {
                        name: "攸县",
                        attr: {
                            id: "27|02|06"
                        }
                    },
                    {
                        name: "茶陵县",
                        attr: {
                            id: "27|02|07"
                        }
                    },
                    {
                        name: "炎陵县",
                        attr: {
                            id: "27|02|08"
                        }
                    },
                    {
                        name: "醴陵市",
                        attr: {
                            id: "27|02|09"
                        }
                    }
                ]
            },
            {
                name: "湘潭",
                attr: {
                    id: "27|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "雨湖区",
                        attr: {
                            id: "27|03|01"
                        }
                    },
                    {
                        name: "岳塘区",
                        attr: {
                            id: "27|03|02"
                        }
                    },
                    {
                        name: "湘潭县",
                        attr: {
                            id: "27|03|03"
                        }
                    },
                    {
                        name: "湘乡市",
                        attr: {
                            id: "27|03|04"
                        }
                    },
                    {
                        name: "韶山市",
                        attr: {
                            id: "27|03|05"
                        }
                    }
                ]
            },
            {
                name: "衡阳",
                attr: {
                    id: "27|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "珠晖区",
                        attr: {
                            id: "27|04|01"
                        }
                    },
                    {
                        name: "雁峰区",
                        attr: {
                            id: "27|04|02"
                        }
                    },
                    {
                        name: "石鼓区",
                        attr: {
                            id: "27|04|03"
                        }
                    },
                    {
                        name: "蒸湘区",
                        attr: {
                            id: "27|04|04"
                        }
                    },
                    {
                        name: "南岳区",
                        attr: {
                            id: "27|04|05"
                        }
                    },
                    {
                        name: "衡阳县",
                        attr: {
                            id: "27|04|06"
                        }
                    },
                    {
                        name: "衡南县",
                        attr: {
                            id: "27|04|07"
                        }
                    },
                    {
                        name: "衡山县",
                        attr: {
                            id: "27|04|08"
                        }
                    },
                    {
                        name: "衡东县",
                        attr: {
                            id: "27|04|09"
                        }
                    },
                    {
                        name: "祁东县",
                        attr: {
                            id: "27|04|10"
                        }
                    },
                    {
                        name: "耒阳市",
                        attr: {
                            id: "27|04|11"
                        }
                    },
                    {
                        name: "常宁市",
                        attr: {
                            id: "27|04|12"
                        }
                    }
                ]
            },
            {
                name: "邵阳",
                attr: {
                    id: "27|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "双清区",
                        attr: {
                            id: "27|05|01"
                        }
                    },
                    {
                        name: "大祥区",
                        attr: {
                            id: "27|05|02"
                        }
                    },
                    {
                        name: "北塔区",
                        attr: {
                            id: "27|05|03"
                        }
                    },
                    {
                        name: "邵东县",
                        attr: {
                            id: "27|05|04"
                        }
                    },
                    {
                        name: "新邵县",
                        attr: {
                            id: "27|05|05"
                        }
                    },
                    {
                        name: "邵阳县",
                        attr: {
                            id: "27|05|06"
                        }
                    },
                    {
                        name: "隆回县",
                        attr: {
                            id: "27|05|07"
                        }
                    },
                    {
                        name: "洞口县",
                        attr: {
                            id: "27|05|08"
                        }
                    },
                    {
                        name: "绥宁县",
                        attr: {
                            id: "27|05|09"
                        }
                    },
                    {
                        name: "新宁县",
                        attr: {
                            id: "27|05|10"
                        }
                    },
                    {
                        name: "城步苗族自治县",
                        attr: {
                            id: "27|05|11"
                        }
                    },
                    {
                        name: "武冈市",
                        attr: {
                            id: "27|05|12"
                        }
                    }
                ]
            },
            {
                name: "岳阳",
                attr: {
                    id: "27|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "岳阳楼区",
                        attr: {
                            id: "27|06|01"
                        }
                    },
                    {
                        name: "云溪区",
                        attr: {
                            id: "27|06|02"
                        }
                    },
                    {
                        name: "君山区",
                        attr: {
                            id: "27|06|03"
                        }
                    },
                    {
                        name: "岳阳县",
                        attr: {
                            id: "27|06|04"
                        }
                    },
                    {
                        name: "华容县",
                        attr: {
                            id: "27|06|05"
                        }
                    },
                    {
                        name: "湘阴县",
                        attr: {
                            id: "27|06|06"
                        }
                    },
                    {
                        name: "平江县",
                        attr: {
                            id: "27|06|07"
                        }
                    },
                    {
                        name: "汨罗市",
                        attr: {
                            id: "27|06|08"
                        }
                    },
                    {
                        name: "临湘市",
                        attr: {
                            id: "27|06|09"
                        }
                    }
                ]
            },
            {
                name: "常德",
                attr: {
                    id: "27|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "武陵区",
                        attr: {
                            id: "27|07|01"
                        }
                    },
                    {
                        name: "鼎城区",
                        attr: {
                            id: "27|07|02"
                        }
                    },
                    {
                        name: "安乡县",
                        attr: {
                            id: "27|07|03"
                        }
                    },
                    {
                        name: "汉寿县",
                        attr: {
                            id: "27|07|04"
                        }
                    },
                    {
                        name: "澧县",
                        attr: {
                            id: "27|07|05"
                        }
                    },
                    {
                        name: "临澧县",
                        attr: {
                            id: "27|07|06"
                        }
                    },
                    {
                        name: "桃源县",
                        attr: {
                            id: "27|07|07"
                        }
                    },
                    {
                        name: "石门县",
                        attr: {
                            id: "27|07|08"
                        }
                    },
                    {
                        name: "津市市",
                        attr: {
                            id: "27|07|09"
                        }
                    }
                ]
            },
            {
                name: "张家界",
                attr: {
                    id: "27|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "永定区",
                        attr: {
                            id: "27|08|01"
                        }
                    },
                    {
                        name: "武陵源区",
                        attr: {
                            id: "27|08|02"
                        }
                    },
                    {
                        name: "慈利县",
                        attr: {
                            id: "27|08|03"
                        }
                    },
                    {
                        name: "桑植县",
                        attr: {
                            id: "27|08|04"
                        }
                    }
                ]
            },
            {
                name: "益阳",
                attr: {
                    id: "27|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "资阳区",
                        attr: {
                            id: "27|09|01"
                        }
                    },
                    {
                        name: "赫山区",
                        attr: {
                            id: "27|09|02"
                        }
                    },
                    {
                        name: "南县",
                        attr: {
                            id: "27|09|03"
                        }
                    },
                    {
                        name: "桃江县",
                        attr: {
                            id: "27|09|04"
                        }
                    },
                    {
                        name: "安化县",
                        attr: {
                            id: "27|09|05"
                        }
                    },
                    {
                        name: "沅江市",
                        attr: {
                            id: "27|09|06"
                        }
                    }
                ]
            },
            {
                name: "郴州",
                attr: {
                    id: "27|10|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "北湖区",
                        attr: {
                            id: "27|10|01"
                        }
                    },
                    {
                        name: "苏仙区",
                        attr: {
                            id: "27|10|02"
                        }
                    },
                    {
                        name: "桂阳县",
                        attr: {
                            id: "27|10|03"
                        }
                    },
                    {
                        name: "宜章县",
                        attr: {
                            id: "27|10|04"
                        }
                    },
                    {
                        name: "永兴县",
                        attr: {
                            id: "27|10|05"
                        }
                    },
                    {
                        name: "嘉禾县",
                        attr: {
                            id: "27|10|06"
                        }
                    },
                    {
                        name: "临武县",
                        attr: {
                            id: "27|10|07"
                        }
                    },
                    {
                        name: "汝城县",
                        attr: {
                            id: "27|10|08"
                        }
                    },
                    {
                        name: "桂东县",
                        attr: {
                            id: "27|10|09"
                        }
                    },
                    {
                        name: "安仁县",
                        attr: {
                            id: "27|10|10"
                        }
                    },
                    {
                        name: "资兴市",
                        attr: {
                            id: "27|10|11"
                        }
                    }
                ]
            },
            {
                name: "永州",
                attr: {
                    id: "27|11|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "零陵区",
                        attr: {
                            id: "27|11|01"
                        }
                    },
                    {
                        name: "冷水滩区",
                        attr: {
                            id: "27|11|02"
                        }
                    },
                    {
                        name: "祁阳县",
                        attr: {
                            id: "27|11|03"
                        }
                    },
                    {
                        name: "东安县",
                        attr: {
                            id: "27|11|04"
                        }
                    },
                    {
                        name: "双牌县",
                        attr: {
                            id: "27|11|05"
                        }
                    },
                    {
                        name: "道县",
                        attr: {
                            id: "27|11|06"
                        }
                    },
                    {
                        name: "江永县",
                        attr: {
                            id: "27|11|07"
                        }
                    },
                    {
                        name: "宁远县",
                        attr: {
                            id: "27|11|08"
                        }
                    },
                    {
                        name: "蓝山县",
                        attr: {
                            id: "27|11|09"
                        }
                    },
                    {
                        name: "新田县",
                        attr: {
                            id: "27|11|10"
                        }
                    },
                    {
                        name: "江华瑶族自治县",
                        attr: {
                            id: "27|11|11"
                        }
                    }
                ]
            },
            {
                name: "怀化",
                attr: {
                    id: "27|12|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "鹤城区",
                        attr: {
                            id: "27|12|01"
                        }
                    },
                    {
                        name: "中方县",
                        attr: {
                            id: "27|12|02"
                        }
                    },
                    {
                        name: "沅陵县",
                        attr: {
                            id: "27|12|03"
                        }
                    },
                    {
                        name: "辰溪县",
                        attr: {
                            id: "27|12|04"
                        }
                    },
                    {
                        name: "溆浦县",
                        attr: {
                            id: "27|12|05"
                        }
                    },
                    {
                        name: "会同县",
                        attr: {
                            id: "27|12|06"
                        }
                    },
                    {
                        name: "麻阳苗族自治县",
                        attr: {
                            id: "27|12|07"
                        }
                    },
                    {
                        name: "新晃侗族自治县",
                        attr: {
                            id: "27|12|08"
                        }
                    },
                    {
                        name: "芷江侗族自治县",
                        attr: {
                            id: "27|12|09"
                        }
                    },
                    {
                        name: "靖州苗族侗族自治县",
                        attr: {
                            id: "27|12|10"
                        }
                    },
                    {
                        name: "通道侗族自治县",
                        attr: {
                            id: "27|12|11"
                        }
                    },
                    {
                        name: "洪江市",
                        attr: {
                            id: "27|12|12"
                        }
                    }
                ]
            },
            {
                name: "娄底",
                attr: {
                    id: "27|13|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "娄星区",
                        attr: {
                            id: "27|13|01"
                        }
                    },
                    {
                        name: "双峰县",
                        attr: {
                            id: "27|13|02"
                        }
                    },
                    {
                        name: "新化县",
                        attr: {
                            id: "27|13|03"
                        }
                    },
                    {
                        name: "冷水江市",
                        attr: {
                            id: "27|13|04"
                        }
                    },
                    {
                        name: "涟源市",
                        attr: {
                            id: "27|13|05"
                        }
                    }
                ]
            },
            {
                name: "湘西",
                attr: {
                    id: "27|14|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "吉首市",
                        attr: {
                            id: "27|14|01"
                        }
                    },
                    {
                        name: "泸溪县",
                        attr: {
                            id: "27|14|02"
                        }
                    },
                    {
                        name: "凤凰县",
                        attr: {
                            id: "27|14|03"
                        }
                    },
                    {
                        name: "花垣县",
                        attr: {
                            id: "27|14|04"
                        }
                    },
                    {
                        name: "保靖县",
                        attr: {
                            id: "27|14|05"
                        }
                    },
                    {
                        name: "古丈县",
                        attr: {
                            id: "27|14|06"
                        }
                    },
                    {
                        name: "永顺县",
                        attr: {
                            id: "27|14|07"
                        }
                    },
                    {
                        name: "龙山县",
                        attr: {
                            id: "27|14|08"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "广东",
        attr: {
            id: "28|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "广州",
                attr: {
                    id: "28|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "荔湾区",
                        attr: {
                            id: "28|01|01"
                        }
                    },
                    {
                        name: "越秀区",
                        attr: {
                            id: "28|01|02"
                        }
                    },
                    {
                        name: "海珠区",
                        attr: {
                            id: "28|01|03"
                        }
                    },
                    {
                        name: "天河区",
                        attr: {
                            id: "28|01|04"
                        }
                    },
                    {
                        name: "白云区",
                        attr: {
                            id: "28|01|05"
                        }
                    },
                    {
                        name: "黄埔区",
                        attr: {
                            id: "28|01|06"
                        }
                    },
                    {
                        name: "番禺区",
                        attr: {
                            id: "28|01|07"
                        }
                    },
                    {
                        name: "花都区",
                        attr: {
                            id: "28|01|08"
                        }
                    },
                    {
                        name: "南沙区",
                        attr: {
                            id: "28|01|09"
                        }
                    },
                    {
                        name: "萝岗区",
                        attr: {
                            id: "28|01|10"
                        }
                    },
                    {
                        name: "增城市",
                        attr: {
                            id: "28|01|11"
                        }
                    },
                    {
                        name: "从化市",
                        attr: {
                            id: "28|01|12"
                        }
                    }
                ]
            },
            {
                name: "韶关",
                attr: {
                    id: "28|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "武江区",
                        attr: {
                            id: "28|02|01"
                        }
                    },
                    {
                        name: "浈江区",
                        attr: {
                            id: "28|02|02"
                        }
                    },
                    {
                        name: "曲江区",
                        attr: {
                            id: "28|02|03"
                        }
                    },
                    {
                        name: "始兴县",
                        attr: {
                            id: "28|02|04"
                        }
                    },
                    {
                        name: "仁化县",
                        attr: {
                            id: "28|02|05"
                        }
                    },
                    {
                        name: "翁源县",
                        attr: {
                            id: "28|02|06"
                        }
                    },
                    {
                        name: "乳源瑶族自治县",
                        attr: {
                            id: "28|02|07"
                        }
                    },
                    {
                        name: "新丰县",
                        attr: {
                            id: "28|02|08"
                        }
                    },
                    {
                        name: "乐昌市",
                        attr: {
                            id: "28|02|09"
                        }
                    },
                    {
                        name: "南雄市",
                        attr: {
                            id: "28|02|10"
                        }
                    }
                ]
            },
            {
                name: "深圳",
                attr: {
                    id: "28|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "罗湖区",
                        attr: {
                            id: "28|03|01"
                        }
                    },
                    {
                        name: "福田区",
                        attr: {
                            id: "28|03|02"
                        }
                    },
                    {
                        name: "南山区",
                        attr: {
                            id: "28|03|03"
                        }
                    },
                    {
                        name: "宝安区",
                        attr: {
                            id: "28|03|04"
                        }
                    },
                    {
                        name: "龙岗区",
                        attr: {
                            id: "28|03|05"
                        }
                    },
                    {
                        name: "盐田区",
                        attr: {
                            id: "28|03|06"
                        }
                    }
                ]
            },
            {
                name: "珠海",
                attr: {
                    id: "28|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "香洲区",
                        attr: {
                            id: "28|04|01"
                        }
                    },
                    {
                        name: "斗门区",
                        attr: {
                            id: "28|04|02"
                        }
                    },
                    {
                        name: "金湾区",
                        attr: {
                            id: "28|04|03"
                        }
                    }
                ]
            },
            {
                name: "汕头",
                attr: {
                    id: "28|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "龙湖区",
                        attr: {
                            id: "28|05|01"
                        }
                    },
                    {
                        name: "金平区",
                        attr: {
                            id: "28|05|02"
                        }
                    },
                    {
                        name: "濠江区",
                        attr: {
                            id: "28|05|03"
                        }
                    },
                    {
                        name: "潮阳区",
                        attr: {
                            id: "28|05|04"
                        }
                    },
                    {
                        name: "潮南区",
                        attr: {
                            id: "28|05|05"
                        }
                    },
                    {
                        name: "澄海区",
                        attr: {
                            id: "28|05|06"
                        }
                    },
                    {
                        name: "南澳县",
                        attr: {
                            id: "28|05|07"
                        }
                    }
                ]
            },
            {
                name: "佛山",
                attr: {
                    id: "28|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "禅城区",
                        attr: {
                            id: "28|06|01"
                        }
                    },
                    {
                        name: "南海区",
                        attr: {
                            id: "28|06|02"
                        }
                    },
                    {
                        name: "顺德区",
                        attr: {
                            id: "28|06|03"
                        }
                    },
                    {
                        name: "三水区",
                        attr: {
                            id: "28|06|04"
                        }
                    },
                    {
                        name: "高明区",
                        attr: {
                            id: "28|06|05"
                        }
                    }
                ]
            },
            {
                name: "江门",
                attr: {
                    id: "28|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "蓬江区",
                        attr: {
                            id: "28|07|01"
                        }
                    },
                    {
                        name: "江海区",
                        attr: {
                            id: "28|07|02"
                        }
                    },
                    {
                        name: "新会区",
                        attr: {
                            id: "28|03|03"
                        }
                    },
                    {
                        name: "台山市",
                        attr: {
                            id: "28|07|04"
                        }
                    },
                    {
                        name: "开平市",
                        attr: {
                            id: "28|07|05"
                        }
                    },
                    {
                        name: "鹤山市",
                        attr: {
                            id: "28|07|06"
                        }
                    },
                    {
                        name: "恩平市",
                        attr: {
                            id: "28|07|07"
                        }
                    }
                ]
            },
            {
                name: "湛江",
                attr: {
                    id: "28|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "赤坎区",
                        attr: {
                            id: "28|08|01"
                        }
                    },
                    {
                        name: "霞山区",
                        attr: {
                            id: "28|08|02"
                        }
                    },
                    {
                        name: "坡头区",
                        attr: {
                            id: "28|08|03"
                        }
                    },
                    {
                        name: "麻章区",
                        attr: {
                            id: "28|08|04"
                        }
                    },
                    {
                        name: "遂溪县",
                        attr: {
                            id: "28|08|05"
                        }
                    },
                    {
                        name: "徐闻县",
                        attr: {
                            id: "28|08|06"
                        }
                    },
                    {
                        name: "廉江市",
                        attr: {
                            id: "28|08|07"
                        }
                    },
                    {
                        name: "雷州市",
                        attr: {
                            id: "28|08|08"
                        }
                    },
                    {
                        name: "吴川市",
                        attr: {
                            id: "28|08|09"
                        }
                    }
                ]
            },
            {
                name: "茂名",
                attr: {
                    id: "28|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "茂南区",
                        attr: {
                            id: "28|09|01"
                        }
                    },
                    {
                        name: "茂港区",
                        attr: {
                            id: "28|09|02"
                        }
                    },
                    {
                        name: "电白县",
                        attr: {
                            id: "28|09|03"
                        }
                    },
                    {
                        name: "高州市",
                        attr: {
                            id: "28|09|04"
                        }
                    },
                    {
                        name: "化州市",
                        attr: {
                            id: "28|09|05"
                        }
                    },
                    {
                        name: "信宜市",
                        attr: {
                            id: "28|09|06"
                        }
                    }
                ]
            },
            {
                name: "肇庆",
                attr: {
                    id: "28|10|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "端州区",
                        attr: {
                            id: "28|10|01"
                        }
                    },
                    {
                        name: "鼎湖区",
                        attr: {
                            id: "28|10|02"
                        }
                    },
                    {
                        name: "广宁县",
                        attr: {
                            id: "28|10|03"
                        }
                    },
                    {
                        name: "怀集县",
                        attr: {
                            id: "28|10|04"
                        }
                    },
                    {
                        name: "封开县",
                        attr: {
                            id: "28|10|05"
                        }
                    },
                    {
                        name: "德庆县",
                        attr: {
                            id: "28|10|06"
                        }
                    },
                    {
                        name: "高要市",
                        attr: {
                            id: "28|10|07"
                        }
                    },
                    {
                        name: "四会市",
                        attr: {
                            id: "28|10|08"
                        }
                    }
                ]
            },
            {
                name: "惠州",
                attr: {
                    id: "28|11|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "惠城区",
                        attr: {
                            id: "28|11|01"
                        }
                    },
                    {
                        name: "惠阳区",
                        attr: {
                            id: "28|11|02"
                        }
                    },
                    {
                        name: "博罗县",
                        attr: {
                            id: "28|11|03"
                        }
                    },
                    {
                        name: "惠东县",
                        attr: {
                            id: "28|11|04"
                        }
                    },
                    {
                        name: "龙门县",
                        attr: {
                            id: "28|11|05"
                        }
                    }
                ]
            },
            {
                name: "梅州",
                attr: {
                    id: "28|12|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "梅江区",
                        attr: {
                            id: "28|12|01"
                        }
                    },
                    {
                        name: "梅县",
                        attr: {
                            id: "28|12|02"
                        }
                    },
                    {
                        name: "大埔县",
                        attr: {
                            id: "28|12|03"
                        }
                    },
                    {
                        name: "丰顺县",
                        attr: {
                            id: "28|12|04"
                        }
                    },
                    {
                        name: "五华县",
                        attr: {
                            id: "28|12|05"
                        }
                    },
                    {
                        name: "平远县",
                        attr: {
                            id: "28|12|06"
                        }
                    },
                    {
                        name: "蕉岭县",
                        attr: {
                            id: "28|12|07"
                        }
                    },
                    {
                        name: "兴宁市",
                        attr: {
                            id: "28|12|08"
                        }
                    }
                ]
            },
            {
                name: "汕尾",
                attr: {
                    id: "28|13|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "城区",
                        attr: {
                            id: "28|13|01"
                        }
                    },
                    {
                        name: "海丰县",
                        attr: {
                            id: "28|13|02"
                        }
                    },
                    {
                        name: "陆河县",
                        attr: {
                            id: "28|13|03"
                        }
                    },
                    {
                        name: "陆丰市",
                        attr: {
                            id: "28|13|04"
                        }
                    }
                ]
            },
            {
                name: "河源",
                attr: {
                    id: "28|14|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "源城区",
                        attr: {
                            id: "28|14|01"
                        }
                    },
                    {
                        name: "紫金县",
                        attr: {
                            id: "28|14|02"
                        }
                    },
                    {
                        name: "龙川县",
                        attr: {
                            id: "28|14|03"
                        }
                    },
                    {
                        name: "连平县",
                        attr: {
                            id: "28|14|04"
                        }
                    },
                    {
                        name: "和平县",
                        attr: {
                            id: "28|14|05"
                        }
                    },
                    {
                        name: "东源县",
                        attr: {
                            id: "28|14|06"
                        }
                    }
                ]
            },
            {
                name: "阳江",
                attr: {
                    id: "28|15|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "江城区",
                        attr: {
                            id: "28|15|01"
                        }
                    },
                    {
                        name: "阳西县",
                        attr: {
                            id: "28|15|02"
                        }
                    },
                    {
                        name: "阳东县",
                        attr: {
                            id: "28|15|03"
                        }
                    },
                    {
                        name: "阳春市",
                        attr: {
                            id: "28|15|04"
                        }
                    }
                ]
            },
            {
                name: "清远",
                attr: {
                    id: "28|16|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "清城区",
                        attr: {
                            id: "28|16|01"
                        }
                    },
                    {
                        name: "佛冈县",
                        attr: {
                            id: "28|16|02"
                        }
                    },
                    {
                        name: "阳山县",
                        attr: {
                            id: "28|16|03"
                        }
                    },
                    {
                        name: "连山壮族瑶族自治县",
                        attr: {
                            id: "28|16|04"
                        }
                    },
                    {
                        name: "连南瑶族自治县",
                        attr: {
                            id: "28|16|05"
                        }
                    },
                    {
                        name: "清新县",
                        attr: {
                            id: "28|16|06"
                        }
                    },
                    {
                        name: "英德市",
                        attr: {
                            id: "28|16|07"
                        }
                    },
                    {
                        name: "连州市",
                        attr: {
                            id: "28|16|08"
                        }
                    }
                ]
            },
            {
                name: "东莞",
                attr: {
                    id: "281700"
                }
            },
            {
                name: "中山",
                attr: {
                    id: "281800"
                }
            },
            {
                name: "潮州",
                attr: {
                    id: "28|19|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "湘桥区",
                        attr: {
                            id: "28|19|01"
                        }
                    },
                    {
                        name: "潮安县",
                        attr: {
                            id: "28|19|02"
                        }
                    },
                    {
                        name: "饶平县",
                        attr: {
                            id: "28|19|03"
                        }
                    }
                ]
            },
            {
                name: "揭阳",
                attr: {
                    id: "28|20|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "榕城区",
                        attr: {
                            id: "28|20|01"
                        }
                    },
                    {
                        name: "揭东县",
                        attr: {
                            id: "28|20|02"
                        }
                    },
                    {
                        name: "揭西县",
                        attr: {
                            id: "28|20|03"
                        }
                    },
                    {
                        name: "惠来县",
                        attr: {
                            id: "28|20|04"
                        }
                    },
                    {
                        name: "普宁市",
                        attr: {
                            id: "28|20|05"
                        }
                    }
                ]
            },
            {
                name: "云浮",
                attr: {
                    id: "28|21|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "云城区",
                        attr: {
                            id: "28|21|01"
                        }
                    },
                    {
                        name: "新兴县",
                        attr: {
                            id: "28|21|02"
                        }
                    },
                    {
                        name: "郁南县",
                        attr: {
                            id: "28|21|03"
                        }
                    },
                    {
                        name: "云安县",
                        attr: {
                            id: "28|21|04"
                        }
                    },
                    {
                        name: "罗定市",
                        attr: {
                            id: "28|21|05"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "广西",
        attr: {
            id: "29|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "南宁",
                attr: {
                    id: "29|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "兴宁区",
                        attr: {
                            id: "29|01|01"
                        }
                    },
                    {
                        name: "青秀区",
                        attr: {
                            id: "29|01|02"
                        }
                    },
                    {
                        name: "江南区",
                        attr: {
                            id: "29|01|03"
                        }
                    },
                    {
                        name: "西乡塘区",
                        attr: {
                            id: "29|01|04"
                        }
                    },
                    {
                        name: "良庆区",
                        attr: {
                            id: "29|01|05"
                        }
                    },
                    {
                        name: "邕宁区",
                        attr: {
                            id: "29|01|06"
                        }
                    },
                    {
                        name: "武鸣县",
                        attr: {
                            id: "29|01|07"
                        }
                    },
                    {
                        name: "隆安县",
                        attr: {
                            id: "29|01|08"
                        }
                    },
                    {
                        name: "马山县",
                        attr: {
                            id: "29|01|09"
                        }
                    },
                    {
                        name: "上林县",
                        attr: {
                            id: "29|01|10"
                        }
                    },
                    {
                        name: "宾阳县",
                        attr: {
                            id: "29|01|11"
                        }
                    },
                    {
                        name: "横县",
                        attr: {
                            id: "29|01|12"
                        }
                    }
                ]
            },
            {
                name: "柳州",
                attr: {
                    id: "29|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "城中区",
                        attr: {
                            id: "29|02|01"
                        }
                    },
                    {
                        name: "鱼峰区",
                        attr: {
                            id: "29|02|02"
                        }
                    },
                    {
                        name: "柳南区",
                        attr: {
                            id: "29|02|03"
                        }
                    },
                    {
                        name: "柳北区",
                        attr: {
                            id: "29|02|04"
                        }
                    },
                    {
                        name: "柳江县",
                        attr: {
                            id: "29|02|05"
                        }
                    },
                    {
                        name: "柳城县",
                        attr: {
                            id: "29|02|06"
                        }
                    },
                    {
                        name: "鹿寨县",
                        attr: {
                            id: "29|02|07"
                        }
                    },
                    {
                        name: "融安县",
                        attr: {
                            id: "29|02|08"
                        }
                    },
                    {
                        name: "融水苗族自治县",
                        attr: {
                            id: "29|02|09"
                        }
                    },
                    {
                        name: "三江侗族自治县",
                        attr: {
                            id: "29|02|10"
                        }
                    }
                ]
            },
            {
                name: "桂林",
                attr: {
                    id: "29|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "秀峰区",
                        attr: {
                            id: "29|03|01"
                        }
                    },
                    {
                        name: "叠彩区",
                        attr: {
                            id: "29|03|02"
                        }
                    },
                    {
                        name: "象山区",
                        attr: {
                            id: "29|03|03"
                        }
                    },
                    {
                        name: "七星区",
                        attr: {
                            id: "29|03|04"
                        }
                    },
                    {
                        name: "雁山区",
                        attr: {
                            id: "29|03|05"
                        }
                    },
                    {
                        name: "阳朔县",
                        attr: {
                            id: "29|03|06"
                        }
                    },
                    {
                        name: "临桂县",
                        attr: {
                            id: "29|03|07"
                        }
                    },
                    {
                        name: "灵川县",
                        attr: {
                            id: "29|03|08"
                        }
                    },
                    {
                        name: "全州县",
                        attr: {
                            id: "29|03|09"
                        }
                    },
                    {
                        name: "兴安县",
                        attr: {
                            id: "29|03|10"
                        }
                    },
                    {
                        name: "永福县",
                        attr: {
                            id: "29|03|11"
                        }
                    },
                    {
                        name: "灌阳县",
                        attr: {
                            id: "29|03|12"
                        }
                    },
                    {
                        name: "龙胜各族自治县",
                        attr: {
                            id: "29|03|13"
                        }
                    },
                    {
                        name: "资源县",
                        attr: {
                            id: "29|03|14"
                        }
                    },
                    {
                        name: "平乐县",
                        attr: {
                            id: "29|03|15"
                        }
                    },
                    {
                        name: "荔蒲县",
                        attr: {
                            id: "29|03|16"
                        }
                    },
                    {
                        name: "恭城瑶族自治县",
                        attr: {
                            id: "29|03|17"
                        }
                    }
                ]
            },
            {
                name: "梧州",
                attr: {
                    id: "29|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "万秀区",
                        attr: {
                            id: "29|04|01"
                        }
                    },
                    {
                        name: "蝶山区",
                        attr: {
                            id: "29|04|02"
                        }
                    },
                    {
                        name: "长洲区",
                        attr: {
                            id: "29|04|03"
                        }
                    },
                    {
                        name: "苍梧县",
                        attr: {
                            id: "29|04|04"
                        }
                    },
                    {
                        name: "藤县",
                        attr: {
                            id: "29|04|05"
                        }
                    },
                    {
                        name: "蒙山县",
                        attr: {
                            id: "29|04|06"
                        }
                    },
                    {
                        name: "岑溪市",
                        attr: {
                            id: "29|04|07"
                        }
                    }
                ]
            },
            {
                name: "北海",
                attr: {
                    id: "29|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "海城区",
                        attr: {
                            id: "29|05|01"
                        }
                    },
                    {
                        name: "银海区",
                        attr: {
                            id: "29|05|02"
                        }
                    },
                    {
                        name: "铁山港区",
                        attr: {
                            id: "29|05|03"
                        }
                    },
                    {
                        name: "合浦县",
                        attr: {
                            id: "29|05|04"
                        }
                    }
                ]
            },
            {
                name: "防城港",
                attr: {
                    id: "29|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "港口区",
                        attr: {
                            id: "29|06|01"
                        }
                    },
                    {
                        name: "防城区",
                        attr: {
                            id: "29|06|02"
                        }
                    },
                    {
                        name: "上思县",
                        attr: {
                            id: "29|06|03"
                        }
                    },
                    {
                        name: "东兴市",
                        attr: {
                            id: "29|06|04"
                        }
                    }
                ]
            },
            {
                name: "钦州",
                attr: {
                    id: "29|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "钦南区",
                        attr: {
                            id: "29|07|01"
                        }
                    },
                    {
                        name: "钦北区",
                        attr: {
                            id: "29|07|02"
                        }
                    },
                    {
                        name: "灵山县",
                        attr: {
                            id: "29|03|03"
                        }
                    },
                    {
                        name: "浦北县",
                        attr: {
                            id: "29|07|04"
                        }
                    }
                ]
            },
            {
                name: "贵港",
                attr: {
                    id: "29|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "港北区",
                        attr: {
                            id: "29|08|01"
                        }
                    },
                    {
                        name: "港南区",
                        attr: {
                            id: "29|08|02"
                        }
                    },
                    {
                        name: "覃塘区",
                        attr: {
                            id: "29|08|03"
                        }
                    },
                    {
                        name: "平南县",
                        attr: {
                            id: "29|08|04"
                        }
                    },
                    {
                        name: "桂平市",
                        attr: {
                            id: "29|08|05"
                        }
                    }
                ]
            },
            {
                name: "玉林",
                attr: {
                    id: "29|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "玉州区",
                        attr: {
                            id: "29|09|01"
                        }
                    },
                    {
                        name: "容县",
                        attr: {
                            id: "29|09|02"
                        }
                    },
                    {
                        name: "陆川县",
                        attr: {
                            id: "29|09|03"
                        }
                    },
                    {
                        name: "博白县",
                        attr: {
                            id: "29|09|04"
                        }
                    },
                    {
                        name: "兴业县",
                        attr: {
                            id: "29|09|05"
                        }
                    },
                    {
                        name: "北流市",
                        attr: {
                            id: "29|09|06"
                        }
                    }
                ]
            },
            {
                name: "百色",
                attr: {
                    id: "29|10|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "右江区",
                        attr: {
                            id: "29|10|01"
                        }
                    },
                    {
                        name: "田阳县",
                        attr: {
                            id: "29|10|02"
                        }
                    },
                    {
                        name: "田东县",
                        attr: {
                            id: "29|10|03"
                        }
                    },
                    {
                        name: "平果县",
                        attr: {
                            id: "29|10|04"
                        }
                    },
                    {
                        name: "德保县",
                        attr: {
                            id: "29|10|05"
                        }
                    },
                    {
                        name: "靖西县",
                        attr: {
                            id: "29|10|06"
                        }
                    },
                    {
                        name: "那坡县",
                        attr: {
                            id: "29|10|07"
                        }
                    },
                    {
                        name: "凌云县",
                        attr: {
                            id: "29|10|08"
                        }
                    },
                    {
                        name: "乐业县",
                        attr: {
                            id: "29|10|09"
                        }
                    },
                    {
                        name: "田林县",
                        attr: {
                            id: "29|10|10"
                        }
                    },
                    {
                        name: "西林县",
                        attr: {
                            id: "29|10|11"
                        }
                    },
                    {
                        name: "隆林各族自治县",
                        attr: {
                            id: "29|10|12"
                        }
                    }
                ]
            },
            {
                name: "贺州",
                attr: {
                    id: "29|11|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "八步区",
                        attr: {
                            id: "29|11|01"
                        }
                    },
                    {
                        name: "昭平县",
                        attr: {
                            id: "29|11|02"
                        }
                    },
                    {
                        name: "钟山县",
                        attr: {
                            id: "29|11|03"
                        }
                    },
                    {
                        name: "富川瑶族自治县",
                        attr: {
                            id: "29|11|04"
                        }
                    }
                ]
            },
            {
                name: "河池",
                attr: {
                    id: "29|12|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "金城江区",
                        attr: {
                            id: "29|12|01"
                        }
                    },
                    {
                        name: "南丹县",
                        attr: {
                            id: "29|12|02"
                        }
                    },
                    {
                        name: "天峨县",
                        attr: {
                            id: "29|12|03"
                        }
                    },
                    {
                        name: "凤山县",
                        attr: {
                            id: "29|12|04"
                        }
                    },
                    {
                        name: "东兰县",
                        attr: {
                            id: "29|12|05"
                        }
                    },
                    {
                        name: "罗城仫佬族自治县",
                        attr: {
                            id: "29|12|06"
                        }
                    },
                    {
                        name: "环江毛南族自治县",
                        attr: {
                            id: "29|12|07"
                        }
                    },
                    {
                        name: "巴马瑶族自治县",
                        attr: {
                            id: "29|12|08"
                        }
                    },
                    {
                        name: "都安瑶族自治县",
                        attr: {
                            id: "29|12|09"
                        }
                    },
                    {
                        name: "大化瑶族自治县",
                        attr: {
                            id: "29|12|10"
                        }
                    },
                    {
                        name: "宜州市",
                        attr: {
                            id: "29|12|11"
                        }
                    }
                ]
            },
            {
                name: "来宾",
                attr: {
                    id: "29|13|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "兴宾区",
                        attr: {
                            id: "29|13|01"
                        }
                    },
                    {
                        name: "忻城县",
                        attr: {
                            id: "29|13|02"
                        }
                    },
                    {
                        name: "象州县",
                        attr: {
                            id: "29|13|03"
                        }
                    },
                    {
                        name: "武宣县",
                        attr: {
                            id: "29|13|04"
                        }
                    },
                    {
                        name: "金秀瑶族自治县",
                        attr: {
                            id: "29|13|05"
                        }
                    },
                    {
                        name: "合山市",
                        attr: {
                            id: "29|13|06"
                        }
                    }
                ]
            },
            {
                name: "崇左",
                attr: {
                    id: "29|14|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "江洲区",
                        attr: {
                            id: "29|14|01"
                        }
                    },
                    {
                        name: "扶绥县",
                        attr: {
                            id: "29|14|02"
                        }
                    },
                    {
                        name: "宁明县",
                        attr: {
                            id: "29|14|03"
                        }
                    },
                    {
                        name: "龙州县",
                        attr: {
                            id: "29|14|04"
                        }
                    },
                    {
                        name: "大新县",
                        attr: {
                            id: "29|14|05"
                        }
                    },
                    {
                        name: "天等县",
                        attr: {
                            id: "29|14|06"
                        }
                    },
                    {
                        name: "凭祥市",
                        attr: {
                            id: "29|14|07"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "海南",
        attr: {
            id: "30|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "海口",
                attr: {
                    id: "30|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "秀英区",
                        attr: {
                            id: "30|01|01"
                        }
                    },
                    {
                        name: "龙华区",
                        attr: {
                            id: "30|01|02"
                        }
                    },
                    {
                        name: "琼山区",
                        attr: {
                            id: "30|01|03"
                        }
                    },
                    {
                        name: "美兰区",
                        attr: {
                            id: "30|01|04"
                        }
                    }
                ]
            },
            {
                name: "三亚",
                attr: {
                    id: "30|02|00"
                },
                nextLevel: [
                    {
                        name: "河西区",
                        attr: {
                            id: "30|02|01"
                        }
                    },
                    {
                        name: "河东区",
                        attr: {
                            id: "30|02|02"
                        }
                    },
                    {
                        name: "海棠湾镇",
                        attr: {
                            id: "30|02|03"
                        }
                    },
                    {
                        name: "吉阳镇",
                        attr: {
                            id: "30|02|04"
                        }
                    },
                    {
                        name: "凤凰镇",
                        attr: {
                            id: "30|02|05"
                        }
                    },
                    {
                        name: "崖城镇",
                        attr: {
                            id: "30|02|06"
                        }
                    },
                    {
                        name: "天涯镇",
                        attr: {
                            id: "30|02|07"
                        }
                    },
                    {
                        name: "育才镇",
                        attr: {
                            id: "30|02|08"
                        }
                    }
                ]
            },
            {
                name: "五指山",
                attr: {
                    id: "30|03|00"
                }
            },
            {
                name: "琼海",
                attr: {
                    id: "30|04|00"
                }
            },
            {
                name: "儋州",
                attr: {
                    id: "30|05|00"
                }
            },
            {
                name: "文昌",
                attr: {
                    id: "30|06|00"
                }
            },
            {
                name: "万宁",
                attr: {
                    id: "30|07|00"
                }
            },
            {
                name: "东方",
                attr: {
                    id: "30|08|00"
                }
            },
            {
                name: "定安县",
                attr: {
                    id: "30|09|00"
                }
            },
            {
                name: "屯昌县",
                attr: {
                    id: "30|10|00"
                }
            },
            {
                name: "澄迈县",
                attr: {
                    id: "30|11|00"
                }
            },
            {
                name: "临高县",
                attr: {
                    id: "30|12|00"
                }
            },
            {
                name: "白沙县",
                attr: {
                    id: "30|13|00"
                }
            },
            {
                name: "昌江县",
                attr: {
                    id: "30|14|00"
                }
            },
            {
                name: "乐东县",
                attr: {
                    id: "30|15|00"
                }
            },
            {
                name: "陵水县",
                attr: {
                    id: "30|16|00"
                }
            },
            {
                name: "保亭县",
                attr: {
                    id: "30|17|00"
                }
            },
            {
                name: "琼中县",
                attr: {
                    id: "30|18|00"
                }
            },
            {
                name: "三沙市",
                attr: {
                    id: "30|19|00"
                },
                nextLevel: [
                    {
                        name: "西沙群岛",
                        attr: {
                            id: "30|19|01"
                        }
                    },
                    {
                        name: "南沙群岛",
                        attr: {
                            id: "30|19|02"
                        }
                    },
                    {
                        name: "中沙群岛",
                        attr: {
                            id: "30|19|03"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "重庆",
        attr: {
            id: "31|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "万州区",
                attr: {
                    id: "31|00|01"
                }
            },
            {
                name: "涪陵区",
                attr: {
                    id: "31|00|02"
                }
            },
            {
                name: "渝中区",
                attr: {
                    id: "31|00|03"
                }
            },
            {
                name: "大渡口区",
                attr: {
                    id: "31|00|04"
                }
            },
            {
                name: "江北区",
                attr: {
                    id: "31|00|05"
                }
            },
            {
                name: "沙坪坝区",
                attr: {
                    id: "31|00|06"
                }
            },
            {
                name: "九龙坡区",
                attr: {
                    id: "31|00|07"
                }
            },
            {
                name: "南岸区",
                attr: {
                    id: "31|00|08"
                }
            },
            {
                name: "北碚区",
                attr: {
                    id: "31|00|09"
                }
            },
            {
                name: "渝北区",
                attr: {
                    id: "31|00|12"
                }
            },
            {
                name: "巴南区",
                attr: {
                    id: "31|00|13"
                }
            },
            {
                name: "长寿区",
                attr: {
                    id: "31|00|14"
                }
            },
            {
                name: "綦江县",
                attr: {
                    id: "31|00|15"
                }
            },
            {
                name: "潼南县",
                attr: {
                    id: "31|00|16"
                }
            },
            {
                name: "铜梁县",
                attr: {
                    id: "31|00|17"
                }
            },
            {
                name: "大足区",
                attr: {
                    id: "31|00|18"
                }
            },
            {
                name: "荣昌县",
                attr: {
                    id: "31|00|19"
                }
            },
            {
                name: "壁山县",
                attr: {
                    id: "31|00|20"
                }
            },
            {
                name: "梁平县",
                attr: {
                    id: "31|00|21"
                }
            },
            {
                name: "城口县",
                attr: {
                    id: "31|00|22"
                }
            },
            {
                name: "丰都县",
                attr: {
                    id: "31|00|23"
                }
            },
            {
                name: "垫江县",
                attr: {
                    id: "31|00|24"
                }
            },
            {
                name: "武隆县",
                attr: {
                    id: "31|00|25"
                }
            },
            {
                name: "忠县",
                attr: {
                    id: "31|00|26"
                }
            },
            {
                name: "开县",
                attr: {
                    id: "31|00|27"
                }
            },
            {
                name: "云阳县",
                attr: {
                    id: "31|00|28"
                }
            },
            {
                name: "奉节县",
                attr: {
                    id: "31|00|29"
                }
            },
            {
                name: "巫山县",
                attr: {
                    id: "31|00|30"
                }
            },
            {
                name: "巫溪县",
                attr: {
                    id: "31|00|31"
                }
            },
            {
                name: "黔江区",
                attr: {
                    id: "31|00|40"
                }
            },
            {
                name: "石柱土家族自治县",
                attr: {
                    id: "31|00|33"
                }
            },
            {
                name: "秀山土家族苗族自治县",
                attr: {
                    id: "31|00|34"
                }
            },
            {
                name: "酉阳土家族苗族自治县",
                attr: {
                    id: "31|00|35"
                }
            },
            {
                name: "彭水苗族土家族自治县",
                attr: {
                    id: "31|00|36"
                }
            },
            {
                name: "江津区",
                attr: {
                    id: "31|00|37"
                }
            },
            {
                name: "合川区",
                attr: {
                    id: "31|00|38"
                }
            },
            {
                name: "永川区",
                attr: {
                    id: "31|00|39"
                }
            },
            {
                name: "南川区",
                attr: {
                    id: "31|00|40"
                }
            }
        ]
    },
    {
        name: "四川",
        attr: {
            id: "40|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "成都",
                attr: {
                    id: "40|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "锦江区",
                        attr: {
                            id: "40|01|01"
                        }
                    },
                    {
                        name: "青羊区",
                        attr: {
                            id: "40|01|02"
                        }
                    },
                    {
                        name: "金牛区",
                        attr: {
                            id: "40|01|03"
                        }
                    },
                    {
                        name: "武侯区",
                        attr: {
                            id: "40|01|04"
                        }
                    },
                    {
                        name: "成华区",
                        attr: {
                            id: "40|01|05"
                        }
                    },
                    {
                        name: "龙泉驿区",
                        attr: {
                            id: "40|01|06"
                        }
                    },
                    {
                        name: "青白江区",
                        attr: {
                            id: "40|01|07"
                        }
                    },
                    {
                        name: "新都区",
                        attr: {
                            id: "40|01|08"
                        }
                    },
                    {
                        name: "温江区",
                        attr: {
                            id: "40|01|09"
                        }
                    },
                    {
                        name: "金堂县",
                        attr: {
                            id: "40|01|10"
                        }
                    },
                    {
                        name: "双流县",
                        attr: {
                            id: "40|01|11"
                        }
                    },
                    {
                        name: "郫县",
                        attr: {
                            id: "40|01|12"
                        }
                    },
                    {
                        name: "大邑县",
                        attr: {
                            id: "40|01|13"
                        }
                    },
                    {
                        name: "蒲江县",
                        attr: {
                            id: "40|01|14"
                        }
                    },
                    {
                        name: "新津县",
                        attr: {
                            id: "40|01|15"
                        }
                    },
                    {
                        name: "都江堰市",
                        attr: {
                            id: "40|01|16"
                        }
                    },
                    {
                        name: "彭州市",
                        attr: {
                            id: "40|01|17"
                        }
                    },
                    {
                        name: "邛崃市",
                        attr: {
                            id: "40|01|18"
                        }
                    },
                    {
                        name: "崇州市",
                        attr: {
                            id: "40|01|19"
                        }
                    }
                ]
            },
            {
                name: "自贡",
                attr: {
                    id: "40|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "自流井区",
                        attr: {
                            id: "40|02|01"
                        }
                    },
                    {
                        name: "贡井区",
                        attr: {
                            id: "40|02|02"
                        }
                    },
                    {
                        name: "大安区",
                        attr: {
                            id: "40|02|03"
                        }
                    },
                    {
                        name: "沿滩区",
                        attr: {
                            id: "40|02|04"
                        }
                    },
                    {
                        name: "荣县",
                        attr: {
                            id: "40|02|05"
                        }
                    },
                    {
                        name: "富顺县",
                        attr: {
                            id: "40|02|06"
                        }
                    }
                ]
            },
            {
                name: "攀枝花",
                attr: {
                    id: "40|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "东区",
                        attr: {
                            id: "40|03|01"
                        }
                    },
                    {
                        name: "西区",
                        attr: {
                            id: "40|03|02"
                        }
                    },
                    {
                        name: "仁和区",
                        attr: {
                            id: "40|03|03"
                        }
                    },
                    {
                        name: "米易县",
                        attr: {
                            id: "40|03|04"
                        }
                    },
                    {
                        name: "盐边县",
                        attr: {
                            id: "40|03|05"
                        }
                    }
                ]
            },
            {
                name: "泸州",
                attr: {
                    id: "40|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "江阳区",
                        attr: {
                            id: "40|04|01"
                        }
                    },
                    {
                        name: "纳溪区",
                        attr: {
                            id: "40|04|02"
                        }
                    },
                    {
                        name: "龙马潭区",
                        attr: {
                            id: "40|04|03"
                        }
                    },
                    {
                        name: "泸县",
                        attr: {
                            id: "40|04|04"
                        }
                    },
                    {
                        name: "合江县",
                        attr: {
                            id: "40|04|05"
                        }
                    },
                    {
                        name: "叙永县",
                        attr: {
                            id: "40|04|06"
                        }
                    },
                    {
                        name: "古蔺县·",
                        attr: {
                            id: "40|04|07"
                        }
                    }
                ]
            },
            {
                name: "德阳",
                attr: {
                    id: "40|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "旌阳区",
                        attr: {
                            id: "40|05|01"
                        }
                    },
                    {
                        name: "中江县",
                        attr: {
                            id: "40|05|02"
                        }
                    },
                    {
                        name: "罗江县",
                        attr: {
                            id: "40|05|03"
                        }
                    },
                    {
                        name: "广汉市",
                        attr: {
                            id: "40|05|04"
                        }
                    },
                    {
                        name: "什邡市",
                        attr: {
                            id: "40|05|05"
                        }
                    },
                    {
                        name: "绵竹市",
                        attr: {
                            id: "40|05|06"
                        }
                    }
                ]
            },
            {
                name: "绵阳",
                attr: {
                    id: "40|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "涪城区",
                        attr: {
                            id: "40|06|01"
                        }
                    },
                    {
                        name: "游仙区",
                        attr: {
                            id: "40|06|02"
                        }
                    },
                    {
                        name: "三台县",
                        attr: {
                            id: "40|06|03"
                        }
                    },
                    {
                        name: "盐亭县",
                        attr: {
                            id: "40|06|04"
                        }
                    },
                    {
                        name: "安县",
                        attr: {
                            id: "40|06|05"
                        }
                    },
                    {
                        name: "梓潼县",
                        attr: {
                            id: "40|06|06"
                        }
                    },
                    {
                        name: "北川羌族自治县",
                        attr: {
                            id: "40|06|07"
                        }
                    },
                    {
                        name: "平武县",
                        attr: {
                            id: "40|06|08"
                        }
                    },
                    {
                        name: "江油市",
                        attr: {
                            id: "40|06|09"
                        }
                    }
                ]
            },
            {
                name: "广元",
                attr: {
                    id: "40|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "利州区",
                        attr: {
                            id: "40|07|01"
                        }
                    },
                    {
                        name: "昭化区",
                        attr: {
                            id: "40|07|02"
                        }
                    },
                    {
                        name: "朝天区",
                        attr: {
                            id: "40|07|03"
                        }
                    },
                    {
                        name: "旺苍县",
                        attr: {
                            id: "40|07|04"
                        }
                    },
                    {
                        name: "青川县",
                        attr: {
                            id: "40|07|05"
                        }
                    },
                    {
                        name: "剑阁县",
                        attr: {
                            id: "40|07|06"
                        }
                    },
                    {
                        name: "苍溪县",
                        attr: {
                            id: "40|07|07"
                        }
                    }
                ]
            },
            {
                name: "遂宁",
                attr: {
                    id: "40|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "船山区",
                        attr: {
                            id: "40|08|01"
                        }
                    },
                    {
                        name: "安居区",
                        attr: {
                            id: "40|08|02"
                        }
                    },
                    {
                        name: "蓬溪县",
                        attr: {
                            id: "40|08|03"
                        }
                    },
                    {
                        name: "射洪县",
                        attr: {
                            id: "40|08|04"
                        }
                    },
                    {
                        name: "大英县",
                        attr: {
                            id: "40|08|05"
                        }
                    }
                ]
            },
            {
                name: "内江",
                attr: {
                    id: "40|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "市中区",
                        attr: {
                            id: "40|09|01"
                        }
                    },
                    {
                        name: "东兴区",
                        attr: {
                            id: "40|09|02"
                        }
                    },
                    {
                        name: "威远县",
                        attr: {
                            id: "40|09|03"
                        }
                    },
                    {
                        name: "资中县",
                        attr: {
                            id: "40|09|04"
                        }
                    },
                    {
                        name: "隆昌县",
                        attr: {
                            id: "40|09|05"
                        }
                    }
                ]
            },
            {
                name: "乐山",
                attr: {
                    id: "40|10|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "市中区",
                        attr: {
                            id: "40|10|01"
                        }
                    },
                    {
                        name: "沙湾区",
                        attr: {
                            id: "40|10|02"
                        }
                    },
                    {
                        name: "五通桥区",
                        attr: {
                            id: "40|10|03"
                        }
                    },
                    {
                        name: "金口河区",
                        attr: {
                            id: "40|10|04"
                        }
                    },
                    {
                        name: "犍为县",
                        attr: {
                            id: "40|10|05"
                        }
                    },
                    {
                        name: "井研县",
                        attr: {
                            id: "40|10|06"
                        }
                    },
                    {
                        name: "夹江县",
                        attr: {
                            id: "40|10|07"
                        }
                    },
                    {
                        name: "沐川县",
                        attr: {
                            id: "40|10|08"
                        }
                    },
                    {
                        name: "峨边彝族自治县",
                        attr: {
                            id: "40|10|09"
                        }
                    },
                    {
                        name: "马边彝族自治县",
                        attr: {
                            id: "40|10|10"
                        }
                    },
                    {
                        name: "峨眉山市",
                        attr: {
                            id: "40|10|11"
                        }
                    }
                ]
            },
            {
                name: "南充",
                attr: {
                    id: "40|11|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "顺庆区",
                        attr: {
                            id: "40|11|01"
                        }
                    },
                    {
                        name: "高坪区",
                        attr: {
                            id: "40|11|02"
                        }
                    },
                    {
                        name: "嘉陵区",
                        attr: {
                            id: "40|11|03"
                        }
                    },
                    {
                        name: "南部县",
                        attr: {
                            id: "40|11|04"
                        }
                    },
                    {
                        name: "营山县",
                        attr: {
                            id: "40|11|05"
                        }
                    },
                    {
                        name: "蓬安县",
                        attr: {
                            id: "40|11|06"
                        }
                    },
                    {
                        name: "仪陇县",
                        attr: {
                            id: "40|11|07"
                        }
                    },
                    {
                        name: "西充县",
                        attr: {
                            id: "40|11|08"
                        }
                    },
                    {
                        name: "阆中市",
                        attr: {
                            id: "40|11|09"
                        }
                    }
                ]
            },
            {
                name: "眉山",
                attr: {
                    id: "40|12|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "东坡区",
                        attr: {
                            id: "40|12|01"
                        }
                    },
                    {
                        name: "仁寿县",
                        attr: {
                            id: "40|12|02"
                        }
                    },
                    {
                        name: "彭山县",
                        attr: {
                            id: "40|12|03"
                        }
                    },
                    {
                        name: "洪雅县",
                        attr: {
                            id: "40|12|04"
                        }
                    },
                    {
                        name: "丹棱县",
                        attr: {
                            id: "40|12|05"
                        }
                    },
                    {
                        name: "青神县",
                        attr: {
                            id: "40|12|06"
                        }
                    }
                ]
            },
            {
                name: "宜宾",
                attr: {
                    id: "40|13|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "翠屏区",
                        attr: {
                            id: "40|13|01"
                        }
                    },
                    {
                        name: "宜宾县",
                        attr: {
                            id: "40|13|02"
                        }
                    },
                    {
                        name: "南溪区",
                        attr: {
                            id: "40|13|03"
                        }
                    },
                    {
                        name: "江安县",
                        attr: {
                            id: "40|13|04"
                        }
                    },
                    {
                        name: "长宁县",
                        attr: {
                            id: "40|13|05"
                        }
                    },
                    {
                        name: "高县",
                        attr: {
                            id: "40|13|06"
                        }
                    },
                    {
                        name: "珙县",
                        attr: {
                            id: "40|13|07"
                        }
                    },
                    {
                        name: "筠连县",
                        attr: {
                            id: "40|13|08"
                        }
                    },
                    {
                        name: "兴文县",
                        attr: {
                            id: "40|13|09"
                        }
                    },
                    {
                        name: "屏山县",
                        attr: {
                            id: "40|13|10"
                        }
                    }
                ]
            },
            {
                name: "广安",
                attr: {
                    id: "40|14|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "广安区",
                        attr: {
                            id: "40|14|01"
                        }
                    },
                    {
                        name: "岳池县",
                        attr: {
                            id: "40|14|02"
                        }
                    },
                    {
                        name: "武胜县",
                        attr: {
                            id: "40|14|03"
                        }
                    },
                    {
                        name: "邻水县",
                        attr: {
                            id: "40|14|04"
                        }
                    },
                    {
                        name: "华蓥市",
                        attr: {
                            id: "40|14|05"
                        }
                    },
                    {
                            name: "前锋区",
                        attr: {
                            id: "40|14|06"
                        }
                    }
                ]
            },
            {
                name: "达川",
                attr: {
                    id: "40|15|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "通川区",
                        attr: {
                            id: "40|15|01"
                        }
                    },
                    {
                        name: "达州区",
                        attr: {
                            id: "40|15|02"
                        }
                    },
                    {
                        name: "宣汉县",
                        attr: {
                            id: "40|15|03"
                        }
                    },
                    {
                        name: "开江县",
                        attr: {
                            id: "40|15|04"
                        }
                    },
                    {
                        name: "大竹县",
                        attr: {
                            id: "40|15|05"
                        }
                    },
                    {
                        name: "渠县",
                        attr: {
                            id: "40|15|06"
                        }
                    },
                    {
                        name: "万源市",
                        attr: {
                            id: "40|15|07"
                        }
                    }
                ]
            },
            {
                name: "雅安",
                attr: {
                    id: "40|16|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "雨城区",
                        attr: {
                            id: "40|16|01"
                        }
                    },
                    {
                        name: "名山县",
                        attr: {
                            id: "40|16|02"
                        }
                    },
                    {
                        name: "荥经县",
                        attr: {
                            id: "40|16|03"
                        }
                    },
                    {
                        name: "汉源县",
                        attr: {
                            id: "40|16|04"
                        }
                    },
                    {
                        name: "石棉县",
                        attr: {
                            id: "40|16|05"
                        }
                    },
                    {
                        name: "天全县",
                        attr: {
                            id: "40|16|06"
                        }
                    },
                    {
                        name: "芦山县",
                        attr: {
                            id: "40|16|07"
                        }
                    },
                    {
                        name: "宝兴县",
                        attr: {
                            id: "40|16|08"
                        }
                    }
                ]
            },
            {
                name: "巴中",
                attr: {
                    id: "40|17|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "巴州区",
                        attr: {
                            id: "40|17|01"
                        }
                    },
                    {
                        name: "通江县",
                        attr: {
                            id: "40|17|02"
                        }
                    },
                    {
                        name: "南江县",
                        attr: {
                            id: "40|17|03"
                        }
                    },
                    {
                        name: "平昌县",
                        attr: {
                            id: "40|17|04"
                        }
                    },
                    {
                        name: "恩阳区",
                        attr: {
                            id: "40|17|05"
                        }
                    }
                ]
            },
            {
                name: "资阳",
                attr: {
                    id: "40|18|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "雁江区",
                        attr: {
                            id: "40|18|01"
                        }
                    },
                    {
                        name: "安岳县",
                        attr: {
                            id: "40|18|02"
                        }
                    },
                    {
                        name: "乐至县",
                        attr: {
                            id: "40|18|03"
                        }
                    },
                    {
                        name: "简阳市",
                        attr: {
                            id: "40|18|04"
                        }
                    }
                ]
            },
            {
                name: "阿坝",
                attr: {
                    id: "40|19|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "汶川县",
                        attr: {
                            id: "40|19|01"
                        }
                    },
                    {
                        name: "理县",
                        attr: {
                            id: "40|19|02"
                        }
                    },
                    {
                        name: "茂县",
                        attr: {
                            id: "40|19|03"
                        }
                    },
                    {
                        name: "松潘县",
                        attr: {
                            id: "40|19|04"
                        }
                    },
                    {
                        name: "九寨沟县",
                        attr: {
                            id: "40|19|05"
                        }
                    },
                    {
                        name: "金川县",
                        attr: {
                            id: "40|19|06"
                        }
                    },
                    {
                        name: "小金县",
                        attr: {
                            id: "40|19|07"
                        }
                    },
                    {
                        name: "黑水县",
                        attr: {
                            id: "40|19|08"
                        }
                    },
                    {
                        name: "马尔康县",
                        attr: {
                            id: "40|19|09"
                        }
                    },
                    {
                        name: "壤塘县",
                        attr: {
                            id: "40|19|10"
                        }
                    },
                    {
                        name: "阿坝县",
                        attr: {
                            id: "40|19|11"
                        }
                    },
                    {
                        name: "若尔盖县",
                        attr: {
                            id: "40|19|12"
                        }
                    },
                    {
                        name: "红原县",
                        attr: {
                            id: "40|19|13"
                        }
                    }
                ]
            },
            {
                name: "甘孜",
                attr: {
                    id: "40|20|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "康定县",
                        attr: {
                            id: "40|20|01"
                        }
                    },
                    {
                        name: "泸定县",
                        attr: {
                            id: "40|20|02"
                        }
                    },
                    {
                        name: "丹巴县",
                        attr: {
                            id: "40|20|03"
                        }
                    },
                    {
                        name: "九龙县",
                        attr: {
                            id: "40|20|04"
                        }
                    },
                    {
                        name: "雅江县",
                        attr: {
                            id: "40|20|05"
                        }
                    },
                    {
                        name: "道孚县",
                        attr: {
                            id: "40|20|06"
                        }
                    },
                    {
                        name: "炉霍县",
                        attr: {
                            id: "40|20|07"
                        }
                    },
                    {
                        name: "甘孜县",
                        attr: {
                            id: "40|20|08"
                        }
                    },
                    {
                        name: "新龙县",
                        attr: {
                            id: "40|20|09"
                        }
                    },
                    {
                        name: "德格县",
                        attr: {
                            id: "40|20|10"
                        }
                    },
                    {
                        name: "白玉县",
                        attr: {
                            id: "40|20|11"
                        }
                    },
                    {
                        name: "石渠县",
                        attr: {
                            id: "40|20|12"
                        }
                    },
                    {
                        name: "色达县",
                        attr: {
                            id: "40|20|13"
                        }
                    },
                    {
                        name: "理塘县",
                        attr: {
                            id: "40|20|14"
                        }
                    },
                    {
                        name: "巴塘县",
                        attr: {
                            id: "40|20|15"
                        }
                    },
                    {
                        name: "乡城县",
                        attr: {
                            id: "40|20|16"
                        }
                    },
                    {
                        name: "稻城县",
                        attr: {
                            id: "40|20|17"
                        }
                    },
                    {
                        name: "得荣县",
                        attr: {
                            id: "40|20|18"
                        }
                    }
                ]
            },
            {
                name: "凉山",
                attr: {
                    id: "40|21|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "西昌市",
                        attr: {
                            id: "40|21|01"
                        }
                    },
                    {
                        name: "木里藏族自治县",
                        attr: {
                            id: "40|21|02"
                        }
                    },
                    {
                        name: "盐源县",
                        attr: {
                            id: "40|21|03"
                        }
                    },
                    {
                        name: "德昌县",
                        attr: {
                            id: "40|21|04"
                        }
                    },
                    {
                        name: "会理县",
                        attr: {
                            id: "40|21|05"
                        }
                    },
                    {
                        name: "会东县",
                        attr: {
                            id: "40|21|06"
                        }
                    },
                    {
                        name: "宁南县",
                        attr: {
                            id: "40|21|07"
                        }
                    },
                    {
                        name: "普格县",
                        attr: {
                            id: "40|21|08"
                        }
                    },
                    {
                        name: "布拖县",
                        attr: {
                            id: "40|21|09"
                        }
                    },
                    {
                        name: "金阳县",
                        attr: {
                            id: "40|21|10"
                        }
                    },
                    {
                        name: "昭觉县",
                        attr: {
                            id: "40|21|11"
                        }
                    },
                    {
                        name: "喜德县",
                        attr: {
                            id: "40|21|12"
                        }
                    },
                    {
                        name: "冕宁县",
                        attr: {
                            id: "40|21|13"
                        }
                    },
                    {
                        name: "越西县",
                        attr: {
                            id: "40|21|14"
                        }
                    },
                    {
                        name: "甘洛县",
                        attr: {
                            id: "40|21|15"
                        }
                    },
                    {
                        name: "美姑县",
                        attr: {
                            id: "40|21|16"
                        }
                    },
                    {
                        name: "雷波县",
                        attr: {
                            id: "40|21|17"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "贵州",
        attr: {
            id: "33|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "贵阳",
                attr: {
                    id: "33|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "南明区",
                        attr: {
                            id: "33|01|01"
                        }
                    },
                    {
                        name: "云岩区",
                        attr: {
                            id: "33|01|02"
                        }
                    },
                    {
                        name: "花溪区",
                        attr: {
                            id: "33|01|03"
                        }
                    },
                    {
                        name: "乌当区",
                        attr: {
                            id: "33|01|04"
                        }
                    },
                    {
                        name: "白云区",
                        attr: {
                            id: "33|01|05"
                        }
                    },
                    {
                        name: "小河区",
                        attr: {
                            id: "33|01|06"
                        }
                    },
                    {
                        name: "开阳县",
                        attr: {
                            id: "33|01|07"
                        }
                    },
                    {
                        name: "息烽县",
                        attr: {
                            id: "33|01|08"
                        }
                    },
                    {
                        name: "修文县",
                        attr: {
                            id: "33|01|09"
                        }
                    },
                    {
                        name: "清镇市",
                        attr: {
                            id: "33|01|10"
                        }
                    }
                ]
            },
            {
                name: "六盘水",
                attr: {
                    id: "33|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "钟山区",
                        attr: {
                            id: "33|02|01"
                        }
                    },
                    {
                        name: "六枝特区",
                        attr: {
                            id: "33|02|02"
                        }
                    },
                    {
                        name: "水城县",
                        attr: {
                            id: "33|02|03"
                        }
                    },
                    {
                        name: "盘县",
                        attr: {
                            id: "33|02|04"
                        }
                    }
                ]
            },
            {
                name: "遵义",
                attr: {
                    id: "33|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "红花岗区",
                        attr: {
                            id: "33|03|01"
                        }
                    },
                    {
                        name: "汇川区",
                        attr: {
                            id: "33|03|02"
                        }
                    },
                    {
                        name: "遵义县",
                        attr: {
                            id: "33|03|03"
                        }
                    },
                    {
                        name: "桐梓县",
                        attr: {
                            id: "33|03|04"
                        }
                    },
                    {
                        name: "绥阳县",
                        attr: {
                            id: "33|03|05"
                        }
                    },
                    {
                        name: "正安县",
                        attr: {
                            id: "33|03|06"
                        }
                    },
                    {
                        name: "道真仡佬族苗族自治县",
                        attr: {
                            id: "33|03|07"
                        }
                    },
                    {
                        name: "务川仡佬族苗族自治县",
                        attr: {
                            id: "33|03|08"
                        }
                    },
                    {
                        name: "凤冈县",
                        attr: {
                            id: "33|03|09"
                        }
                    },
                    {
                        name: "湄潭县",
                        attr: {
                            id: "33|03|10"
                        }
                    },
                    {
                        name: "余庆县",
                        attr: {
                            id: "33|03|11"
                        }
                    },
                    {
                        name: "习水县",
                        attr: {
                            id: "33|03|12"
                        }
                    },
                    {
                        name: "赤水市",
                        attr: {
                            id: "33|03|13"
                        }
                    },
                    {
                        name: "仁怀市",
                        attr: {
                            id: "33|03|14"
                        }
                    }
                ]
            },
            {
                name: "安顺",
                attr: {
                    id: "33|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "西秀区",
                        attr: {
                            id: "33|04|01"
                        }
                    },
                    {
                        name: "平坝县",
                        attr: {
                            id: "33|04|02"
                        }
                    },
                    {
                        name: "普定县",
                        attr: {
                            id: "33|04|03"
                        }
                    },
                    {
                        name: "镇宁布依族苗族自治县",
                        attr: {
                            id: "33|04|04"
                        }
                    },
                    {
                        name: "关岭布依族苗族自治县",
                        attr: {
                            id: "33|04|05"
                        }
                    },
                    {
                        name: "紫云苗族布依族自治县",
                        attr: {
                            id: "33|04|06"
                        }
                    }
                ]
            },
            {
                name: "铜仁",
                attr: {
                    id: "33|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "碧江区",
                        attr: {
                            id: "33|05|01"
                        }
                    },
                    {
                        name: "碧江区",
                        attr: {
                            id: "33|05|02"
                        }
                    },
                    {
                        name: "玉屏侗族自治县",
                        attr: {
                            id: "33|05|03"
                        }
                    },
                    {
                        name: "石阡县",
                        attr: {
                            id: "33|05|04"
                        }
                    },
                    {
                        name: "思南县",
                        attr: {
                            id: "33|05|05"
                        }
                    },
                    {
                        name: "印江土家族苗族自治县",
                        attr: {
                            id: "33|05|06"
                        }
                    },
                    {
                        name: "德江县",
                        attr: {
                            id: "33|05|07"
                        }
                    },
                    {
                        name: "沿河土家族自治县",
                        attr: {
                            id: "33|05|08"
                        }
                    },
                    {
                        name: "松桃苗族自治县",
                        attr: {
                            id: "33|05|09"
                        }
                    },
                    {
                        name: "万山区",
                        attr: {
                            id: "33|05|10"
                        }
                    }
                ]
            },
            {
                name: "黔西南",
                attr: {
                    id: "33|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "兴义市",
                        attr: {
                            id: "33|06|01"
                        }
                    },
                    {
                        name: "兴仁县",
                        attr: {
                            id: "33|06|02"
                        }
                    },
                    {
                        name: "普安县",
                        attr: {
                            id: "33|06|03"
                        }
                    },
                    {
                        name: "晴隆县",
                        attr: {
                            id: "33|06|04"
                        }
                    },
                    {
                        name: "贞丰县",
                        attr: {
                            id: "33|06|05"
                        }
                    },
                    {
                        name: "望谟县",
                        attr: {
                            id: "33|06|06"
                        }
                    },
                    {
                        name: "册亨县",
                        attr: {
                            id: "33|06|07"
                        }
                    },
                    {
                        name: "安龙县",
                        attr: {
                            id: "33|06|08"
                        }
                    }
                ]
            },
            {
                name: "毕节",
                attr: {
                    id: "33|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "七星关区",
                        attr: {
                            id: "33|07|01"
                        }
                    },
                    {
                        name: "大方县",
                        attr: {
                            id: "33|07|02"
                        }
                    },
                    {
                        name: "黔西县",
                        attr: {
                            id: "33|07|03"
                        }
                    },
                    {
                        name: "金沙县",
                        attr: {
                            id: "33|07|04"
                        }
                    },
                    {
                        name: "织金县",
                        attr: {
                            id: "33|07|05"
                        }
                    },
                    {
                        name: "纳雍县",
                        attr: {
                            id: "33|07|06"
                        }
                    },
                    {
                        name: "威宁彝族回族苗族自治县",
                        attr: {
                            id: "33|07|07"
                        }
                    },
                    {
                        name: "赫章县",
                        attr: {
                            id: "33|07|08"
                        }
                    }
                ]
            },
            {
                name: "黔东南",
                attr: {
                    id: "33|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "凯里市",
                        attr: {
                            id: "33|08|01"
                        }
                    },
                    {
                        name: "黄平县",
                        attr: {
                            id: "33|08|02"
                        }
                    },
                    {
                        name: "施秉县",
                        attr: {
                            id: "33|08|03"
                        }
                    },
                    {
                        name: "三穗县",
                        attr: {
                            id: "33|08|04"
                        }
                    },
                    {
                        name: "镇远县",
                        attr: {
                            id: "33|08|05"
                        }
                    },
                    {
                        name: "岑巩县",
                        attr: {
                            id: "33|08|06"
                        }
                    },
                    {
                        name: "天柱县",
                        attr: {
                            id: "33|08|07"
                        }
                    },
                    {
                        name: "锦屏县",
                        attr: {
                            id: "33|08|08"
                        }
                    },
                    {
                        name: "剑河县",
                        attr: {
                            id: "33|08|09"
                        }
                    },
                    {
                        name: "台江县",
                        attr: {
                            id: "33|08|10"
                        }
                    },
                    {
                        name: "黎平县",
                        attr: {
                            id: "33|08|11"
                        }
                    },
                    {
                        name: "榕江县",
                        attr: {
                            id: "33|08|12"
                        }
                    },
                    {
                        name: "从江县",
                        attr: {
                            id: "33|08|13"
                        }
                    },
                    {
                        name: "雷山县",
                        attr: {
                            id: "33|08|14"
                        }
                    },
                    {
                        name: "麻江县",
                        attr: {
                            id: "33|08|15"
                        }
                    },
                    {
                        name: "丹寨县",
                        attr: {
                            id: "33|08|16"
                        }
                    }
                ]
            },
            {
                name: "黔南",
                attr: {
                    id: "33|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "都匀市",
                        attr: {
                            id: "33|09|01"
                        }
                    },
                    {
                        name: "福泉市",
                        attr: {
                            id: "33|09|02"
                        }
                    },
                    {
                        name: "荔波县",
                        attr: {
                            id: "33|09|03"
                        }
                    },
                    {
                        name: "贵定县",
                        attr: {
                            id: "33|09|04"
                        }
                    },
                    {
                        name: "瓮安县",
                        attr: {
                            id: "33|09|05"
                        }
                    },
                    {
                        name: "独山县",
                        attr: {
                            id: "33|09|06"
                        }
                    },
                    {
                        name: "平塘县",
                        attr: {
                            id: "33|09|07"
                        }
                    },
                    {
                        name: "罗甸县",
                        attr: {
                            id: "33|09|08"
                        }
                    },
                    {
                        name: "长顺县",
                        attr: {
                            id: "33|09|09"
                        }
                    },
                    {
                        name: "龙里县",
                        attr: {
                            id: "33|09|10"
                        }
                    },
                    {
                        name: "惠水县",
                        attr: {
                            id: "33|09|11"
                        }
                    },
                    {
                        name: "三都水族自治县",
                        attr: {
                            id: "33|09|12"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "云南",
        attr: {
            id: "34|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "昆明",
                attr: {
                    id: "34|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "五华区",
                        attr: {
                            id: "34|01|01"
                        }
                    },
                    {
                        name: "盘龙区",
                        attr: {
                            id: "34|01|02"
                        }
                    },
                    {
                        name: "官渡区",
                        attr: {
                            id: "34|01|03"
                        }
                    },
                    {
                        name: "西山区",
                        attr: {
                            id: "34|01|04"
                        }
                    },
                    {
                        name: "东川区",
                        attr: {
                            id: "34|01|05"
                        }
                    },
                    {
                        name: "呈贡县",
                        attr: {
                            id: "34|01|06"
                        }
                    },
                    {
                        name: "晋宁县",
                        attr: {
                            id: "34|01|07"
                        }
                    },
                    {
                        name: "富民县",
                        attr: {
                            id: "34|01|08"
                        }
                    },
                    {
                        name: "宜良县",
                        attr: {
                            id: "34|01|09"
                        }
                    },
                    {
                        name: "石林彝族自治县",
                        attr: {
                            id: "34|01|10"
                        }
                    },
                    {
                        name: "嵩明县",
                        attr: {
                            id: "34|01|11"
                        }
                    },
                    {
                        name: "禄劝彝族苗族自治县",
                        attr: {
                            id: "34|01|12"
                        }
                    },
                    {
                        name: "寻甸回族彝族自治县",
                        attr: {
                            id: "34|01|13"
                        }
                    },
                    {
                        name: "安宁市",
                        attr: {
                            id: "34|01|14"
                        }
                    }
                ]
            },
            {
                name: "曲靖",
                attr: {
                    id: "34|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "麒麟区",
                        attr: {
                            id: "34|02|01"
                        }
                    },
                    {
                        name: "马龙县",
                        attr: {
                            id: "34|02|02"
                        }
                    },
                    {
                        name: "陆良县",
                        attr: {
                            id: "34|02|03"
                        }
                    },
                    {
                        name: "师宗县",
                        attr: {
                            id: "34|02|04"
                        }
                    },
                    {
                        name: "罗平县",
                        attr: {
                            id: "34|02|05"
                        }
                    },
                    {
                        name: "富源县",
                        attr: {
                            id: "34|02|06"
                        }
                    },
                    {
                        name: "会泽县",
                        attr: {
                            id: "34|02|07"
                        }
                    },
                    {
                        name: "沾益县",
                        attr: {
                            id: "34|02|08"
                        }
                    },
                    {
                        name: "宣威市",
                        attr: {
                            id: "34|02|09"
                        }
                    }
                ]
            },
            {
                name: "玉溪",
                attr: {
                    id: "34|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "红塔区",
                        attr: {
                            id: "34|03|01"
                        }
                    },
                    {
                        name: "江川县",
                        attr: {
                            id: "34|03|02"
                        }
                    },
                    {
                        name: "澄江县",
                        attr: {
                            id: "34|03|03"
                        }
                    },
                    {
                        name: "通海县",
                        attr: {
                            id: "34|03|04"
                        }
                    },
                    {
                        name: "华宁县",
                        attr: {
                            id: "34|03|05"
                        }
                    },
                    {
                        name: "易门县",
                        attr: {
                            id: "34|03|06"
                        }
                    },
                    {
                        name: "峨山彝族自治县",
                        attr: {
                            id: "34|03|07"
                        }
                    },
                    {
                        name: "新平彝族傣族自治县",
                        attr: {
                            id: "34|03|08"
                        }
                    },
                    {
                        name: "元江哈尼族彝族傣族自治县",
                        attr: {
                            id: "34|03|09"
                        }
                    }
                ]
            },
            {
                name: "保山",
                attr: {
                    id: "34|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "隆阳区",
                        attr: {
                            id: "34|04|01"
                        }
                    },
                    {
                        name: "施甸县",
                        attr: {
                            id: "34|04|02"
                        }
                    },
                    {
                        name: "腾冲县",
                        attr: {
                            id: "34|04|03"
                        }
                    },
                    {
                        name: "龙陵县",
                        attr: {
                            id: "34|04|04"
                        }
                    },
                    {
                        name: "昌宁县",
                        attr: {
                            id: "34|04|05"
                        }
                    }
                ]
            },
            {
                name: "昭通",
                attr: {
                    id: "34|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "昭阳区",
                        attr: {
                            id: "34|05|01"
                        }
                    },
                    {
                        name: "鲁甸县",
                        attr: {
                            id: "34|05|02"
                        }
                    },
                    {
                        name: "巧家县",
                        attr: {
                            id: "34|05|03"
                        }
                    },
                    {
                        name: "盐津县",
                        attr: {
                            id: "34|05|04"
                        }
                    },
                    {
                        name: "大关县",
                        attr: {
                            id: "34|05|05"
                        }
                    },
                    {
                        name: "永善县",
                        attr: {
                            id: "34|05|06"
                        }
                    },
                    {
                        name: "绥江县",
                        attr: {
                            id: "34|05|07"
                        }
                    },
                    {
                        name: "镇雄县",
                        attr: {
                            id: "34|05|08"
                        }
                    },
                    {
                        name: "彝良县",
                        attr: {
                            id: "34|05|09"
                        }
                    },
                    {
                        name: "威信县",
                        attr: {
                            id: "34|05|10"
                        }
                    },
                    {
                        name: "水富县",
                        attr: {
                            id: "34|05|11"
                        }
                    }
                ]
            },
            {
                name: "丽江",
                attr: {
                    id: "34|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "古城区",
                        attr: {
                            id: "34|06|01"
                        }
                    },
                    {
                        name: "玉龙纳西族自治县",
                        attr: {
                            id: "34|06|02"
                        }
                    },
                    {
                        name: "永胜县",
                        attr: {
                            id: "34|06|03"
                        }
                    },
                    {
                        name: "华坪县",
                        attr: {
                            id: "34|06|04"
                        }
                    },
                    {
                        name: "宁蒗彝族自治县",
                        attr: {
                            id: "34|06|05"
                        }
                    }
                ]
            },
            {
                name: "普洱",
                attr: {
                    id: "34|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "思茅区",
                        attr: {
                            id: "34|07|01"
                        }
                    },
                    {
                        name: "宁洱镇",
                        attr: {
                            id: "34|07|02"
                        }
                    },
                    {
                        name: "墨江哈尼族自治县",
                        attr: {
                            id: "34|07|03"
                        }
                    },
                    {
                        name: "景东彝族自治县",
                        attr: {
                            id: "34|07|04"
                        }
                    },
                    {
                        name: "景谷傣族彝族自治县",
                        attr: {
                            id: "34|07|05"
                        }
                    },
                    {
                        name: "镇沅彝族哈尼族拉祜族自治县",
                        attr: {
                            id: "34|07|06"
                        }
                    },
                    {
                        name: "江城哈尼族彝族自治县",
                        attr: {
                            id: "34|07|07"
                        }
                    },
                    {
                        name: "孟连傣族拉祜族佤族自治县",
                        attr: {
                            id: "34|07|08"
                        }
                    },
                    {
                        name: "澜沧拉祜族自治县",
                        attr: {
                            id: "34|07|09"
                        }
                    },
                    {
                        name: "西盟佤族自治县",
                        attr: {
                            id: "34|07|10"
                        }
                    }
                ]
            },
            {
                name: "临沧",
                attr: {
                    id: "34|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "临翔区",
                        attr: {
                            id: "34|08|01"
                        }
                    },
                    {
                        name: "凤庆县",
                        attr: {
                            id: "34|08|02"
                        }
                    },
                    {
                        name: "云县",
                        attr: {
                            id: "34|08|03"
                        }
                    },
                    {
                        name: "永德县",
                        attr: {
                            id: "34|08|04"
                        }
                    },
                    {
                        name: "镇康县",
                        attr: {
                            id: "34|08|05"
                        }
                    },
                    {
                        name: "双江拉祜族佤族布朗族傣族自治县",
                        attr: {
                            id: "34|08|06"
                        }
                    },
                    {
                        name: "耿马傣族佤族自治县",
                        attr: {
                            id: "34|08|07"
                        }
                    },
                    {
                        name: "沧源佤族自治县",
                        attr: {
                            id: "34|08|08"
                        }
                    }
                ]
            },
            {
                name: "楚雄",
                attr: {
                    id: "34|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "楚雄市",
                        attr: {
                            id: "34|09|01"
                        }
                    },
                    {
                        name: "双柏县",
                        attr: {
                            id: "34|09|02"
                        }
                    },
                    {
                        name: "牟定县",
                        attr: {
                            id: "34|09|03"
                        }
                    },
                    {
                        name: "南华县",
                        attr: {
                            id: "34|09|04"
                        }
                    },
                    {
                        name: "姚安县",
                        attr: {
                            id: "34|09|05"
                        }
                    },
                    {
                        name: "大姚县",
                        attr: {
                            id: "34|09|06"
                        }
                    },
                    {
                        name: "永仁县",
                        attr: {
                            id: "34|09|07"
                        }
                    },
                    {
                        name: "元谋县",
                        attr: {
                            id: "34|09|08"
                        }
                    },
                    {
                        name: "武定县",
                        attr: {
                            id: "34|09|09"
                        }
                    },
                    {
                        name: "禄丰县",
                        attr: {
                            id: "34|09|10"
                        }
                    }
                ]
            },
            {
                name: "红河",
                attr: {
                    id: "34|10|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "个旧市",
                        attr: {
                            id: "34|10|01"
                        }
                    },
                    {
                        name: "开远市",
                        attr: {
                            id: "34|10|02"
                        }
                    },
                    {
                        name: "蒙自县",
                        attr: {
                            id: "34|10|03"
                        }
                    },
                    {
                        name: "屏边苗族自治县",
                        attr: {
                            id: "34|10|04"
                        }
                    },
                    {
                        name: "建水县",
                        attr: {
                            id: "34|10|05"
                        }
                    },
                    {
                        name: "石屏县",
                        attr: {
                            id: "34|10|06"
                        }
                    },
                    {
                        name: "弥勒县",
                        attr: {
                            id: "34|10|07"
                        }
                    },
                    {
                        name: "泸西县",
                        attr: {
                            id: "34|10|08"
                        }
                    },
                    {
                        name: "元阳县",
                        attr: {
                            id: "34|10|09"
                        }
                    },
                    {
                        name: "红河县",
                        attr: {
                            id: "34|10|10"
                        }
                    },
                    {
                        name: "金平苗族瑶族傣族自治县",
                        attr: {
                            id: "34|10|11"
                        }
                    },
                    {
                        name: "绿春县",
                        attr: {
                            id: "34|10|12"
                        }
                    },
                    {
                        name: "河口瑶族自治县",
                        attr: {
                            id: "34|10|13"
                        }
                    }
                ]
            },
            {
                name: "文山",
                attr: {
                    id: "34|11|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "文山县",
                        attr: {
                            id: "34|11|01"
                        }
                    },
                    {
                        name: "砚山县",
                        attr: {
                            id: "34|11|02"
                        }
                    },
                    {
                        name: "西畴县",
                        attr: {
                            id: "34|11|03"
                        }
                    },
                    {
                        name: "麻栗坡县",
                        attr: {
                            id: "34|11|04"
                        }
                    },
                    {
                        name: "马关县",
                        attr: {
                            id: "34|11|05"
                        }
                    },
                    {
                        name: "丘北县",
                        attr: {
                            id: "34|11|06"
                        }
                    },
                    {
                        name: "广南县",
                        attr: {
                            id: "34|11|07"
                        }
                    },
                    {
                        name: "富宁县",
                        attr: {
                            id: "34|11|08"
                        }
                    }
                ]
            },
            {
                name: "西双版纳",
                attr: {
                    id: "34|12|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "景洪市",
                        attr: {
                            id: "34|12|01"
                        }
                    },
                    {
                        name: "勐海县",
                        attr: {
                            id: "34|12|02"
                        }
                    },
                    {
                        name: "勐腊县",
                        attr: {
                            id: "34|12|03"
                        }
                    }
                ]
            },
            {
                name: "大理",
                attr: {
                    id: "34|13|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "大理市",
                        attr: {
                            id: "34|13|01"
                        }
                    },
                    {
                        name: "漾濞彝族自治县",
                        attr: {
                            id: "34|13|02"
                        }
                    },
                    {
                        name: "祥云县",
                        attr: {
                            id: "34|13|03"
                        }
                    },
                    {
                        name: "宾川县",
                        attr: {
                            id: "34|13|04"
                        }
                    },
                    {
                        name: "弥渡县",
                        attr: {
                            id: "34|13|05"
                        }
                    },
                    {
                        name: "南涧彝族自治县",
                        attr: {
                            id: "34|13|06"
                        }
                    },
                    {
                        name: "巍山彝族回族自治县",
                        attr: {
                            id: "34|13|07"
                        }
                    },
                    {
                        name: "永平县",
                        attr: {
                            id: "34|13|08"
                        }
                    },
                    {
                        name: "云龙县",
                        attr: {
                            id: "34|13|09"
                        }
                    },
                    {
                        name: "洱源县",
                        attr: {
                            id: "34|13|10"
                        }
                    },
                    {
                        name: "剑川县",
                        attr: {
                            id: "34|13|11"
                        }
                    },
                    {
                        name: "鹤庆县",
                        attr: {
                            id: "34|13|12"
                        }
                    }
                ]
            },
            {
                name: "德宏",
                attr: {
                    id: "34|14|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "瑞丽市",
                        attr: {
                            id: "34|14|01"
                        }
                    },
                    {
                        name: "芒市",
                        attr: {
                            id: "34|14|02"
                        }
                    },
                    {
                        name: "梁河县",
                        attr: {
                            id: "34|14|03"
                        }
                    },
                    {
                        name: "盈江县",
                        attr: {
                            id: "34|14|04"
                        }
                    },
                    {
                        name: "陇川县",
                        attr: {
                            id: "34|14|05"
                        }
                    }
                ]
            },
            {
                name: "怒江",
                attr: {
                    id: "34|15|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "泸水县",
                        attr: {
                            id: "34|15|01"
                        }
                    },
                    {
                        name: "福贡县",
                        attr: {
                            id: "34|15|02"
                        }
                    },
                    {
                        name: "贡山独龙族怒族自治县",
                        attr: {
                            id: "34|15|03"
                        }
                    },
                    {
                        name: "兰坪白族普米族自治县",
                        attr: {
                            id: "34|15|04"
                        }
                    }
                ]
            },
            {
                name: "迪庆",
                attr: {
                    id: "34|16|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "香格里拉县",
                        attr: {
                            id: "34|16|01"
                        }
                    },
                    {
                        name: "德钦县",
                        attr: {
                            id: "34|16|02"
                        }
                    },
                    {
                        name: "维西傈僳族自治县",
                        attr: {
                            id: "34|16|03"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "西藏",
        attr: {
            id: "35|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "拉萨",
                attr: {
                    id: "35|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "城关区",
                        attr: {
                            id: "35|01|01"
                        }
                    },
                    {
                        name: "林周县",
                        attr: {
                            id: "35|01|02"
                        }
                    },
                    {
                        name: "当雄县",
                        attr: {
                            id: "35|01|03"
                        }
                    },
                    {
                        name: "尼木县",
                        attr: {
                            id: "35|01|04"
                        }
                    },
                    {
                        name: "曲水县",
                        attr: {
                            id: "35|01|05"
                        }
                    },
                    {
                        name: "堆龙德庆县",
                        attr: {
                            id: "35|01|06"
                        }
                    },
                    {
                        name: "达孜县",
                        attr: {
                            id: "35|01|07"
                        }
                    },
                    {
                        name: "墨竹工卡县",
                        attr: {
                            id: "35|01|08"
                        }
                    }
                ]
            },
            {
                name: "昌都",
                attr: {
                    id: "35|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "昌都县",
                        attr: {
                            id: "35|02|01"
                        }
                    },
                    {
                        name: "江达县",
                        attr: {
                            id: "35|02|02"
                        }
                    },
                    {
                        name: "贡觉县",
                        attr: {
                            id: "35|02|03"
                        }
                    },
                    {
                        name: "类乌齐县",
                        attr: {
                            id: "35|02|04"
                        }
                    },
                    {
                        name: "丁青县",
                        attr: {
                            id: "35|02|05"
                        }
                    },
                    {
                        name: "察雅县",
                        attr: {
                            id: "35|02|06"
                        }
                    },
                    {
                        name: "八宿县",
                        attr: {
                            id: "35|02|07"
                        }
                    },
                    {
                        name: "左贡县",
                        attr: {
                            id: "35|02|08"
                        }
                    },
                    {
                        name: "芒康县",
                        attr: {
                            id: "35|02|09"
                        }
                    },
                    {
                        name: "洛隆县",
                        attr: {
                            id: "35|02|10"
                        }
                    },
                    {
                        name: "边坝县",
                        attr: {
                            id: "35|02|11"
                        }
                    }
                ]
            },
            {
                name: "山南",
                attr: {
                    id: "35|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "乃东县",
                        attr: {
                            id: "35|03|01"
                        }
                    },
                    {
                        name: "扎囊县",
                        attr: {
                            id: "35|03|02"
                        }
                    },
                    {
                        name: "贡嘎县",
                        attr: {
                            id: "35|03|03"
                        }
                    },
                    {
                        name: "桑日县",
                        attr: {
                            id: "35|03|04"
                        }
                    },
                    {
                        name: "琼结县",
                        attr: {
                            id: "35|03|05"
                        }
                    },
                    {
                        name: "曲松县",
                        attr: {
                            id: "35|03|06"
                        }
                    },
                    {
                        name: "措美县",
                        attr: {
                            id: "35|03|07"
                        }
                    },
                    {
                        name: "洛扎县",
                        attr: {
                            id: "35|03|08"
                        }
                    },
                    {
                        name: "加查县",
                        attr: {
                            id: "35|03|09"
                        }
                    },
                    {
                        name: "隆子县",
                        attr: {
                            id: "35|03|10"
                        }
                    },
                    {
                        name: "错那县",
                        attr: {
                            id: "35|03|11"
                        }
                    },
                    {
                        name: "浪卡子县",
                        attr: {
                            id: "35|03|12"
                        }
                    }
                ]
            },
            {
                name: "日喀则",
                attr: {
                    id: "35|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "日喀则市",
                        attr: {
                            id: "35|04|01"
                        }
                    },
                    {
                        name: "南木林县",
                        attr: {
                            id: "35|04|02"
                        }
                    },
                    {
                        name: "江孜县",
                        attr: {
                            id: "35|04|03"
                        }
                    },
                    {
                        name: "定日县",
                        attr: {
                            id: "35|04|04"
                        }
                    },
                    {
                        name: "萨迦县",
                        attr: {
                            id: "35|04|05"
                        }
                    },
                    {
                        name: "拉孜县",
                        attr: {
                            id: "35|04|06"
                        }
                    },
                    {
                        name: "昂仁县",
                        attr: {
                            id: "35|04|07"
                        }
                    },
                    {
                        name: "谢通门县",
                        attr: {
                            id: "35|04|08"
                        }
                    },
                    {
                        name: "白朗县",
                        attr: {
                            id: "35|04|09"
                        }
                    },
                    {
                        name: "仁布县",
                        attr: {
                            id: "35|04|10"
                        }
                    },
                    {
                        name: "康马县",
                        attr: {
                            id: "35|04|11"
                        }
                    },
                    {
                        name: "定结县",
                        attr: {
                            id: "35|04|12"
                        }
                    },
                    {
                        name: "仲巴县",
                        attr: {
                            id: "35|04|13"
                        }
                    },
                    {
                        name: "亚东县",
                        attr: {
                            id: "35|04|14"
                        }
                    },
                    {
                        name: "吉隆县",
                        attr: {
                            id: "35|04|15"
                        }
                    },
                    {
                        name: "聂拉木县",
                        attr: {
                            id: "35|04|16"
                        }
                    },
                    {
                        name: "萨嘎县",
                        attr: {
                            id: "35|04|17"
                        }
                    },
                    {
                        name: "岗巴县",
                        attr: {
                            id: "35|04|18"
                        }
                    }
                ]
            },
            {
                name: "那曲",
                attr: {
                    id: "35|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "那曲县",
                        attr: {
                            id: "35|05|01"
                        }
                    },
                    {
                        name: "嘉黎县",
                        attr: {
                            id: "35|05|02"
                        }
                    },
                    {
                        name: "比如县",
                        attr: {
                            id: "35|05|03"
                        }
                    },
                    {
                        name: "聂荣县",
                        attr: {
                            id: "35|05|04"
                        }
                    },
                    {
                        name: "安多县",
                        attr: {
                            id: "35|05|05"
                        }
                    },
                    {
                        name: "申扎县",
                        attr: {
                            id: "35|05|06"
                        }
                    },
                    {
                        name: "索县",
                        attr: {
                            id: "35|05|07"
                        }
                    },
                    {
                        name: "班戈县",
                        attr: {
                            id: "35|05|08"
                        }
                    },
                    {
                        name: "巴青县",
                        attr: {
                            id: "35|05|09"
                        }
                    },
                    {
                        name: "尼玛县",
                        attr: {
                            id: "35|05|10"
                        }
                    },
                    {
                        name: "双湖县",
                        attr: {
                            id: "35|05|11"
                        }
                    }
                ]
            },
            {
                name: "阿里",
                attr: {
                    id: "35|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "普兰县",
                        attr: {
                            id: "35|06|01"
                        }
                    },
                    {
                        name: "札达县",
                        attr: {
                            id: "35|06|02"
                        }
                    },
                    {
                        name: "噶尔县",
                        attr: {
                            id: "35|06|03"
                        }
                    },
                    {
                        name: "日土县",
                        attr: {
                            id: "35|06|04"
                        }
                    },
                    {
                        name: "革吉县",
                        attr: {
                            id: "35|06|05"
                        }
                    },
                    {
                        name: "改则县",
                        attr: {
                            id: "35|06|06"
                        }
                    },
                    {
                        name: "措勤县",
                        attr: {
                            id: "35|06|07"
                        }
                    }
                ]
            },
            {
                name: "林芝",
                attr: {
                    id: "35|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "林芝县",
                        attr: {
                            id: "35|07|01"
                        }
                    },
                    {
                        name: "工布江达县",
                        attr: {
                            id: "35|07|02"
                        }
                    },
                    {
                        name: "米林县",
                        attr: {
                            id: "35|07|03"
                        }
                    },
                    {
                        name: "墨脱县",
                        attr: {
                            id: "35|07|04"
                        }
                    },
                    {
                        name: "波密县",
                        attr: {
                            id: "35|07|05"
                        }
                    },
                    {
                        name: "察隅县",
                        attr: {
                            id: "35|07|06"
                        }
                    },
                    {
                        name: "朗县",
                        attr: {
                            id: "35|07|07"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "陕西",
        attr: {
            id: "36|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "西安",
                attr: {
                    id: "36|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "新城区",
                        attr: {
                            id: "36|01|01"
                        }
                    },
                    {
                        name: "碑林区",
                        attr: {
                            id: "36|01|02"
                        }
                    },
                    {
                        name: "莲湖区",
                        attr: {
                            id: "36|01|03"
                        }
                    },
                    {
                        name: "灞桥区",
                        attr: {
                            id: "36|01|04"
                        }
                    },
                    {
                        name: "未央区",
                        attr: {
                            id: "36|01|05"
                        }
                    },
                    {
                        name: "雁塔区",
                        attr: {
                            id: "36|01|06"
                        }
                    },
                    {
                        name: "阎良区",
                        attr: {
                            id: "36|01|07"
                        }
                    },
                    {
                        name: "临潼区",
                        attr: {
                            id: "36|01|08"
                        }
                    },
                    {
                        name: "长安区",
                        attr: {
                            id: "36|01|09"
                        }
                    },
                    {
                        name: "蓝田县",
                        attr: {
                            id: "36|01|10"
                        }
                    },
                    {
                        name: "周至县",
                        attr: {
                            id: "36|01|11"
                        }
                    },
                    {
                        name: "户县",
                        attr: {
                            id: "36|01|12"
                        }
                    },
                    {
                        name: "高陵县",
                        attr: {
                            id: "36|01|13"
                        }
                    }
                ]
            },
            {
                name: "铜川",
                attr: {
                    id: "36|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "王益区",
                        attr: {
                            id: "36|02|01"
                        }
                    },
                    {
                        name: "印台区",
                        attr: {
                            id: "36|02|02"
                        }
                    },
                    {
                        name: "耀州区",
                        attr: {
                            id: "36|02|03"
                        }
                    },
                    {
                        name: "宜君县",
                        attr: {
                            id: "36|02|04"
                        }
                    }
                ]
            },
            {
                name: "宝鸡",
                attr: {
                    id: "36|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "渭滨区",
                        attr: {
                            id: "36|03|01"
                        }
                    },
                    {
                        name: "金台区",
                        attr: {
                            id: "36|03|02"
                        }
                    },
                    {
                        name: "陈仓区",
                        attr: {
                            id: "36|03|03"
                        }
                    },
                    {
                        name: "凤翔县",
                        attr: {
                            id: "36|03|04"
                        }
                    },
                    {
                        name: "岐山县",
                        attr: {
                            id: "36|03|05"
                        }
                    },
                    {
                        name: "扶风县",
                        attr: {
                            id: "36|03|06"
                        }
                    },
                    {
                        name: "眉县",
                        attr: {
                            id: "36|03|07"
                        }
                    },
                    {
                        name: "陇县",
                        attr: {
                            id: "36|03|08"
                        }
                    },
                    {
                        name: "千阳县",
                        attr: {
                            id: "36|03|09"
                        }
                    },
                    {
                        name: "麟游县",
                        attr: {
                            id: "36|03|10"
                        }
                    },
                    {
                        name: "凤县",
                        attr: {
                            id: "36|03|11"
                        }
                    },
                    {
                        name: "太白县",
                        attr: {
                            id: "36|03|12"
                        }
                    }
                ]
            },
            {
                name: "咸阳",
                attr: {
                    id: "36|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "秦都区",
                        attr: {
                            id: "36|04|01"
                        }
                    },
                    {
                        name: "杨凌区",
                        attr: {
                            id: "36|04|02"
                        }
                    },
                    {
                        name: "渭城区",
                        attr: {
                            id: "36|04|03"
                        }
                    },
                    {
                        name: "三原县",
                        attr: {
                            id: "36|04|04"
                        }
                    },
                    {
                        name: "泾阳县",
                        attr: {
                            id: "36|04|05"
                        }
                    },
                    {
                        name: "乾县",
                        attr: {
                            id: "36|04|06"
                        }
                    },
                    {
                        name: "礼泉县",
                        attr: {
                            id: "36|04|07"
                        }
                    },
                    {
                        name: "永寿县",
                        attr: {
                            id: "36|04|08"
                        }
                    },
                    {
                        name: "彬县",
                        attr: {
                            id: "36|04|09"
                        }
                    },
                    {
                        name: "长武县",
                        attr: {
                            id: "36|04|10"
                        }
                    },
                    {
                        name: "旬邑县",
                        attr: {
                            id: "36|04|11"
                        }
                    },
                    {
                        name: "淳化县",
                        attr: {
                            id: "36|04|12"
                        }
                    },
                    {
                        name: "武功县",
                        attr: {
                            id: "36|04|13"
                        }
                    },
                    {
                        name: "兴平市",
                        attr: {
                            id: "36|04|14"
                        }
                    }
                ]
            },
            {
                name: "渭南",
                attr: {
                    id: "36|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "临渭区",
                        attr: {
                            id: "36|05|01"
                        }
                    },
                    {
                        name: "华县",
                        attr: {
                            id: "36|05|02"
                        }
                    },
                    {
                        name: "潼关县",
                        attr: {
                            id: "36|05|03"
                        }
                    },
                    {
                        name: "大荔县",
                        attr: {
                            id: "36|05|04"
                        }
                    },
                    {
                        name: "合阳县",
                        attr: {
                            id: "36|05|05"
                        }
                    },
                    {
                        name: "澄城县",
                        attr: {
                            id: "36|05|06"
                        }
                    },
                    {
                        name: "蒲城县",
                        attr: {
                            id: "36|05|07"
                        }
                    },
                    {
                        name: "白水县",
                        attr: {
                            id: "36|05|08"
                        }
                    },
                    {
                        name: "富平县",
                        attr: {
                            id: "36|05|09"
                        }
                    },
                    {
                        name: "韩城市",
                        attr: {
                            id: "36|05|10"
                        }
                    },
                    {
                        name: "华阴市",
                        attr: {
                            id: "36|05|11"
                        }
                    }
                ]
            },
            {
                name: "延安",
                attr: {
                    id: "36|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "宝塔区",
                        attr: {
                            id: "36|06|01"
                        }
                    },
                    {
                        name: "延长县",
                        attr: {
                            id: "36|06|02"
                        }
                    },
                    {
                        name: "延川县",
                        attr: {
                            id: "36|06|03"
                        }
                    },
                    {
                        name: "子长县",
                        attr: {
                            id: "36|06|04"
                        }
                    },
                    {
                        name: "安塞县",
                        attr: {
                            id: "36|06|05"
                        }
                    },
                    {
                        name: "志丹县",
                        attr: {
                            id: "36|06|06"
                        }
                    },
                    {
                        name: "吴起县",
                        attr: {
                            id: "36|06|07"
                        }
                    },
                    {
                        name: "甘泉县",
                        attr: {
                            id: "36|06|08"
                        }
                    },
                    {
                        name: "富县",
                        attr: {
                            id: "36|06|09"
                        }
                    },
                    {
                        name: "洛川县",
                        attr: {
                            id: "36|06|10"
                        }
                    },
                    {
                        name: "宜川县",
                        attr: {
                            id: "36|06|11"
                        }
                    },
                    {
                        name: "黄龙县",
                        attr: {
                            id: "36|06|12"
                        }
                    },
                    {
                        name: "黄陵县",
                        attr: {
                            id: "36|06|13"
                        }
                    }
                ]
            },
            {
                name: "汉中",
                attr: {
                    id: "36|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "汉台区",
                        attr: {
                            id: "36|07|01"
                        }
                    },
                    {
                        name: "南郑县",
                        attr: {
                            id: "36|07|02"
                        }
                    },
                    {
                        name: "城固县",
                        attr: {
                            id: "36|07|03"
                        }
                    },
                    {
                        name: "洋县",
                        attr: {
                            id: "36|07|04"
                        }
                    },
                    {
                        name: "西乡县",
                        attr: {
                            id: "36|07|05"
                        }
                    },
                    {
                        name: "勉县",
                        attr: {
                            id: "36|07|06"
                        }
                    },
                    {
                        name: "宁强县",
                        attr: {
                            id: "36|07|07"
                        }
                    },
                    {
                        name: "略阳县",
                        attr: {
                            id: "36|07|08"
                        }
                    },
                    {
                        name: "镇巴县",
                        attr: {
                            id: "36|07|09"
                        }
                    },
                    {
                        name: "留坝县",
                        attr: {
                            id: "36|07|10"
                        }
                    },
                    {
                        name: "佛坪县",
                        attr: {
                            id: "36|07|11"
                        }
                    }
                ]
            },
            {
                name: "榆林",
                attr: {
                    id: "36|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "榆阳区",
                        attr: {
                            id: "36|08|01"
                        }
                    },
                    {
                        name: "神木县",
                        attr: {
                            id: "36|08|02"
                        }
                    },
                    {
                        name: "府谷县",
                        attr: {
                            id: "36|08|03"
                        }
                    },
                    {
                        name: "横山县",
                        attr: {
                            id: "36|08|04"
                        }
                    },
                    {
                        name: "靖边县",
                        attr: {
                            id: "36|08|05"
                        }
                    },
                    {
                        name: "定边县",
                        attr: {
                            id: "36|08|06"
                        }
                    },
                    {
                        name: "绥德县",
                        attr: {
                            id: "36|08|07"
                        }
                    },
                    {
                        name: "米脂县",
                        attr: {
                            id: "36|08|08"
                        }
                    },
                    {
                        name: "佳县",
                        attr: {
                            id: "36|08|09"
                        }
                    },
                    {
                        name: "吴堡县",
                        attr: {
                            id: "36|08|10"
                        }
                    },
                    {
                        name: "清涧县",
                        attr: {
                            id: "36|08|11"
                        }
                    },
                    {
                        name: "子洲县",
                        attr: {
                            id: "36|08|12"
                        }
                    }
                ]
            },
            {
                name: "安康",
                attr: {
                    id: "36|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "汉滨区",
                        attr: {
                            id: "36|09|01"
                        }
                    },
                    {
                        name: "汉阴县",
                        attr: {
                            id: "36|09|02"
                        }
                    },
                    {
                        name: "石泉县",
                        attr: {
                            id: "36|09|03"
                        }
                    },
                    {
                        name: "宁陕县",
                        attr: {
                            id: "36|09|04"
                        }
                    },
                    {
                        name: "紫阳县",
                        attr: {
                            id: "36|09|05"
                        }
                    },
                    {
                        name: "岚皋县",
                        attr: {
                            id: "36|09|06"
                        }
                    },
                    {
                        name: "平利县",
                        attr: {
                            id: "36|09|07"
                        }
                    },
                    {
                        name: "镇坪县",
                        attr: {
                            id: "36|09|08"
                        }
                    },
                    {
                        name: "旬阳县",
                        attr: {
                            id: "36|09|09"
                        }
                    },
                    {
                        name: "白河县",
                        attr: {
                            id: "36|09|10"
                        }
                    }
                ]
            },
            {
                name: "商洛",
                attr: {
                    id: "36|10|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "商州区",
                        attr: {
                            id: "36|10|01"
                        }
                    },
                    {
                        name: "洛南县",
                        attr: {
                            id: "36|10|02"
                        }
                    },
                    {
                        name: "丹凤县",
                        attr: {
                            id: "36|10|03"
                        }
                    },
                    {
                        name: "商南县",
                        attr: {
                            id: "36|10|04"
                        }
                    },
                    {
                        name: "山阳县",
                        attr: {
                            id: "36|10|05"
                        }
                    },
                    {
                        name: "镇安县",
                        attr: {
                            id: "36|10|06"
                        }
                    },
                    {
                        name: "柞水县",
                        attr: {
                            id: "36|10|07"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "甘肃",
        attr: {
            id: "37|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "兰州",
                attr: {
                    id: "37|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "城关区",
                        attr: {
                            id: "37|01|02"
                        }
                    },
                    {
                        name: "七里河区",
                        attr: {
                            id: "37|01|03"
                        }
                    },
                    {
                        name: "西固区",
                        attr: {
                            id: "37|01|04"
                        }
                    },
                    {
                        name: "安宁区",
                        attr: {
                            id: "37|01|05"
                        }
                    },
                    {
                        name: "红古区",
                        attr: {
                            id: "37|01|06"
                        }
                    },
                    {
                        name: "永登县",
                        attr: {
                            id: "37|01|07"
                        }
                    },
                    {
                        name: "皋兰县",
                        attr: {
                            id: "37|01|08"
                        }
                    },
                    {
                        name: "榆中县",
                        attr: {
                            id: "37|01|09"
                        }
                    }
                ]
            },
            {
                name: "嘉峪关",
                attr: {
                    id: "37|02|00"
                }
            },
            {
                name: "金昌",
                attr: {
                    id: "37|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "金川区",
                        attr: {
                            id: "37|03|01"
                        }
                    },
                    {
                        name: "永昌县",
                        attr: {
                            id: "37|03|02"
                        }
                    }
                ]
            },
            {
                name: "白银",
                attr: {
                    id: "37|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "白银区",
                        attr: {
                            id: "37|04|01"
                        }
                    },
                    {
                        name: "平川区",
                        attr: {
                            id: "37|04|02"
                        }
                    },
                    {
                        name: "靖远县",
                        attr: {
                            id: "37|04|03"
                        }
                    },
                    {
                        name: "会宁县",
                        attr: {
                            id: "37|04|04"
                        }
                    },
                    {
                        name: "景泰县",
                        attr: {
                            id: "37|04|05"
                        }
                    }
                ]
            },
            {
                name: "天水",
                attr: {
                    id: "37|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "秦城区",
                        attr: {
                            id: "37|05|01"
                        }
                    },
                    {
                        name: "麦积区",
                        attr: {
                            id: "37|05|02"
                        }
                    },
                    {
                        name: "清水县",
                        attr: {
                            id: "37|05|03"
                        }
                    },
                    {
                        name: "秦安县",
                        attr: {
                            id: "37|05|04"
                        }
                    },
                    {
                        name: "甘谷县",
                        attr: {
                            id: "37|05|05"
                        }
                    },
                    {
                        name: "武山县",
                        attr: {
                            id: "37|05|06"
                        }
                    },
                    {
                        name: "张家川回族自治县",
                        attr: {
                            id: "37|05|07"
                        }
                    }
                ]
            },
            {
                name: "武威",
                attr: {
                    id: "37|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "凉州区",
                        attr: {
                            id: "37|06|01"
                        }
                    },
                    {
                        name: "民勤县",
                        attr: {
                            id: "37|06|02"
                        }
                    },
                    {
                        name: "古浪县",
                        attr: {
                            id: "37|06|03"
                        }
                    },
                    {
                        name: "天祝藏族自治县",
                        attr: {
                            id: "37|06|04"
                        }
                    }
                ]
            },
            {
                name: "张掖",
                attr: {
                    id: "37|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "甘州区",
                        attr: {
                            id: "37|07|01"
                        }
                    },
                    {
                        name: "肃南裕固族自治县",
                        attr: {
                            id: "37|07|02"
                        }
                    },
                    {
                        name: "民乐县",
                        attr: {
                            id: "37|07|03"
                        }
                    },
                    {
                        name: "临泽县",
                        attr: {
                            id: "37|07|04"
                        }
                    },
                    {
                        name: "高台县",
                        attr: {
                            id: "37|07|05"
                        }
                    },
                    {
                        name: "山丹县",
                        attr: {
                            id: "37|07|06"
                        }
                    }
                ]
            },
            {
                name: "平凉",
                attr: {
                    id: "37|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "崆峒区",
                        attr: {
                            id: "37|08|01"
                        }
                    },
                    {
                        name: "泾川县",
                        attr: {
                            id: "37|08|02"
                        }
                    },
                    {
                        name: "灵台县",
                        attr: {
                            id: "37|08|03"
                        }
                    },
                    {
                        name: "崇信县",
                        attr: {
                            id: "37|08|04"
                        }
                    },
                    {
                        name: "华亭县",
                        attr: {
                            id: "37|08|05"
                        }
                    },
                    {
                        name: "庄浪县",
                        attr: {
                            id: "37|08|06"
                        }
                    },
                    {
                        name: "静宁县",
                        attr: {
                            id: "37|08|07"
                        }
                    }
                ]
            },
            {
                name: "酒泉",
                attr: {
                    id: "37|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "肃州区",
                        attr: {
                            id: "37|09|01"
                        }
                    },
                    {
                        name: "金塔县",
                        attr: {
                            id: "37|09|02"
                        }
                    },
                    {
                        name: "瓜州县",
                        attr: {
                            id: "37|09|03"
                        }
                    },
                    {
                        name: "肃北蒙古族自治县",
                        attr: {
                            id: "37|09|04"
                        }
                    },
                    {
                        name: "阿克塞哈萨克族自治县",
                        attr: {
                            id: "37|09|05"
                        }
                    },
                    {
                        name: "玉门市",
                        attr: {
                            id: "37|09|06"
                        }
                    },
                    {
                        name: "敦煌市",
                        attr: {
                            id: "37|09|07"
                        }
                    }
                ]
            },
            {
                name: "庆阳",
                attr: {
                    id: "37|10|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "西峰区",
                        attr: {
                            id: "37|10|01"
                        }
                    },
                    {
                        name: "庆城县",
                        attr: {
                            id: "37|10|02"
                        }
                    },
                    {
                        name: "环县",
                        attr: {
                            id: "37|10|03"
                        }
                    },
                    {
                        name: "华池县",
                        attr: {
                            id: "37|10|04"
                        }
                    },
                    {
                        name: "合水县",
                        attr: {
                            id: "37|10|05"
                        }
                    },
                    {
                        name: "正宁县",
                        attr: {
                            id: "37|10|06"
                        }
                    },
                    {
                        name: "宁县",
                        attr: {
                            id: "37|10|07"
                        }
                    },
                    {
                        name: "镇原县",
                        attr: {
                            id: "37|10|08"
                        }
                    }
                ]
            },
            {
                name: "定西",
                attr: {
                    id: "37|11|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "安定区",
                        attr: {
                            id: "37|11|01"
                        }
                    },
                    {
                        name: "通渭县",
                        attr: {
                            id: "37|11|02"
                        }
                    },
                    {
                        name: "陇西县",
                        attr: {
                            id: "37|11|03"
                        }
                    },
                    {
                        name: "渭源县",
                        attr: {
                            id: "37|11|04"
                        }
                    },
                    {
                        name: "临洮县",
                        attr: {
                            id: "37|11|05"
                        }
                    },
                    {
                        name: "漳县",
                        attr: {
                            id: "37|11|06"
                        }
                    },
                    {
                        name: "岷县",
                        attr: {
                            id: "37|11|07"
                        }
                    }
                ]
            },
            {
                name: "陇南",
                attr: {
                    id: "37|12|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "武都区",
                        attr: {
                            id: "37|12|01"
                        }
                    },
                    {
                        name: "成县",
                        attr: {
                            id: "37|12|02"
                        }
                    },
                    {
                        name: "文县",
                        attr: {
                            id: "37|12|03"
                        }
                    },
                    {
                        name: "宕昌县",
                        attr: {
                            id: "37|12|04"
                        }
                    },
                    {
                        name: "康县",
                        attr: {
                            id: "37|12|05"
                        }
                    },
                    {
                        name: "西和县",
                        attr: {
                            id: "37|12|06"
                        }
                    },
                    {
                        name: "礼县",
                        attr: {
                            id: "37|12|07"
                        }
                    },
                    {
                        name: "徽县",
                        attr: {
                            id: "37|12|08"
                        }
                    },
                    {
                        name: "两当县",
                        attr: {
                            id: "37|12|09"
                        }
                    }
                ]
            },
            {
                name: "临夏",
                attr: {
                    id: "37|13|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "临夏市",
                        attr: {
                            id: "37|13|01"
                        }
                    },
                    {
                        name: "临夏县",
                        attr: {
                            id: "37|13|02"
                        }
                    },
                    {
                        name: "康乐县",
                        attr: {
                            id: "37|13|03"
                        }
                    },
                    {
                        name: "永靖县",
                        attr: {
                            id: "37|13|04"
                        }
                    },
                    {
                        name: "广河县",
                        attr: {
                            id: "37|13|05"
                        }
                    },
                    {
                        name: "和政县",
                        attr: {
                            id: "37|13|06"
                        }
                    },
                    {
                        name: "东乡族自治县",
                        attr: {
                            id: "37|13|07"
                        }
                    },
                    {
                        name: "积石山保安族东乡族撒拉族自治县",
                        attr: {
                            id: "37|13|08"
                        }
                    }
                ]
            },
            {
                name: "甘南",
                attr: {
                    id: "37|14|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "合作市",
                        attr: {
                            id: "37|14|01"
                        }
                    },
                    {
                        name: "临潭县",
                        attr: {
                            id: "37|14|02"
                        }
                    },
                    {
                        name: "卓尼县",
                        attr: {
                            id: "37|14|03"
                        }
                    },
                    {
                        name: "舟曲县",
                        attr: {
                            id: "37|14|04"
                        }
                    },
                    {
                        name: "迭部县",
                        attr: {
                            id: "37|14|05"
                        }
                    },
                    {
                        name: "玛曲县",
                        attr: {
                            id: "37|14|06"
                        }
                    },
                    {
                        name: "碌曲县",
                        attr: {
                            id: "37|14|07"
                        }
                    },
                    {
                        name: "夏河县",
                        attr: {
                            id: "37|14|08"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "青海",
        attr: {
            id: "38|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "西宁",
                attr: {
                    id: "38|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "城东区",
                        attr: {
                            id: "38|01|01"
                        }
                    },
                    {
                        name: "城中区",
                        attr: {
                            id: "38|01|02"
                        }
                    },
                    {
                        name: "城西区",
                        attr: {
                            id: "38|01|03"
                        }
                    },
                    {
                        name: "城北区",
                        attr: {
                            id: "38|01|04"
                        }
                    },
                    {
                        name: "大通回族土族自治县",
                        attr: {
                            id: "38|01|05"
                        }
                    },
                    {
                        name: "湟中县",
                        attr: {
                            id: "38|01|06"
                        }
                    },
                    {
                        name: "湟源县",
                        attr: {
                            id: "38|01|07"
                        }
                    }
                ]
            },
            {
                name: "海东",
                attr: {
                    id: "38|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "平安县",
                        attr: {
                            id: "38|02|01"
                        }
                    },
                    {
                        name: "民和回族土族自治县",
                        attr: {
                            id: "38|02|02"
                        }
                    },
                    {
                        name: "乐都区",
                        attr: {
                            id: "38|02|03"
                        }
                    },
                    {
                        name: "互助土族自治县",
                        attr: {
                            id: "38|02|04"
                        }
                    },
                    {
                        name: "化隆回族自治县",
                        attr: {
                            id: "38|02|05"
                        }
                    },
                    {
                        name: "循化撒拉族自治县",
                        attr: {
                            id: "38|02|06"
                        }
                    }
                ]
            },
            {
                name: "海北",
                attr: {
                    id: "38|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "门源回族自治县",
                        attr: {
                            id: "38|03|01"
                        }
                    },
                    {
                        name: "祁连县",
                        attr: {
                            id: "38|03|02"
                        }
                    },
                    {
                        name: "海晏县",
                        attr: {
                            id: "38|03|03"
                        }
                    },
                    {
                        name: "刚察县",
                        attr: {
                            id: "38|03|04"
                        }
                    }
                ]
            },
            {
                name: "黄南",
                attr: {
                    id: "38|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "同仁县",
                        attr: {
                            id: "38|04|01"
                        }
                    },
                    {
                        name: "尖扎县",
                        attr: {
                            id: "38|04|02"
                        }
                    },
                    {
                        name: "泽库县",
                        attr: {
                            id: "38|04|03"
                        }
                    },
                    {
                        name: "河南蒙古族自治县",
                        attr: {
                            id: "38|04|04"
                        }
                    }
                ]
            },
            {
                name: "海南",
                attr: {
                    id: "38|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "共和县",
                        attr: {
                            id: "38|05|01"
                        }
                    },
                    {
                        name: "同德县",
                        attr: {
                            id: "38|05|02"
                        }
                    },
                    {
                        name: "贵德县",
                        attr: {
                            id: "38|05|03"
                        }
                    },
                    {
                        name: "兴海县",
                        attr: {
                            id: "38|05|04"
                        }
                    },
                    {
                        name: "贵南县",
                        attr: {
                            id: "38|05|05"
                        }
                    }
                ]
            },
            {
                name: "果洛",
                attr: {
                    id: "38|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "玛沁县",
                        attr: {
                            id: "38|06|01"
                        }
                    },
                    {
                        name: "班玛县",
                        attr: {
                            id: "38|06|02"
                        }
                    },
                    {
                        name: "甘德县",
                        attr: {
                            id: "38|06|03"
                        }
                    },
                    {
                        name: "达日县",
                        attr: {
                            id: "38|06|04"
                        }
                    },
                    {
                        name: "久治县",
                        attr: {
                            id: "38|06|05"
                        }
                    },
                    {
                        name: "玛多县",
                        attr: {
                            id: "38|06|06"
                        }
                    }
                ]
            },
            {
                name: "玉树",
                attr: {
                    id: "38|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "玉树市",
                        attr: {
                            id: "38|07|01"
                        }
                    },
                    {
                        name: "杂多县",
                        attr: {
                            id: "38|07|02"
                        }
                    },
                    {
                        name: "称多县",
                        attr: {
                            id: "38|07|03"
                        }
                    },
                    {
                        name: "治多县",
                        attr: {
                            id: "38|07|04"
                        }
                    },
                    {
                        name: "囊谦县",
                        attr: {
                            id: "38|07|05"
                        }
                    },
                    {
                        name: "曲麻莱县",
                        attr: {
                            id: "38|07|06"
                        }
                    }
                ]
            },
            {
                name: "梅西",
                attr: {
                    id: "38|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "格尔木市",
                        attr: {
                            id: "38|08|01"
                        }
                    },
                    {
                        name: "德令哈市",
                        attr: {
                            id: "38|08|02"
                        }
                    },
                    {
                        name: "乌兰县",
                        attr: {
                            id: "38|08|03"
                        }
                    },
                    {
                        name: "都兰县",
                        attr: {
                            id: "38|08|04"
                        }
                    },
                    {
                        name: "天峻县",
                        attr: {
                            id: "38|08|05"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "宁夏",
        attr: {
            id: "39|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "银川",
                attr: {
                    id: "39|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "兴庆区",
                        attr: {
                            id: "39|01|01"
                        }
                    },
                    {
                        name: "西夏区",
                        attr: {
                            id: "39|01|02"
                        }
                    },
                    {
                        name: "金凤区",
                        attr: {
                            id: "39|01|03"
                        }
                    },
                    {
                        name: "永宁县",
                        attr: {
                            id: "39|01|04"
                        }
                    },
                    {
                        name: "贺兰县",
                        attr: {
                            id: "39|01|05"
                        }
                    },
                    {
                        name: "灵武市",
                        attr: {
                            id: "39|01|06"
                        }
                    }
                ]
            },
            {
                name: "石嘴山",
                attr: {
                    id: "39|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "大武口区",
                        attr: {
                            id: "39|02|01"
                        }
                    },
                    {
                        name: "惠农区",
                        attr: {
                            id: "39|02|02"
                        }
                    },
                    {
                        name: "平罗县",
                        attr: {
                            id: "39|02|03"
                        }
                    }
                ]
            },
            {
                name: "吴忠",
                attr: {
                    id: "39|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "利通区",
                        attr: {
                            id: "39|03|01"
                        }
                    },
                    {
                        name: "红寺堡区",
                        attr: {
                            id: "39|03|02"
                        }
                    },
                    {
                        name: "盐池县",
                        attr: {
                            id: "39|03|03"
                        }
                    },
                    {
                        name: "同心县",
                        attr: {
                            id: "39|03|04"
                        }
                    },
                    {
                        name: "青铜峡市",
                        attr: {
                            id: "39|03|05"
                        }
                    }
                ]
            },
            {
                name: "固原",
                attr: {
                    id: "39|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "原州区",
                        attr: {
                            id: "39|04|01"
                        }
                    },
                    {
                        name: "西吉县",
                        attr: {
                            id: "39|04|02"
                        }
                    },
                    {
                        name: "隆德县",
                        attr: {
                            id: "39|04|03"
                        }
                    },
                    {
                        name: "泾源县",
                        attr: {
                            id: "39|04|04"
                        }
                    },
                    {
                        name: "彭阳县",
                        attr: {
                            id: "39|04|05"
                        }
                    }
                ]
            },
            {
                name: "中卫",
                attr: {
                    id: "39|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "沙坡头区",
                        attr: {
                            id: "39|05|01"
                        }
                    },
                    {
                        name: "中宁县",
                        attr: {
                            id: "39|05|02"
                        }
                    },
                    {
                        name: "海原县",
                        attr: {
                            id: "39|05|03"
                        }
                    }
                ]
            }
        ]
    },
    {
        name: "新疆",
        attr: {
            id: "40|00|00"
        },
        nextLevel: [
            {
                name: "请选择..."
            },
            {
                name: "乌鲁木齐",
                attr: {
                    id: "40|01|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "天山区",
                        attr: {
                            id: "40|01|01"
                        }
                    },
                    {
                        name: "沙依巴克区",
                        attr: {
                            id: "40|01|02"
                        }
                    },
                    {
                        name: "新市区",
                        attr: {
                            id: "40|01|03"
                        }
                    },
                    {
                        name: "水磨沟区",
                        attr: {
                            id: "40|01|04"
                        }
                    },
                    {
                        name: "头屯河区",
                        attr: {
                            id: "40|01|05"
                        }
                    },
                    {
                        name: "达坂城区",
                        attr: {
                            id: "40|01|06"
                        }
                    },
                    {
                        name: "米东区",
                        attr: {
                            id: "40|01|07"
                        }
                    },
                    {
                        name: "乌鲁木齐县",
                        attr: {
                            id: "40|01|08"
                        }
                    }
                ]
            },
            {
                name: "克拉玛依",
                attr: {
                    id: "40|02|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "独山子区",
                        attr: {
                            id: "40|02|01"
                        }
                    },
                    {
                        name: "克拉玛依区",
                        attr: {
                            id: "40|02|02"
                        }
                    },
                    {
                        name: "白碱滩区",
                        attr: {
                            id: "40|02|03"
                        }
                    },
                    {
                        name: "乌尔禾区",
                        attr: {
                            id: "40|02|04"
                        }
                    }
                ]
            },
            {
                name: "吐鲁番",
                attr: {
                    id: "40|03|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "吐鲁番市",
                        attr: {
                            id: "40|03|01"
                        }
                    },
                    {
                        name: "鄯善县",
                        attr: {
                            id: "40|03|02"
                        }
                    },
                    {
                        name: "托克逊县",
                        attr: {
                            id: "40|03|03"
                        }
                    }
                ]
            },
            {
                name: "哈密",
                attr: {
                    id: "40|04|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "哈密市",
                        attr: {
                            id: "40|04|01"
                        }
                    },
                    {
                        name: "巴里坤哈萨克自治县",
                        attr: {
                            id: "40|04|02"
                        }
                    },
                    {
                        name: "伊吾县",
                        attr: {
                            id: "40|04|03"
                        }
                    }
                ]
            },
            {
                name: "昌吉",
                attr: {
                    id: "40|05|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "昌吉市",
                        attr: {
                            id: "40|05|01"
                        }
                    },
                    {
                        name: "阜康市",
                        attr: {
                            id: "40|05|02"
                        }
                    },
                    {
                        name: "呼图壁县",
                        attr: {
                            id: "40|05|03"
                        }
                    },
                    {
                        name: "玛纳斯县",
                        attr: {
                            id: "40|05|04"
                        }
                    },
                    {
                        name: "奇台县",
                        attr: {
                            id: "40|05|05"
                        }
                    },
                    {
                        name: "吉木萨尔县",
                        attr: {
                            id: "40|05|06"
                        }
                    },
                    {
                        name: "木垒哈萨克自治县",
                        attr: {
                            id: "40|05|07"
                        }
                    }
                ]
            },
            {
                name: "博尔塔拉",
                attr: {
                    id: "40|06|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "博乐市",
                        attr: {
                            id: "40|06|01"
                        }
                    },
                    {
                        name: "精河县",
                        attr: {
                            id: "40|06|02"
                        }
                    },
                    {
                        name: "温泉县",
                        attr: {
                            id: "40|06|03"
                        }
                    },
                    {
                        name: "阿拉山口市",
                        attr: {
                            id: "40|06|04"
                        }
                    }
                ]
            },
            {
                name: "巴音郭楞",
                attr: {
                    id: "40|07|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "库尔勒市",
                        attr: {
                            id: "40|07|01"
                        }
                    },
                    {
                        name: "轮台县",
                        attr: {
                            id: "40|07|02"
                        }
                    },
                    {
                        name: "尉犁县",
                        attr: {
                            id: "40|07|03"
                        }
                    },
                    {
                        name: "若羌县",
                        attr: {
                            id: "40|07|04"
                        }
                    },
                    {
                        name: "且末县",
                        attr: {
                            id: "40|07|05"
                        }
                    },
                    {
                        name: "焉耆回族自治县",
                        attr: {
                            id: "40|07|06"
                        }
                    },
                    {
                        name: "和静县",
                        attr: {
                            id: "40|07|07"
                        }
                    },
                    {
                        name: "和硕县",
                        attr: {
                            id: "40|07|08"
                        }
                    },
                    {
                        name: "博湖县",
                        attr: {
                            id: "40|07|09"
                        }
                    }
                ]
            },
            {
                name: "阿克苏",
                attr: {
                    id: "40|08|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "阿克苏市",
                        attr: {
                            id: "40|08|01"
                        }
                    },
                    {
                        name: "温宿县",
                        attr: {
                            id: "40|08|02"
                        }
                    },
                    {
                        name: "库车县",
                        attr: {
                            id: "40|08|03"
                        }
                    },
                    {
                        name: "沙雅县",
                        attr: {
                            id: "40|08|04"
                        }
                    },
                    {
                        name: "新和县",
                        attr: {
                            id: "40|08|05"
                        }
                    },
                    {
                        name: "拜城县",
                        attr: {
                            id: "40|08|06"
                        }
                    },
                    {
                        name: "乌什县",
                        attr: {
                            id: "40|08|07"
                        }
                    },
                    {
                        name: "阿瓦提县",
                        attr: {
                            id: "40|08|08"
                        }
                    },
                    {
                        name: "柯坪县",
                        attr: {
                            id: "40|08|09"
                        }
                    }
                ]
            },
            {
                name: "克孜勒苏",//存档点
                attr: {
                    id: "40|09|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "市中区",
                        attr: {
                            id: "40|09|01"
                        }
                    },
                    {
                        name: "东兴区",
                        attr: {
                            id: "40|09|02"
                        }
                    },
                    {
                        name: "威远县",
                        attr: {
                            id: "40|09|03"
                        }
                    },
                    {
                        name: "资中县",
                        attr: {
                            id: "40|09|04"
                        }
                    },
                    {
                        name: "隆昌县",
                        attr: {
                            id: "40|09|05"
                        }
                    }
                ]
            },
            {
                name: "乐山",
                attr: {
                    id: "40|10|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "市中区",
                        attr: {
                            id: "40|10|01"
                        }
                    },
                    {
                        name: "沙湾区",
                        attr: {
                            id: "40|10|02"
                        }
                    },
                    {
                        name: "五通桥区",
                        attr: {
                            id: "40|10|03"
                        }
                    },
                    {
                        name: "金口河区",
                        attr: {
                            id: "40|10|04"
                        }
                    },
                    {
                        name: "犍为县",
                        attr: {
                            id: "40|10|05"
                        }
                    },
                    {
                        name: "井研县",
                        attr: {
                            id: "40|10|06"
                        }
                    },
                    {
                        name: "夹江县",
                        attr: {
                            id: "40|10|07"
                        }
                    },
                    {
                        name: "沐川县",
                        attr: {
                            id: "40|10|08"
                        }
                    },
                    {
                        name: "峨边彝族自治县",
                        attr: {
                            id: "40|10|09"
                        }
                    },
                    {
                        name: "马边彝族自治县",
                        attr: {
                            id: "40|10|10"
                        }
                    },
                    {
                        name: "峨眉山市",
                        attr: {
                            id: "40|10|11"
                        }
                    }
                ]
            },
            {
                name: "南充",
                attr: {
                    id: "40|11|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "顺庆区",
                        attr: {
                            id: "40|11|01"
                        }
                    },
                    {
                        name: "高坪区",
                        attr: {
                            id: "40|11|02"
                        }
                    },
                    {
                        name: "嘉陵区",
                        attr: {
                            id: "40|11|03"
                        }
                    },
                    {
                        name: "南部县",
                        attr: {
                            id: "40|11|04"
                        }
                    },
                    {
                        name: "营山县",
                        attr: {
                            id: "40|11|05"
                        }
                    },
                    {
                        name: "蓬安县",
                        attr: {
                            id: "40|11|06"
                        }
                    },
                    {
                        name: "仪陇县",
                        attr: {
                            id: "40|11|07"
                        }
                    },
                    {
                        name: "西充县",
                        attr: {
                            id: "40|11|08"
                        }
                    },
                    {
                        name: "阆中市",
                        attr: {
                            id: "40|11|09"
                        }
                    }
                ]
            },
            {
                name: "眉山",
                attr: {
                    id: "40|12|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "东坡区",
                        attr: {
                            id: "40|12|01"
                        }
                    },
                    {
                        name: "仁寿县",
                        attr: {
                            id: "40|12|02"
                        }
                    },
                    {
                        name: "彭山县",
                        attr: {
                            id: "40|12|03"
                        }
                    },
                    {
                        name: "洪雅县",
                        attr: {
                            id: "40|12|04"
                        }
                    },
                    {
                        name: "丹棱县",
                        attr: {
                            id: "40|12|05"
                        }
                    },
                    {
                        name: "青神县",
                        attr: {
                            id: "40|12|06"
                        }
                    }
                ]
            },
            {
                name: "宜宾",
                attr: {
                    id: "40|13|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "翠屏区",
                        attr: {
                            id: "40|13|01"
                        }
                    },
                    {
                        name: "宜宾县",
                        attr: {
                            id: "40|13|02"
                        }
                    },
                    {
                        name: "南溪区",
                        attr: {
                            id: "40|13|03"
                        }
                    },
                    {
                        name: "江安县",
                        attr: {
                            id: "40|13|04"
                        }
                    },
                    {
                        name: "长宁县",
                        attr: {
                            id: "40|13|05"
                        }
                    },
                    {
                        name: "高县",
                        attr: {
                            id: "40|13|06"
                        }
                    },
                    {
                        name: "珙县",
                        attr: {
                            id: "40|13|07"
                        }
                    },
                    {
                        name: "筠连县",
                        attr: {
                            id: "40|13|08"
                        }
                    },
                    {
                        name: "兴文县",
                        attr: {
                            id: "40|13|09"
                        }
                    },
                    {
                        name: "屏山县",
                        attr: {
                            id: "40|13|10"
                        }
                    }
                ]
            },
            {
                name: "广安",
                attr: {
                    id: "40|14|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "广安区",
                        attr: {
                            id: "40|14|01"
                        }
                    },
                    {
                        name: "岳池县",
                        attr: {
                            id: "40|14|02"
                        }
                    },
                    {
                        name: "武胜县",
                        attr: {
                            id: "40|14|03"
                        }
                    },
                    {
                        name: "邻水县",
                        attr: {
                            id: "40|14|04"
                        }
                    },
                    {
                        name: "华蓥市",
                        attr: {
                            id: "40|14|05"
                        }
                    },
                    {
                        name: "前锋区",
                        attr: {
                            id: "40|14|06"
                        }
                    }
                ]
            },
            {
                name: "达川",
                attr: {
                    id: "40|15|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "通川区",
                        attr: {
                            id: "40|15|01"
                        }
                    },
                    {
                        name: "达州区",
                        attr: {
                            id: "40|15|02"
                        }
                    },
                    {
                        name: "宣汉县",
                        attr: {
                            id: "40|15|03"
                        }
                    },
                    {
                        name: "开江县",
                        attr: {
                            id: "40|15|04"
                        }
                    },
                    {
                        name: "大竹县",
                        attr: {
                            id: "40|15|05"
                        }
                    },
                    {
                        name: "渠县",
                        attr: {
                            id: "40|15|06"
                        }
                    },
                    {
                        name: "万源市",
                        attr: {
                            id: "40|15|07"
                        }
                    }
                ]
            },
            {
                name: "雅安",
                attr: {
                    id: "40|16|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "雨城区",
                        attr: {
                            id: "40|16|01"
                        }
                    },
                    {
                        name: "名山县",
                        attr: {
                            id: "40|16|02"
                        }
                    },
                    {
                        name: "荥经县",
                        attr: {
                            id: "40|16|03"
                        }
                    },
                    {
                        name: "汉源县",
                        attr: {
                            id: "40|16|04"
                        }
                    },
                    {
                        name: "石棉县",
                        attr: {
                            id: "40|16|05"
                        }
                    },
                    {
                        name: "天全县",
                        attr: {
                            id: "40|16|06"
                        }
                    },
                    {
                        name: "芦山县",
                        attr: {
                            id: "40|16|07"
                        }
                    },
                    {
                        name: "宝兴县",
                        attr: {
                            id: "40|16|08"
                        }
                    }
                ]
            },
            {
                name: "巴中",
                attr: {
                    id: "40|17|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "巴州区",
                        attr: {
                            id: "40|17|01"
                        }
                    },
                    {
                        name: "通江县",
                        attr: {
                            id: "40|17|02"
                        }
                    },
                    {
                        name: "南江县",
                        attr: {
                            id: "40|17|03"
                        }
                    },
                    {
                        name: "平昌县",
                        attr: {
                            id: "40|17|04"
                        }
                    },
                    {
                        name: "恩阳区",
                        attr: {
                            id: "40|17|05"
                        }
                    }
                ]
            },
            {
                name: "资阳",
                attr: {
                    id: "40|18|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "雁江区",
                        attr: {
                            id: "40|18|01"
                        }
                    },
                    {
                        name: "安岳县",
                        attr: {
                            id: "40|18|02"
                        }
                    },
                    {
                        name: "乐至县",
                        attr: {
                            id: "40|18|03"
                        }
                    },
                    {
                        name: "简阳市",
                        attr: {
                            id: "40|18|04"
                        }
                    }
                ]
            },
            {
                name: "阿坝",
                attr: {
                    id: "40|19|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "汶川县",
                        attr: {
                            id: "40|19|01"
                        }
                    },
                    {
                        name: "理县",
                        attr: {
                            id: "40|19|02"
                        }
                    },
                    {
                        name: "茂县",
                        attr: {
                            id: "40|19|03"
                        }
                    },
                    {
                        name: "松潘县",
                        attr: {
                            id: "40|19|04"
                        }
                    },
                    {
                        name: "九寨沟县",
                        attr: {
                            id: "40|19|05"
                        }
                    },
                    {
                        name: "金川县",
                        attr: {
                            id: "40|19|06"
                        }
                    },
                    {
                        name: "小金县",
                        attr: {
                            id: "40|19|07"
                        }
                    },
                    {
                        name: "黑水县",
                        attr: {
                            id: "40|19|08"
                        }
                    },
                    {
                        name: "马尔康县",
                        attr: {
                            id: "40|19|09"
                        }
                    },
                    {
                        name: "壤塘县",
                        attr: {
                            id: "40|19|10"
                        }
                    },
                    {
                        name: "阿坝县",
                        attr: {
                            id: "40|19|11"
                        }
                    },
                    {
                        name: "若尔盖县",
                        attr: {
                            id: "40|19|12"
                        }
                    },
                    {
                        name: "红原县",
                        attr: {
                            id: "40|19|13"
                        }
                    }
                ]
            },
            {
                name: "甘孜",
                attr: {
                    id: "40|20|00"
                },
                nextLevel: [
                    {
                        name: "请选择..."
                    },
                    {
                        name: "康定县",
                        attr: {
                            id: "40|20|01"
                        }
                    },
                    {
                        name: "泸定县",
                        attr: {
                            id: "40|20|02"
                        }
                    },
                    {
                        name: "丹巴县",
                        attr: {
                            id: "40|20|03"
                        }
                    },
                    {
                        name: "九龙县",
                        attr: {
                            id: "40|20|04"
                        }
                    },
                    {
                        name: "雅江县",
                        attr: {
                            id: "40|20|05"
                        }
                    },
                    {
                        name: "道孚县",
                        attr: {
                            id: "40|20|06"
                        }
                    },
                    {
                        name: "炉霍县",
                        attr: {
                            id: "40|20|07"
                        }
                    },
                    {
                        name: "甘孜县",
                        attr: {
                            id: "40|20|08"
                        }
                    },
                    {
                        name: "新龙县",
                        attr: {
                            id: "40|20|09"
                        }
                    },
                    {
                        name: "德格县",
                        attr: {
                            id: "40|20|10"
                        }
                    },
                    {
                        name: "白玉县",
                        attr: {
                            id: "40|20|11"
                        }
                    },
                    {
                        name: "石渠县",
                        attr: {
                            id: "40|20|12"
                        }
                    },
                    {
                        name: "色达县",
                        attr: {
                            id: "40|20|13"
                        }
                    },
                    {
                        name: "理塘县",
                        attr: {
                            id: "40|20|14"
                        }
                    },
                    {
                        name: "巴塘县",
                        attr: {
                            id: "40|20|15"
                        }
                    },
                    {
                        name: "乡城县",
                        attr: {
                            id: "40|20|16"
                        }
                    },
                    {
                        name: "稻城县",
                        attr: {
                            id: "40|20|17"
                        }
                    },
                    {
                        name: "得荣县",
                        attr: {
                            id: "40|20|18"
                        }
                    }
                ]
            }
        ]
    }
]