window.onload = function() {
	WG.init();
};

window.WG = {
	init: function(T) {
		T = T || this;
		for (var p in T) {
			T[p] && T[p].init && T[p].init();
		};
	},
	stopBubble: function(e){  
		// 如果传入了事件对象，那么就是非ie浏览器  
		if(e&&e.stopPropagation){  
			//因此它支持W3C的stopPropagation()方法  
			e.stopPropagation();  
		}else{  
			//否则我们使用ie的方法来取消事件冒泡  
			window.event.cancelBubble = true;  
		}  
	}  
};
