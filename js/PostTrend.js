/**
 * [PostTrend 发布动态组件]
 * @type {Object}
 */
WG.PostTrend = {
	init: function () {
		$(".Js_posttrend_btn").on("click",this.showPostTrendBox);
	},
	showPostTrendBox: function(){

		var T = WG.PostTrend;
		var html = [
			'<div class="posttrend">',
			'   <div class="pt-title">',
			'       <span class="t fl">发布动态</span>',
			'       <a class="pt-close Js_pt_closebtn fr" href="javascript:">关闭</a>',
			'   </div>',
			'   <div class="pt-content">',
			'       <div class="texton">',
			'           <textarea name="" id="Js_posttrend_text" cols="30" rows="10" placeholder="请输入你要发布的内容"></textarea>',
			'       </div>',
			'       <div class="pt-bar">',
			'           <a class="abtn pt-pic Js_pt_uploadimg" href="javascript:">插入图片</a>',
			'           <a class="abtn pt-video Js_pt_videourl" href="javascript:">插入视频</a>',
			'       </div>',
			'       <div class="pt-video-url">',
			'           <input class="Js_pt_videourl_text video-url-text" type="text" placeholder="请输入你要发布视频的链接地址">',
			'       </div>',
			'       <div class="uploadWrap-more dargArea left">',
			'           <span class="btn lightgrey hide Js_uploadbox_more">',
			'               <span class="btn lightgrey_l">上传</span>',
			'           </span>',
			'           <input class="file_uploadbox-more hide" type="file" multiple="multiple" accept="image/gif,image/jpeg,image/x-png,image/tiff" name="cover">',
			'       </div>',
			'       <div id="upload-more" class="clearfix Js_fm_cover_div upload-more"></div>',
			'   </div>',
			'   <div class="pt-btnarea clearfix">',
			'       <span class="sblue-btn disable Js_pt_btn">发布动态</span>',
			'   </div>',
			'</div>'].join("");

		$.fancybox({
			fitToView   : false,
			autoSize    : true,
			padding     : 0,
			closeBtn    : false,
			content     : html,
			afterShow   : T.run
		});
	},
	run: function (){
		var T = WG.PostTrend;
		if($(".posttrend").length>0) T.setTex = setInterval(T.checkTextarea, 300);
		if(!T.st){
			$("body").on("click", ".Js_pt_uploadimg", T.upLoadimg );
			$("body").on("click", ".Js_pt_videourl", T.videoUrl );
			$("body").on("click", ".Js_pt_closebtn", T.closeTrendBox );
			T.st = true;
		}
		WG.UploadMore.init({
			//多文件上传的回调函数
			callback: function(){
				$.fancybox.reposition()
			}
		});
	},
	checkTextarea: function(){
		var $obj = $("#Js_posttrend_text");
		if($.trim($obj.val()) !== ""){
			$(".Js_pt_btn").removeClass('disable');
		}else{
			if($(".posttrend .updateSucImg").length>0){
				$(".Js_pt_btn").removeClass('disable');
				$(".Js_pt_uploadimg").addClass('on');
			}else{
				if($(".Js_pt_videourl_text").is(":visible") && $.trim($(".Js_pt_videourl_text").val()) !== ""){
					$(".Js_pt_btn").removeClass('disable');
				}else{
					$(".Js_pt_btn").addClass('disable');
					$(".Js_pt_uploadimg").removeClass('on');
				}
			}
		}
	},
	upLoadimg: function(){
		var $target = $(".Js_pt_uploadimg").parents('.pt-content').find(".Js_uploadbox_more");
		$target.click();
	},
	videoUrl: function(){
		if($(".pt-video-url").is(":hidden")){
			$(".pt-video-url").show();
			$(this).addClass('on')
		}else{
			$(".pt-video-url").hide();
			$(this).removeClass('on')
		}
	},
	closeTrendBox: function(){
		$.fancybox.close()
	}
};