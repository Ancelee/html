$(function(){
    var endwidth = 54,//两端的宽度
        totallength = 850,//总长度
        marginwidth,
        marginarray = [],//中间的margin宽度
        liwidth = $('.Js_tab:eq(0) li').width(),
        tabnum = $('.Js_tab').length,
        tablength,
        href = location.href,
        step = location.href.split('#');

    step.length == 1 ? step = 'step-one' : step = location.href.split('#')[location.href.split('#').length - 1];

    for(var i = 0; i < tabnum; i++){
        tablength = $('.Js_tab:eq(' + i + ') li').length;
        marginwidth = (totallength - endwidth * 2 - liwidth * tablength)/(tablength - 2 + 1)/2;
        marginarray.push(marginwidth);
        $('.Js_tab:eq(' + i + ') li.first').attr('style','margin-right:'+marginwidth+'px');
        $('.Js_tab:eq(' + i + ') li.last').attr('style','margin-left:'+marginwidth+'px');
        $('.Js_tab:eq(' + i + ') li.center').attr('style','margin-left:'+marginwidth+'px;'+'margin-right:'+marginwidth+'px');
        $('.Js_hor_inline:eq(' + i + ')').width(endwidth + liwidth + marginwidth);
    }
    $('.Js_tab li').on('click',function(){
        var tab_length = $(this).parent().children().length,
            index = $(this).index(),//li的index
            tab_index = 0,
            linewidth;

        //alert(tab_index);
        $('.tabs_wraper').each(function(i){
            $(this).attr("data-index", i)
        });
        tab_index = $(this).parents('.tabs_wraper').data("index");
        marginwidth = marginarray[tab_index];
        linewidth = endwidth + liwidth * ( index + 1 ) + marginwidth * ( ( index + 1 ) * 2 - 1 );
        if(index == tab_length - 1){
            linewidth = linewidth - marginwidth + endwidth;
        }
        $(this).parent().prevAll('.Js_hor_line').children('.Js_hor_inline').width(linewidth);
        for(var i = 0 ;i <= index; i++){
            $(this).parent().children(':eq(' + i + ')').addClass('focustab');
        }
        for(var i = index + 1; i < tab_length ;i++){
            $(this).parent().children(':eq(' + i + ')').removeClass('focustab');
        }
        $(this).parents('.Js_tabs_step').nextAll('.Js_tabs_content').hide();
        $(this).parents('.Js_tabs_step').nextAll('.Js_tabs_content:eq(' + index + ')').show();
    });
    $('.Js_tab li a[href=#' + step + ']').click();
});