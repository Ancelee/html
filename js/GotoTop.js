/**
 * [GoToTop description]
 * @type {Object}
 */
WG.GoToTop = {
	init: function(){
		this.render();
	},
	default: {
		topdistance: 200//到顶部的距离，达到此距离显示回到顶部按钮
	},
	render: function(){
		var gohtml = [
		'<div class="side_tools">',
			'<a id="go_top"></a>',
			'<a class="feedback" href=""></a>',
		'</div>'].join("");
		$('body').append(gohtml);
		if($(".side_tools").length > 0){
			var go_top = $("#go_top");
			if ($(window).scrollTop() < this.default.topdistance) {
				go_top.hide();
			}
			$(window).scroll(this.scrollDom);
			go_top.on("click",this.scrollToTop)
		}
	},
	scrollDom: function(){
		var go_top = $("#go_top"),T = WG.GoToTop;
		if ($(window).scrollTop() < T.default.topdistance) {
			go_top.fadeOut("fast");
		} else {
			go_top.fadeIn("fast");
		}
	},
	scrollToTop: function(){
		$("html,body").animate({
			scrollTop: 0
		}, 300);
		return false
	}
}
