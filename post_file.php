<?php
// In PHP versions earlier than 4.1.0, $HTTP_POST_FILES should be used instead
// of $_FILES.
$path = "upload/";

if (move_uploaded_file($_FILES['userfile']['tmp_name'],$path. $_FILES["userfile"]["name"])) {
    echo '{"content":"'.$path. $_FILES["userfile"]["name"].'","status":0}';
} else {
    echo "Possible file upload attack!\n";
}

?>